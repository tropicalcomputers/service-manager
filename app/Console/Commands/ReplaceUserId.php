<?php

namespace App\Console\Commands;

use App\Utilities\DataManagement\UserIdReplacer;
use Illuminate\Console\Command;

class ReplaceUserId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'replace:userid {old} {new} {dbName=tcl_servicemanager} {--delete}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Replaces the user ID in various database tables with a new value and optionally deletes the old user on completion';


    private $userIdReplacer;

    /**
     * Create a new command instance.
     *
     * @param UserIdReplacer $userIdReplacer
     * @return void
     */
    public function __construct(UserIdReplacer $userIdReplacer)
    {
        $this->userIdReplacer = $userIdReplacer;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $oldVal = $this->argument('old');
        $newVal = $this->argument('new');
        $dbName = $this->argument('dbName');
        $shouldDelete = $this->option('delete');

        $this->userIdReplacer->replace($oldVal, $newVal, $dbName, $shouldDelete);
    }

}
