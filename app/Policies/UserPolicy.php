<?php

namespace App\Policies;

use App\ServiceManager\User\Eloquent\UserModel;
use App\ServiceManager\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(UserModel $user)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  User $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\ServiceManager\User\Eloquent\UserModel  $user
     * @return mixed
     */
    public function create(UserModel $user)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\ServiceManager\User\Eloquent\UserModel  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update(UserModel $user, User $model)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\ServiceManager\User\Eloquent\UserModel  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete(UserModel $user, User $model)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\ServiceManager\User\Eloquent\UserModel  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function restore(UserModel $user, User $model)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\ServiceManager\User\Eloquent\UserModel  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function forceDelete(UserModel $user, User $model)
    {
        return $user->hasRole('admin');
    }
}
