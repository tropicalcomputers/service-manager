<?php
namespace App\ServiceManager\Part;

use App\Repositories\RepositoryBase\Repository;

interface PartRepository extends Repository
{
    public function find($id): ?Part;
    public function create(array $attributes): Part;
}
