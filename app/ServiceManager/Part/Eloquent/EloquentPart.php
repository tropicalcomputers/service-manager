<?php

namespace App\ServiceManager\Part\Eloquent;

use App\ServiceManager\Part\Part;
use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Part
 *
 * @property $id
 * @property $work_item_id
 * @property $serial
 * @property $description
 * @property $user_id
 * @property $created_at
 * @property $updated_at
 *
 *
 * @package App\ServiceManager\Part
 */
class EloquentPart extends Model implements Part
{
    /**
     * @var array
     */
    protected $guarded = [];

    protected $table = 'parts';

    public function getId()
    {
        return $this->id;
    }

    public function getWorkItemId(): int
    {
        return $this->work_item_id;
    }

    public function getSerial(): ?string
    {
        return $this->serial;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getUserId(): int
    {
        return $this->user_id;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): Carbon
    {
        return $this->updated_at;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function workItem()
    {
        return $this->belongsTo(EloquentWorkItem::class, 'work_item_id', 'id');
    }

}
