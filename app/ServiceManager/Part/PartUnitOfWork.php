<?php
namespace App\ServiceManager\Part;

use App\Repositories\RepositoryContracts\WorkItemRepository;
use App\ServiceManager\WorkItemHistory\WorkItemHistoryUnitOfWork;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PartUnitOfWork
{
    private $partRepository;
    private $workItemHistoryUnitOfWork;
    private $workItemRepository;

    public function __construct(
        PartRepository $partRepository,
        WorkItemHistoryUnitOfWork $workItemHistoryUnitOfWork,
        WorkItemRepository $workItemRepository
    )
    {
        $this->partRepository = $partRepository;
        $this->workItemHistoryUnitOfWork = $workItemHistoryUnitOfWork;
        $this->workItemRepository = $workItemRepository;
    }

    public function create($attributes)
    {
        return DB::transaction(function() use ($attributes) {
            $part = $this->partRepository->create($attributes);
            $logAttributes = [
                'work_item_id' => $attributes['work_item_id'],
                'changed_by' => $attributes['user_id'],
                'assigned_to' => $attributes['user_id'],
                'description' => "Part Added: {$part->getDescription()} ({$part->getSerial()})",
            ];
            $this->workItemHistoryUnitOfWork->logToHistory($logAttributes);
            return $part;
        });
    }

    public function delete($id)
    {
        return DB::transaction(function() use ($id) {

            if (!$part = $this->partRepository->find($id)) {
                return null;
            }

            if (!$this->partRepository->delete($id)) {
                return false;
            }

            $logAttributes = [
                'work_item_id' => $part->getWorkItemId(),
                'changed_by' => Auth::id(),
                'assigned_to' => Auth::id(),
                'description' => "Part Removed: {$part->getDescription()} ({$part->getSerial()})",
            ];
            $this->workItemHistoryUnitOfWork->logToHistory($logAttributes);
            return true;
        });
    }
}
