<?php


namespace App\ServiceManager\Part;


use Carbon\Carbon;

interface Part
{
    public function getId();
    public function getWorkItemId(): int;
    public function getSerial(): ?string;
    public function getDescription(): string;
    public function getUserId(): int;
    public function getCreatedAt(): Carbon;
    public function getUpdatedAt(): Carbon;

}
