<?php


namespace App\ServiceManager\User;


use Carbon\Carbon;

/**
 * Interface User
 * @package App\ServiceManager\User
 */
interface User
{
    public function getId(): int;
    public function getName(): string;
    public function getEmail(): string;
    public function getSignedUrl(): string;
    public function isEnabled(): bool;
    public function getCreatedAt(): Carbon;
    public function getUpdatedAt(): ?Carbon;

    public function  hasRole($roles): bool;

}
