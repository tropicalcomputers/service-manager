<?php

namespace App\ServiceManager\User\Eloquent;

use App\ServiceManager\User\User;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class UserModel
 * @package App\ServiceManager\User\Eloquent
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $signed_url
 * @property bool $enabled
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 */
class UserModel extends Authenticatable implements User
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'users';

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getSignedUrl(): string
    {
        return $this->signed_url;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): ?Carbon
    {
        return $this->updated_at;
    }


}
