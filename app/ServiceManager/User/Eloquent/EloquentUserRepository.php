<?php
namespace App\ServiceManager\User\Eloquent;

use App\Repositories\Eloquent\EloquentRepository;
use App\Repositories\RepositoryContracts\UserRepository;
use App\ServiceManager\User\User;

class EloquentUserRepository extends EloquentRepository implements UserRepository
{
    protected function getModelClass():string
    {
        return UserModel::class;
    }

    public function find($id): ?User
    {
        return parent::find($id);
    }

    public function first(): ?User
    {
        return parent::first();
    }

    public function firstOrFail(): User
    {
        return parent::firstOrFail();
    }

    public function findOrFail($id): User
    {
        return parent::find($id);
    }

    public function create(array $attributes = []): User
    {
        return parent::create($attributes);
    }
}
