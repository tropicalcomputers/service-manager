<?php

namespace App\ServiceManager\Priority;

use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    protected $guarded = [];
}
