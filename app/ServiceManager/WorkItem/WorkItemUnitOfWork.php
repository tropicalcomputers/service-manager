<?php
namespace App\ServiceManager\WorkItem;

use App\Repositories\RepositoryContracts\DeviceRepository;
use App\Repositories\RepositoryContracts\WorkItemRepository;

class WorkItemUnitOfWork
{
    private $workItemRepository;
    private $deviceRepository;

    public function __construct(
        WorkItemRepository $workItemRepository,
        DeviceRepository $deviceRepository
    )
    {
        $this->workItemRepository = $workItemRepository;
        $this->deviceRepository = $deviceRepository;
    }

    public function create(array $attributes): WorkItem
    {
        $workItem = $this->workItemRepository->create($attributes['workItem']);
        $this->deviceRepository->update($workItem->getDeviceId(), $attributes['device']);
        $this->workItemRepository->createInventory($workItem->getId(), $attributes['inventory']);
        return $workItem;
    }
}
