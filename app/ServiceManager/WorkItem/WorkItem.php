<?php


namespace App\ServiceManager\WorkItem;


use Carbon\Carbon;
use Illuminate\Support\Collection;

interface WorkItem
{
    public function getId(): int;

    /**
     * @return mixed
     */
    public function getBranch();

    /**
     * @return mixed
     */
    public function getPriority();

    /**
     * @return mixed
     */
    public function getStatus();

    /**
     * @return mixed
     */
    public function getSignedInBy();

    /**
     * @return mixed
     */
    public function getDescription();

    /**
     * @return mixed
     */
    public function getInvoiceNumber();

    /**
     * @return string
     */
    public function getNotes(): ?string;

    /**
     * @return int
     */
    public function getTime(): int;

    /**
     * @return mixed
     */
    public function getInventory();

    /**
     * @return string|null
     */
    public function getInventoryNotes(): ?string;

    public function getDeviceId(): int;
    public function getDevice();
    public function getBillings();
    public function getParts();
    public function getCustomer();
    public function getHistory(): Collection;
    public function getAssignedTo();

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon;

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon;

}
