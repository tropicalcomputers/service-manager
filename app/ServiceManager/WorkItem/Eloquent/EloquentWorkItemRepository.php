<?php
namespace App\ServiceManager\WorkItem\Eloquent;

use App\Repositories\Eloquent\EloquentRepository;
use App\Repositories\RepositoryContracts\WorkItemRepository;
use App\ServiceManager\WorkItem\WorkItem;
use Illuminate\Support\Facades\Auth;

class EloquentWorkItemRepository extends EloquentRepository implements WorkItemRepository
{
    public function create(array $attributes = []): WorkItem
    {
        return parent::create($attributes);
    }

    public function find($id): WorkItem
    {
        return parent::find($id);
    }

    protected function getModelClass():string
    {
        return EloquentWorkItem::class;
    }

    public function createHistory($workItemId, array $properties)
    {
        return $this->find($workItemId)->history()->create($properties);
    }

    public function createInventory($workItemId, array $options)
    {
        $workItem = $this->find($workItemId);
        foreach ($options as $option) {
            $workItem->inventory()->create(["inventory_option_id" => $option]);
        }
    }

    public function addBilling($workItemId, array $billingProperties)
    {
        return $this->find($workItemId)->billings()->create($billingProperties);
    }

    public function addPart($workItemId, array $properties)
    {
        $workItem = $this->find($workItemId);
        $workItem->parts()->create(array_merge($properties,['user_id'=>Auth::id()]));
        $this->logToHistory(
            $workItem,
            'Part Added: ' . $this->getPartDescription($properties)
        );
        return $workItem->fresh();
    }

    public function removePart($workItemId, $partId)
    {
        $workItem = $this->find($workItemId);
        if ($part = $workItem->parts->where('id','=',$partId)->first()) {
            $partArray = $part->toArray();
            $part->delete();
            $this->logToHistory(
                $workItem,
                'Part Removed: ' . $this->getPartDescription($partArray)
            );
        }
        return $workItem->fresh();
    }

    protected function getPartDescription(array $part)
    {
        return  $part['description'] . ' (Serial: ' . ($part['serial'] ?? 'none') .  ')';
    }
}
