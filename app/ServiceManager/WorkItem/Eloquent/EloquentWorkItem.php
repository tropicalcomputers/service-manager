<?php

namespace App\ServiceManager\WorkItem\Eloquent;

use App\ServiceManager\Billing\Billing;
use App\ServiceManager\Branch\Branch;
use App\ServiceManager\Customer\Customer;
use App\ServiceManager\Device\Eloquent\EloquentDevice;
use App\ServiceManager\Inventory\Inventory;
use App\ServiceManager\Part\Eloquent\EloquentPart;
use App\ServiceManager\Priority\Priority;
use App\ServiceManager\Status\Status;
use App\ServiceManager\User\Eloquent\UserModel;
use App\ServiceManager\WorkItem\WorkItem;
use App\ServiceManager\WorkItemHistory\WorkItemHistory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 *
 * Class EloquentWorkItem
 *
 * @property int $id
 * @property $branch
 * @property $priority
 * @property $status
 * @property $device_id
 * @property $device
 * @property $signedInBy
 * @property $assignedTo
 * @property $parts
 * @property Collection $history
 * @property $customer
 * @property $description
 * @property $invoice_number
 * @property string|null $notes
 * @property int $time
 * @property $inventory_options
 * @property string $inventory_notes
 * @property Collection $billings
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\ServiceManager\WorkItem\Eloquent
 */
class EloquentWorkItem extends Model implements WorkItem
{

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string
     */
    protected $table = 'work_items';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getSignedInBy()
    {
        return $this->signedInBy;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getInvoiceNumber()
    {
        return $this->invoice_number;
    }

    /**
     * @return string|null
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    /**
     * @return int
     */
    public function getTime(): int
    {
        return $this->time;
    }

    /**
     * @return mixed
     */
    public function getInventory()
    {
        return $this->inventory_options;
    }

    /**
     * @return string|null
     */
    public function getInventoryNotes(): ?string
    {
        return $this->inventory_notes;
    }

    /**
     * @return int
     */
    public function getDeviceId(): int
    {
        return $this->device_id;
    }

    /**
     * @return mixed
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @return Collection
     */
    public function getBillings()
    {
        return $this->billings;
    }

    /**
     * @return mixed
     */
    public function getParts()
    {
        return $this->parts;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return Collection
     */
    public function getHistory(): Collection
    {
        return $this->history;
    }

    /**
     * @return mixed
     */
    public function getAssignedTo()
    {
        return $this->assignedTo;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updated_at;
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function signedInBy()
    {
        return $this->belongsTo(UserModel::class, 'signed_in_by', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assignedTo()
    {
        return $this->belongsTo(UserModel::class, 'assigned_to', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parts()
    {
        return $this->hasMany(EloquentPart::class, 'work_item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function device()
    {
        return $this->belongsTo(EloquentDevice::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inventory()
    {
        return $this->hasMany(Inventory::class, 'work_item_id');
    }

    /**
     * @return mixed
     */
    public function getInventoryOptionsAttribute()
    {
        return $this->inventory->map(function($item,$key){
            return $item->option->name;
        })->implode(', ');

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this->hasMany(WorkItemHistory::class, 'work_item_id')->orderBy('id','desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function priority()
    {
        return $this->belongsTo(Priority::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo( Status::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo( Branch::class);
    }

    /**
     * @return mixed
     */
    public function scopeHasValidCustomer()
    {
        return $this->has('customer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function billings()
    {
        return $this->hasMany(Billing::class, 'work_item_id');
    }
}
