<?php
namespace App\ServiceManager\WorkItemHistory;

use App\Repositories\Eloquent\EloquentRepository;
use App\Repositories\RepositoryContracts\WorkItemHistoryRepository;
use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem;

class EloquentWorkItemHistoryRepository extends EloquentRepository implements WorkItemHistoryRepository
{
    protected function getModelClass():string
    {
        return WorkItemHistory::class;
    }

    public function create(array $properties = []): WorkItemHistory
    {
        $workItemHistory = parent::create($properties);
        $this->updateWorkItem($workItemHistory->workItem);
        return $workItemHistory;
    }

    protected function updateWorkItem(EloquentWorkItem $workItem)
    {
        $workItem->time = $workItem->history->sum('time');
        $workItem->status_id = $workItem->history->first() ? $workItem->history->first()->status_id : $workItem->status_id;
        $workItem->branch_id = $workItem->history->first() ? $workItem->history->first()->branch_id : $workItem->branch_id;
        $workItem->assigned_to = $workItem->history->first() ? $workItem->history->first()->assigned_to : $workItem->assigned_to;
        $workItem->branch_id = $workItem->history->first() ? $workItem->history->first()->branch_id : $workItem->branch_id;
        $workItem->save();
    }
}
