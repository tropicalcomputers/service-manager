<?php
namespace App\ServiceManager\WorkItemHistory;

use App\Repositories\RepositoryContracts\WorkItemHistoryRepository;
use App\Repositories\RepositoryContracts\WorkItemRepository;
use App\Repositories\RepositoryCriteria\Common\ByWorkItemId;
use App\Repositories\RepositoryCriteria\OrderBy;
use Illuminate\Support\Facades\Log;

class WorkItemHistoryUnitOfWork
{
    private $workItemHistoryRepository;
    private $workItemRepository;

    public function __construct(WorkItemHistoryRepository $workItemHistoryRepository, WorkItemRepository $workItemRepository)
    {
        $this->workItemHistoryRepository = $workItemHistoryRepository;
        $this->workItemRepository = $workItemRepository;
    }

    public function logToHistory($attributes)
    {
        $workItem = $this->workItemRepository->findOrFail($attributes['work_item_id']);
        $newWorkItemHistory = array_merge([
            'time' => 0,
            'customer_contacted' => false,
            'system_log' => true,
            'branch_id' => $workItem->branch_id,
            'status_id' => $workItem->status_id,

        ], $attributes);
        return $this->workItemHistoryRepository->create($newWorkItemHistory);
    }
}
