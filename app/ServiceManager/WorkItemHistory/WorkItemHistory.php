<?php

namespace App\ServiceManager\WorkItemHistory;

use App\ServiceManager\Branch\Branch;
use App\ServiceManager\Part\Eloquent\EloquentPart;
use App\ServiceManager\Status\Status;
use App\ServiceManager\User\Eloquent\UserModel;
use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem;
use Illuminate\Database\Eloquent\Model;

class WorkItemHistory extends Model
{
    protected $touches = ['workItem'];
    protected $guarded = [];

    public function changedBy()
    {
        return $this->belongsTo(UserModel::class, 'changed_by', 'id');
    }

    public function assignedTo()
    {
        return $this->belongsTo(UserModel::class, 'assigned_to', 'id');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function parts()
    {
        return $this->hasMany(EloquentPart::class, 'change_id', 'id');
    }

    public function workItem()
    {
        return $this->belongsTo(EloquentWorkItem::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
