<?php

namespace App\ServiceManager\Component;

use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem;
use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    protected $guarded = [];

    public function workItems()
    {
        return $this->belongsToMany(EloquentWorkItem::class);
    }
}
