<?php
namespace App\ServiceManager\Device;

use App\Repositories\Eloquent\EloquentCriterionFactory;
use App\Repositories\Eloquent\EloquentRepository;
use App\Repositories\RepositoryContracts\DeviceRepository;
use App\ServiceManager\Device\Eloquent\EloquentDevice;

class EloquentDeviceRepository extends EloquentRepository implements DeviceRepository
{
    public function __construct(EloquentCriterionFactory $factory)
    {
        parent::__construct($factory);
    }

    protected function getModelClass():string
    {
        return EloquentDevice::class;
    }

    public function find($id): ?Device
    {
        return parent::find($id);
    }

    public function findOrFail($id): Device
    {
        return parent::findOrFail($id);
    }

    public function first(): ?Device
    {
        return parent::first();
    }

    public function firstOrFail(): Device
    {
        return parent::firstOrFail();
    }

    public function create(array $attributes = []): Device
    {
        return parent::create($attributes);
    }
}


