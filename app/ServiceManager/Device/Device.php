<?php


namespace App\ServiceManager\Device;


interface Device
{
    public function getPassword(): ?string;
    public function getId(): int;
}
