<?php

namespace App\ServiceManager\Device\Eloquent;

use App\ServiceManager\Device\Device;
use App\ServiceManager\Device\DeviceType;
use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem;
use App\Utilities\Encryption\StringCryptInterface;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EloquentDevice
 *
 * @property $id
 * @property $password
 *
 * @package App\ServiceManager\Device\Eloquent
 */
class EloquentDevice extends Model implements Device
{
    /** @var StringCryptInterface */
    private static $stringCrypt;
    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var array
     */
    protected $appends = ['name'];

    protected $table = 'devices';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    protected static function getStringCrypt()
    {
        return self::$stringCrypt ?? resolve(StringCryptInterface::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::updating(function(EloquentDevice $model)
        {
            $model = self::encryptPassword($model);
        });

        static::creating(function(EloquentDevice $model)
        {
            $model = self::encryptPassword($model);
        });

        static::saving(function(EloquentDevice $model)
        {
            $model = self::encryptPassword($model);
        });

        static::retrieved(function($model)
        {
            $model = self::decryptPassword($model);
        });
    }

    private static function encryptPassword(EloquentDevice $model): EloquentDevice
    {
        try {
            if ($model->getPassword()) {
                self::getStringCrypt()->decrypt($model->password);
            }
        } catch (DecryptException $exception) {
            $model->password = !$model->password ?: self::getStringCrypt()->encrypt($model->password);
        }
        return $model;
    }

    private static function decryptPassword(EloquentDevice $model): EloquentDevice
    {
        try {
            if ($model->getPassword()) {
                while (true) {
                    $model->password = self::getStringCrypt()->decrypt($model->password);
                }
            }
        } catch (DecryptException $exception) {
            // already decrypted.
        }
        return $model;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deviceType()
    {
        return $this->belongsTo(DeviceType::class);
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->manufacturer . " " . $this->model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workItems()
    {
        return $this->hasMany(EloquentWorkItem::class,'device_id', 'id');
    }

    /**
     * @param Builder $query
     * @param string $term
     * @return Builder
     */
    public function scopeLike(Builder $query, $term = "")
    {
        return $query->whereRaw("CONCAT(`manufacturer`, ' ', `model`, ' ', `serial`) LIKE ?", ["%{$term}%"]);
    }

    /**
     * @param Builder $query
     * @param string $term
     * @return Builder
     */
    public function scopeManufacturerLike(Builder $query, $term = "")
    {
        return $query
            ->where("manufacturer","like", "%{$term}%")
            ->distinct()
            ->select('manufacturer')
            ->orderBy('manufacturer','asc');

    }

    /**
     * @param Builder $query
     * @param string $term
     * @param string $manufacturuer
     * @return Builder
     */
    public function scopeModelLike(Builder $query, $term = "", $manufacturuer = "")
    {
        return $query
            ->where("model","like", "%{$term}%")
            ->where("manufacturer","like", "%{$manufacturuer}%")
            ->distinct()
            ->select('model','manufacturer')
            ->orderBy('model','asc');
    }
}
