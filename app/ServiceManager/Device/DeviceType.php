<?php

namespace App\ServiceManager\Device;

use App\ServiceManager\Inventory\DeviceTypeInventoryOption;
use App\ServiceManager\Inventory\InventoryOption;
use Illuminate\Database\Eloquent\Model;

class DeviceType extends Model
{
    protected $guarded = [];

    public function inventoryOptions()
    {
        return $this->belongsToMany(InventoryOption::class)->using(DeviceTypeInventoryOption::class);
    }
}
