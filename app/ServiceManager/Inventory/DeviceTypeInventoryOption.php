<?php


namespace App\ServiceManager\Inventory;

use Illuminate\Database\Eloquent\Relations\Pivot;

class DeviceTypeInventoryOption extends Pivot
{
    public $timestamps = false;
}
