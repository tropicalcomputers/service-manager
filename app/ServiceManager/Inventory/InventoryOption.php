<?php

namespace App\ServiceManager\Inventory;

use App\ServiceManager\Device\DeviceType;
use Illuminate\Database\Eloquent\Model;

class InventoryOption extends Model
{
    protected $guarded = [];

    public function deviceTypes()
    {
        return $this->belongsToMany(DeviceType::class);
    }
}
