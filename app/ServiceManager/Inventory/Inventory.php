<?php

namespace App\ServiceManager\Inventory;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = 'inventory';
    protected $guarded = [];

    public function option()
    {
        return $this->belongsTo(InventoryOption::class,'inventory_option_id', 'id');
    }
}
