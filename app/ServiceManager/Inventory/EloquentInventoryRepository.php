<?php
namespace App\ServiceManager\WorkItem;

use App\Repositories\Eloquent\EloquentRepository;
use App\Repositories\RepositoryContracts\InventoryRepository;
use App\ServiceManager\Inventory\Inventory;

class EloquentInventoryRepository extends EloquentRepository implements InventoryRepository
{

    protected function entity()
    {
        return Inventory::class;
    }

}
