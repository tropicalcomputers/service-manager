<?php

namespace App\ServiceManager\Branch;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $guarded = [];
}
