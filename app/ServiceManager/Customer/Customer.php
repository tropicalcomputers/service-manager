<?php

namespace App\ServiceManager\Customer;

use App\ServiceManager\Company\Company;
use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded = [];
    protected $appends = ['full_name'];

    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function workItems()
    {
        return $this->hasMany(EloquentWorkItem::class, 'customer_id', 'id');
    }

    public function scopeLike($query, $term = "")
    {
        return $query->where('first_name','like',"%{$term}%")
            ->orWhere('last_name','like',"%{$term}%")
            ->orWhere(function(Builder $query) use($term) {
                $parts = explode(" ", $term);
                if (count($parts) > 1) {
                    return $query
                        ->where('first_name','like', "%" . $parts[0] . "%")
                        ->where('last_name','like', "%" . $parts[1] . "%");
                }
                return $query;
            })
            ->orWhereHas('company', function (Builder $query) use ($term) {
                $query->where('name', 'like', "%{$term}%");
            });
    }

    public function getDevicesAttribute()
    {
        $devices = collect([]);
        foreach ($this->workItems as $workItem) {
            $devices->push($workItem->device);
        }
        return $devices->unique('id')->values();
    }
}
