<?php
namespace App\ServiceManager\Customer;

use App\Repositories\Eloquent\EloquentRepository;
use App\Repositories\RepositoryContracts\CustomerRepository;

class EloquentCustomerRepository extends EloquentRepository implements CustomerRepository
{
    protected function getModelClass():string
    {
        return Customer::class;
    }

    public function createCompany($customerId, array $properties)
    {
        $customer = $this->find($customerId);
        $company = $customer->company()->create($properties);
        $customer->company()->associate($company);
        $customer->save();
    }

    public function deleteCompany($customerId, $companyId)
    {
        $this->find($customerId)->company()->delete($companyId);
    }

    public function find($id): ?Customer
    {
        return parent::find($id);
    }
}
