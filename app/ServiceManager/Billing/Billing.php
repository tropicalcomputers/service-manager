<?php

namespace App\ServiceManager\Billing;

use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem;
use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    protected $guarded = ['work_item_id'];
    public function workItem()
    {
        return $this->hasOne(EloquentWorkItem::class);
    }

}
