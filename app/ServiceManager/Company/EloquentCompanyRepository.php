<?php
namespace App\ServiceManager\Company;

use App\Repositories\Eloquent\EloquentRepository;
use App\Repositories\RepositoryContracts\CompanyRepository;

class EloquentCompanyRepository extends EloquentRepository implements CompanyRepository
{
    protected function getModelClass():string
    {
        return Company::class;
    }

}
