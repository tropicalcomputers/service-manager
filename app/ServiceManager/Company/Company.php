<?php

namespace App\ServiceManager\Company;

use App\ServiceManager\Customer\Customer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded = [];

    public function contacts()
    {
        return $this->hasMany(Customer::class, 'company_id', 'id');
    }

    public function getDevicesAttribute()
    {
        $devices = collect([]);
        foreach ($this->contacts as $contact) {
            foreach ($contact->workItems as $workItem) {
                $devices->push($workItem->device);
            }
        }
        return $devices->unique('id')->values();
    }

    public function scopeLike(Builder $query, $term = "")
    {
        return $query->where('name','like',"%{$term}%");
    }
}
