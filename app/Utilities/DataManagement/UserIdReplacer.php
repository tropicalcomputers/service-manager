<?php


namespace App\Utilities\DataManagement;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Class UserIdReplacer
 *
 * Replaces the user ID in various database tables with a new value and optionally deletes the old user on completion
 *
 *
 * @package App\Utilities\DataManagement
 */
class UserIdReplacer
{
    /**
     * @param $oldVal
     * @param $newVal
     * @param $dbName
     * @param bool $deleteOld
     */
    public function replace($oldVal, $newVal, $dbName, $deleteOld = true)
    {
        $columnNames = $this->getColumnNames();
        $tableNames = $this->getTableNames($dbName);
        $listOfUpdates = $this->getListOfUpdates($columnNames, $tableNames);
        $this->updateValues($oldVal, $newVal, $listOfUpdates);
        if ($deleteOld) {
            $this->deleteUser($oldVal);
        }
    }

    /**
     * @param $id
     */
    private function deleteUser($id)
    {
        DB::table('users')->delete($id);
    }

    /**
     * @return array
     */
    private function getColumnNames()
    {
        return ['user_id','changed_by','assigned_to','signed_in_by','model_id'];
    }

    /**
     * @param string $dbName
     * @return array
     */
    private function getTableNames(string $dbName): array
    {
        $tableNames = [];
        $tables = DB::select('SHOW TABLES');
        $tableNameAttribute = "Tables_in_{$dbName}";
        foreach($tables as $table)
        {
            $tableNames[] = $table->$tableNameAttribute;
        }
        return $tableNames;
    }

    /**
     * @param array $columnNames
     * @param array $tableNames
     * @return array
     */
    private function getListOfUpdates(array $columnNames, array $tableNames)
    {
        $list = [];

        foreach ($columnNames as $columnName) {
            foreach($tableNames as $tableName) {
                if (Schema::hasColumn($tableName, $columnName)) {
                    $list[] = ['column' => $columnName,'table'=>$tableName];
                }
            }
        }

        return $list;
    }

    /**
     * @param $oldVal
     * @param $newVal
     * @param $listOfUpdates
     */
    private function updateValues($oldVal, $newVal, $listOfUpdates)
    {
        foreach ($listOfUpdates as $update) {
            DB::table($update['table'])->where($update['column'],'=',$oldVal)->update([$update['column']=>$newVal]);
        }
    }

}
