<?php

namespace App\Utilities\Encryption;

interface StringCryptInterface
{
    public function encrypt(string $string): string;

    public function decrypt(string $string): ?string;
}
