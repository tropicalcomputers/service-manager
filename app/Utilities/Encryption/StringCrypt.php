<?php
namespace App\Utilities\Encryption;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;

class StringCrypt implements StringCryptInterface
{
    public function encrypt(string $password): string
    {
        return Crypt::encryptString($password);
    }

    public function decrypt(string $password): ?string
    {
        return Crypt::decryptString($password);
    }
}
