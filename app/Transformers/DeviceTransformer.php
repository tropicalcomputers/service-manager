<?php

namespace App\Transformers;

use App\ServiceManager\Device\Device;
use App\ServiceManager\Device\Eloquent\EloquentDevice;
use League\Fractal\TransformerAbstract;

class DeviceTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'workItems'
    ];

    /**
     * A Fractal transformer.
     *
     * @param Device $device
     * @return array
     */
    public function transform(Device $device)
    {
        return [
            'id'=>$device->getId(),
            'name' => $device->name,
            'type' => $device->deviceType->only('id','name'),
            'manufacturer' => $device->manufacturer,
            'model' => $device->model,
            'serial' => $device->serial,
            'password' => $device->getPassword(),
        ];
    }

    public function includeWorkItems(EloquentDevice $device)
    {
        return $this->collection($device->workItems, new WorkItemTransformer());
    }
}
