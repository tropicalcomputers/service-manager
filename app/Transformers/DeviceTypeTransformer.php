<?php

namespace App\Transformers;

use App\ServiceManager\Device\DeviceType;
use League\Fractal\TransformerAbstract;

class DeviceTypeTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param DeviceType $deviceType
     * @return array
     */
    public function transform(DeviceType $deviceType)
    {
        return [
            'id'=>$deviceType->id,
            'name' => $deviceType->name,
        ];
    }
}
