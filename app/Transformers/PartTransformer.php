<?php

namespace App\Transformers;

use App\ServiceManager\Part\Eloquent\EloquentPart;
use League\Fractal\TransformerAbstract;

class PartTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     * @param EloquentPart $part
     * @return array
     */
    public function transform(EloquentPart $part)
    {
        return [
            'id'=>$part->id,
            'description' => $part->description,
            'serial' => $part->serial,
        ];
    }
}
