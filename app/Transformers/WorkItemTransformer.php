<?php

namespace App\Transformers;

use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem;
use App\ServiceManager\WorkItem\WorkItem;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class WorkItemTransformer extends TransformerAbstract
{
    protected $deviceTransformer;

    protected $availableIncludes = [
        'customer', 'device', 'workItemHistory','billings','parts','signed_in_by','assigned_to'
    ];

    protected $defaultIncludes = [
    ];
    /**
     * A Fractal transformer.
     *
     * @param WorkItem $workItem
     * @return array
     */
    public function transform(WorkItem $workItem)
    {
        return [
            'id' => (int) $workItem->getId(),
            'device' => $workItem->getDevice(),
            'branch' => $workItem->getBranch()->only('id','name'),
            'priority' => $workItem->getPriority()->only('id','name'),
            'status' => $workItem->getStatus()->only('id','name'),
            'signed_in_by' => $workItem->getSignedInBy(),
            'description' => $workItem->getDescription(),
            'invoice_number' => $workItem->getInvoiceNumber(),
            'notes' => $workItem->getNotes(),
            'time' => $workItem->getTime(),
            'inventory' => $workItem->getInventory(),
            'inventory_notes' => $workItem->getInventoryNotes(),
            'updated_at' => $workItem->getUpdatedAt(),
            'created_at' => $workItem->getCreatedAt()
        ];
    }




    public function includeBillings(WorkItem $workItem)
    {
        return $this->collection($workItem->getBillings(), new BillingTransformer());
    }

    public function includeParts(WorkItem $workItem)
    {
        return $this->collection(collect($workItem->getParts()), new PartTransformer());
    }

    public function includeCustomer(WorkItem $workItem)
    {
        $customer = $workItem->getCustomer();
        return $this->item($customer, new CustomerTransformer());
    }

    public function includeDevice(WorkItem $workItem)
    {
        return $workItem->getDevice() ? $this->item($workItem->getDevice(), new DeviceTransformer()) : null;
    }

    public function includeWorkItemHistory(WorkItem $workItem)
    {
        return $this->collection($workItem->getHistory(), new WorkItemHistoryTransformer());
    }

    public function includeAssignedTo(WorkItem $workItem)
    {
        if ($workItem->getAssignedTo()) {
            return $this->item($workItem->getAssignedTo() , new AssignedToTransformer());
        }
        return null;
    }

    public function includeSignedInBy(WorkItem $workItem)
    {
        return $this->item($workItem->getSignedInBy(), new ChangedByTransformer());
    }
}
