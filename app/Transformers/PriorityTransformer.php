<?php

namespace App\Transformers;

use App\ServiceManager\Priority\Priority;
use League\Fractal\TransformerAbstract;

class PriorityTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Priority $priority
     * @return array
     */
    public function transform(Priority $priority)
    {
        return [
            'id'=>$priority->id,
            'name' => $priority->name,
        ];
    }
}
