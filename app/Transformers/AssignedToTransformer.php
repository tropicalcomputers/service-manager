<?php

namespace App\Transformers;

use App\ServiceManager\Customer\Customer;
use App\ServiceManager\User\Eloquent\UserModel;
use League\Fractal\TransformerAbstract;

class AssignedToTransformer extends TransformerAbstract
{

    /**
     * A Fractal transformer.
     * @param UserModel $user
     * @return array
     */
    public function transform(UserModel $user)
    {
        return [
            'id'=>$user->id,
            'name' => $user->name,
            'email' => $user->email,
        ];
    }
}
