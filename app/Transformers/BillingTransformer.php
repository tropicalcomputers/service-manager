<?php

namespace App\Transformers;

use App\ServiceManager\Billing\Billing;
use App\ServiceManager\Device\Eloquent\EloquentDevice;
use League\Fractal\TransformerAbstract;

class BillingTransformer extends TransformerAbstract
{

    /**
     * A Fractal transformer.
     *
     * @param Billing $billing
     * @return array
     */
    public function transform(Billing $billing)
    {
        return [
            'id'=>$billing->id,
            'type' => $billing->type,
            'reference' => $billing->reference,
            'notes' => $billing->notes,
            'void' => $billing->void,
            'created_at' => $billing->created_at->toDayDateTimeString(),
            'updated_at' => $billing->updated_at->toDayDateTimeString(),
        ];
    }
}
