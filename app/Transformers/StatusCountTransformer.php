<?php

namespace App\Transformers;
use App\ServiceManager\Status\Status;
use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem;
use League\Fractal\TransformerAbstract;

class StatusCountTransformer extends TransformerAbstract
{
    private $statuses;
    private $groups = [];

    /**
     * A Fractal transformer.
     * @return array
     */
    public function transform()
    {
        $this->setupGroups();

        return [
            'aggregates' => [
                'total_open'=> $this->getCounts('total_open','Total Open'),
            ],
            'totals' => [
                'active'=> $this->getCounts('active','Active'),
                'awaiting_billing' => $this->getCounts('billing','Awaiting Billing'),
                'awaiting_parts' => $this->getCounts('parts','Awaiting Parts'),
                'awaiting_customer' => $this->getCounts('customer','Awaiting Customer Response'),
                'awaiting_collection' => $this->getCounts('collection','Awaiting Collection'),
                'awaiting_delivery' => $this->getCounts('delivery','Awaiting Delivery'),
                'rma' => $this->getCounts('rma','RMA'),
            ],
        ];
    }

    protected function setupGroups()
    {
        $this->statuses = Status::select(['id','name'])->get();
        $this->groups['total_open'] = $this->statuses->whereIn('name',['Active','Awaiting Parts','Awaiting Customer Response','RMA'])->pluck('id')->toArray();
        $this->groups['active'] = $this->statuses->where('name','Active')->pluck('id')->toArray();
        $this->groups['parts'] = $this->statuses->where('name','Awaiting Parts')->pluck('id')->toArray();
        $this->groups['billing'] = $this->statuses->where('name','Awaiting Billing')->pluck('id')->toArray();
        $this->groups['customer'] = $this->statuses->where('name','Awaiting Customer Response')->pluck('id')->toArray();
        $this->groups['collection'] = $this->statuses->where('name','Awaiting Collection')->pluck('id')->toArray();
        $this->groups['delivery'] = $this->statuses->where('name','Awaiting Delivery')->pluck('id')->toArray();
        $this->groups['rma'] = $this->statuses->where('name','RMA')->pluck('id')->toArray();
    }

    protected function getCounts(string $group, string $label)
    {
        return [
            'id' => implode("",$this->groups[$group]),
            'label' => $label,
            'value' => EloquentWorkItem::whereIn('status_id',$this->groups[$group])->count(),
        ];
    }
}
