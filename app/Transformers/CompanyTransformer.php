<?php

namespace App\Transformers;

use App\ServiceManager\Company\Company;
use League\Fractal\TransformerAbstract;

class CompanyTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'contacts','devices'
    ];

    /**
     * A Fractal transformer.
     * @param Company $company
     * @return array
     */
    public function transform(Company $company)
    {
        return [
            'id'=>$company->id,
            'name' => $company->name,
            'telephone' => $company->telephone,
            'address' => $company->address,
        ];
    }

    public function includeContacts(Company $company)
    {
        return $this->collection($company->contacts, new ContactTransformer());
    }

    public function includeDevices(Company $company)
    {
        return $this->collection($company->devices, new DeviceTransformer());
    }
}
