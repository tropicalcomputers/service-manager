<?php

namespace App\Transformers;

use App\ServiceManager\Customer\Customer;
use League\Fractal\TransformerAbstract;

class CustomerTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'company','devices'
    ];

    /**
     * A Fractal transformer.
     * @param Customer $customer
     * @return array
     */
    public function transform(Customer $customer)
    {
        return [
            'id'=>$customer->id,
            'first_name' => $customer->first_name,
            'last_name' => $customer->last_name,
            'full_name' => $customer->fullName,
            'email' => $customer->email,
            'address' => $customer->address,
            'mobile_telephone' => $customer->mobile_telephone,
            'home_telephone' => $customer->home_telephone,
            'work_telephone' => $customer->work_telephone,
        ];
    }

    public function includeCompany(Customer $customer)
    {
        $company = $customer->company;
        return $company ? $this->item($company, new CompanyTransformer()) : null;
    }


    public function includeDevices(Customer $customer)
    {
        return $this->collection($customer->devices, new DeviceTransformer());
    }
}
