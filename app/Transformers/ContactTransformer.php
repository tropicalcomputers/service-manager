<?php

namespace App\Transformers;

use App\ServiceManager\Customer\Customer;
use League\Fractal\TransformerAbstract;

class ContactTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'company'
    ];

    /**
     * A Fractal transformer.
     * @param Customer $customer
     * @return array
     */
    public function transform(Customer $customer)
    {
        return [
            'id'=>$customer->id,
            'name' => $customer->fullName,
            'first_name' => $customer->first_name,
            'last_name' => $customer->last_name,
            'email' => $customer->email,
            'work_telephone' => $customer->work_telephone,
            'mobile_telephone' => $customer->mobile_telephone,
        ];
    }

    public function includeCompany(Customer $contact)
    {
        return $this->item($contact->company, new CompanyTransformer());
    }
}
