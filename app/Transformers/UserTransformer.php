<?php

namespace App\Transformers;

use App\ServiceManager\User\Eloquent\UserModel;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     * @param UserModel $user
     * @return array
     */
    public function transform(UserModel $user)
    {
        return [
            'id'=>$user->id,
            'name' => $user->name,
            'email' => $user->email,
            'enabled' => $user->isEnabled(),
            'updated_at' => $user->updated_at->toDayDateTimeString(),
            'created_at' => $user->created_at->toDayDateTimeString()
        ];
    }
}
