<?php

namespace App\Transformers;

use App\ServiceManager\Customer\Customer;
use App\ServiceManager\User\Eloquent\UserModel;
use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem;
use App\ServiceManager\WorkItemHistory\WorkItemHistory;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class WorkItemHistoryTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'assigned_to', 'changed_by'
    ];

    /**
     * A Fractal transformer.
     * @param WorkItemHistory $workItemHistory
     * @return array
     */
    public function transform(WorkItemHistory $workItemHistory)
    {
        return [
            'id'=>$workItemHistory->id,
            'branch' => $workItemHistory->branch->only('id','name'),
            'status' => $workItemHistory->status->only('id','name'),
            'description' => $workItemHistory->description,
            'customer_contacted' => $workItemHistory->customer_contacted,
            'time' => $workItemHistory->time,
            'system_log' => !!$workItemHistory->system_log,
            'created_at' => $workItemHistory->created_at,
            'updated_at' => $workItemHistory->updated_at,
        ];
    }

    public function includeAssignedTo(WorkItemHistory $workItemHistory)
    {
        if ($workItemHistory->assignedTo) {
            return $this->item($workItemHistory->assignedTo , new AssignedToTransformer());
        }
        return null;
    }

    public function includeChangedBy(WorkItemHistory $workItemHistory)
    {
        return $this->item($workItemHistory->changedBy, new ChangedByTransformer());
    }
}
