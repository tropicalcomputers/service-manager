<?php
namespace App\Repositories\RepositoryContracts;

use App\Repositories\RepositoryBase\Criteria\Criteria;
use App\Repositories\RepositoryBase\Repository;
use App\ServiceManager\WorkItem\WorkItem;

interface WorkItemRepository extends Repository {

    public function create(array $attributes = []): WorkItem;
    public function find($id): ?WorkItem;

    public function createHistory($workItemId, array $properties);
    public function createInventory($workItemId, array $options);
    public function addBilling($workItemId, array $billingProperties);
    public function addPart($workItemId, array $partProperties);
    public function removePart($workItemId, $partId);
}
