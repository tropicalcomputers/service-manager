<?php
namespace App\Repositories\RepositoryContracts;

use App\Repositories\RepositoryBase\Criteria\Criteria;
use App\Repositories\RepositoryBase\Repository;

interface WorkItemHistoryRepository extends Repository {
}
