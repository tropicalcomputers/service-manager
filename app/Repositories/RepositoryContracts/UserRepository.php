<?php
namespace App\Repositories\RepositoryContracts;

use App\Repositories\RepositoryBase\Repository;
use App\ServiceManager\User\User;

interface UserRepository extends Repository
{
    public function find($id): ?User;
    public function first(): ?User;
    public function findOrFail($id):User;
    public function create(array $attributes = []):User;
}
