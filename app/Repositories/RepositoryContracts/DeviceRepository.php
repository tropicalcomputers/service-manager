<?php
namespace App\Repositories\RepositoryContracts;

use App\Repositories\RepositoryBase\Criteria\Criteria;
use App\Repositories\RepositoryBase\Repository;
use App\ServiceManager\Device\Device;

interface DeviceRepository extends Repository, Criteria
{
    public function find($id): ?Device;
    public function findOrFail($id): Device;
    public function first(): ?Device;
    public function firstOrFail(): Device;
    public function create(array $attributes = []): Device;
}
