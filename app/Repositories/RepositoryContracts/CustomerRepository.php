<?php
namespace App\Repositories\RepositoryContracts;

use App\Repositories\RepositoryBase\Criteria\Criteria;
use App\Repositories\RepositoryBase\Repository;
use App\ServiceManager\Customer\Customer;

interface CustomerRepository extends Repository, Criteria
{
    public function find($id): ?Customer;

    public function createCompany($customerId, array $properties);
    public function deleteCompany($customerId, $companyId);
}
