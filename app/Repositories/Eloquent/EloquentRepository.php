<?php
namespace App\Repositories\Eloquent;

use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;
use App\Repositories\RepositoryBase\Repository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

abstract class EloquentRepository implements Repository {
    protected $model;
    protected $criterionFactory;


    /**
     * @return string
     */
    abstract protected function getModelClass(): string;

    /**
     * @var Builder
     */
    protected $query;

    /**
     * @var array
     */
    protected $defaultCriteria = [];

    /**
     * EloquentRepository constructor.
     */
    public function __construct(EloquentCriterionFactory $factory)
    {
        $this->criterionFactory = $factory;
        $this->resolveModel();
    }

    /**
     *
     */
    private function resolveModel()
    {
        $class = $this->getModelClass();
        $model = new $class;

        if (!$model instanceof Model) {
            throw new \InvalidArgumentException("Eloquent repository getModelClass must define an eloquent model. {$class} is not an eloquent model");
        }

        $this->query = $model;
    }



    /**
     * @param mixed ...$criteria
     * @return $this
     * @throws \Exception
     */
    public function withDefaultCriteria(...$criteria)
    {
        $criteria = Arr::flatten($criteria);

        foreach ($criteria as $criterion) {
            if (!is_object($criterion) || !$criterion instanceof Criterion) {
                throw new \Exception(is_object($criterion) ? get_class($criterion) : $criterion . "not an instance of " . Criterion::class); //TODO: exception
            }
        }

        $this->defaultCriteria = array_merge($this->defaultCriteria, $criteria);

        return $this;
    }

    /**
     * @param mixed ...$criteria
     * @return $this
     * @throws \Exception
     */
    public function withCriteria(...$criterionBases): Repository
    {
        $criterionBases = Arr::flatten($criterionBases);
        foreach ($criterionBases as $criterionBase) {
            if (!is_object($criterionBase) || !$criterionBase instanceof CriterionBase) {
                throw new \Exception((is_object($criterionBase) ? get_class($criterionBase) : $criterionBase) . " is not an instance of " . CriterionBase::class); //TODO: exception
            }

            $concreteCriterion = $this->criterionFactory->make($criterionBase);
            $this->query = $concreteCriterion->apply($this->query);
        }

        return $this;
    }

    /**
     * @param mixed $id
     */
    public function find($id)
    {
        return $this->getFinalQuery()->find($id);
    }

    /**
     * @return Builder|Model|\Illuminate\Database\Query\Builder
     */
    public function getFinalQuery()
    {
        foreach ($this->defaultCriteria as $criterion) {
            $this->query = $criterion->applyToEloquent($this->query);
        }

        $query = $this->query;

        $this->reset();

        return $query;
    }

    /**
     * @return $this
     */
    public function reset()
    {
        $this->resolveModel();
        return $this;
    }

    /**
     * @param mixed $id
     * @throws ModelNotFoundException
     */
    public function findOrFail($id)
    {
        return $this->getFinalQuery()->findOrFail($id);
    }

    /**
     * @return mixed
     */
    public function newInstance()
    {
        $class = $this->getModelClass();
        return new $class;
    }

    /**
     * @return Collection
     */
    public function get(): Collection
    {
        return $this->getFinalQuery()->get();
    }

    /**
     * @return mixed
     */
    public function first()
    {
        return $this->getFinalQuery()->first();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function firstOrFail()
    {
        return $this->getFinalQuery()->firstOrFail();
    }

    /**
     * @param string|array $columns
     * @return int
     */
    public function count($columns = '*'): int
    {
        return $this->getFinalQuery()->count($columns);
    }

    /**
     * @return bool
     */
    public function exists(): bool
    {
        return $this->getFinalQuery()->exists();
    }

    /**
     * @param mixed ...$relations
     * @return EloquentRepository
     */
    public function withRelations(...$relations): Repository
    {
        $relations = Arr::flatten($relations);

        $this->query = $this->query->with($relations);

        return $this;
    }

    /**
     * @param string $id
     * @param array $attributes
     * @throws ModelNotFoundException
     * @return bool
     */
    public function update($id, array $attributes): bool
    {
        //TODO: Why is this not $this->>getFinalQuery() ?
        $model = $this->query->find($id);

        if (!$model) {
            throw (new ModelNotFoundException())->setModel($model);
        }
        return $model->update($attributes);
    }

    /**
     * @param array $attributes
     * @param array|null $ids
     * @return int
     */
    public function massUpdate(array $attributes, array $ids = null): int
    {
        if ($ids) {
            //TODO: Why is this not $this->>getFinalQuery() ?
            $this->query = $this->getQuery()->whereIn($this->getKeyField(), $ids);
        }

        return $this->getFinalQuery()->update($attributes);
    }

    /**
     * @return Builder|Model|\Illuminate\Database\Query\Builder
     */
    public function getQuery()
    {
        if ($this->query instanceof Model) {
            return $this->query->getQuery();
        }

        return $this->query;
    }

    /**
     * @return string
     */
    private function getKeyField()
    {
        return $this->getModel()->getKeyName();
    }

    /**
     * @return Builder|Model
     */
    protected function getModel()
    {
        if ($this->query instanceof Builder) {
            return $this->query->getModel();
        }

        return $this->query;
    }

    /**
     * @param $id
     * @return bool|null
     * @throws \Exception
     */
    public function delete($id): ?bool
    {
        $model = $this->query->find($id);

        if (!$model) {
            return null;
        }

        return $model->delete();
    }

    /**
     * @param array $ids
     * @return $this
     */
    public function whereIds(array $ids)
    {
        $this->query = $this->queryWithIds($ids);

        return $this;
    }

    /**
     * @param array $ids
     * @return Builder
     */
    private function queryWithIds(array $ids)
    {
        return $this->query->whereIn($this->getKeyField(), $ids);
    }

    /**
     * @param array|null $ids
     * @return int
     * @throws \Exception
     */
    public function massDelete(array $ids = null): int
    {
        if ($ids) {
            $this->query = $this->getQuery()->whereIn($this->getKeyField(), $ids);
        }

        return $this->getFinalQuery()->delete();
    }

    /**
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function paginate($perPage = 15): LengthAwarePaginator
    {
        return $this->getFinalQuery()->paginate($perPage);
    }


    /**
     * @param int $limit
     * @return $this|Repository
     */
    public function withLimit(int $limit):Repository
    {
        $this->query = $this->query->limit($limit);

        return $this;
    }

    /**
     * @param int $offset
     * @return $this|Repository
     */
    public function withOffset(int $offset)
    {
        $this->query = $this->query->offset($offset);

        return $this;
    }

    /**
     * @param  string $column
     * @param  string $direction
     * @return $this
     */
    public function withOrderBy($column, $direction = 'asc')
    {
        $this->query = $this->query->orderBy($column, $direction);

        return $this;
    }


    /**
     * @param int $size
     * @param callable $callback
     * @return Collection
     */
    public function chunk(int $size, callable $callback): Collection
    {
        return $this->getFinalQuery()->chunk($size, $callback);
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes = [])
    {
        return $this->getModel()->create($attributes);
    }

    /**
     * @param \stdClass $model
     * @param array $attributes
     * @return \stdClass
     */
    public function saveModel($model, array $attributes)
    {
        $model->fill($attributes)->save();
        return $model;
    }
}
