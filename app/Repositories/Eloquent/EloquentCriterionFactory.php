<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Eloquent\Criteria\Common\EloquentByWorkItemId;
use App\Repositories\Eloquent\Criteria\Common\EloquentColumnTextSearch;
use App\Repositories\Eloquent\Criteria\Device\EloquentBelongsToCustomer;
use App\Repositories\Eloquent\Criteria\Device\EloquentHasPassword;
use App\Repositories\Eloquent\Criteria\EloquentCompanyLike;
use App\Repositories\Eloquent\Criteria\EloquentContains;
use App\Repositories\Eloquent\Criteria\EloquentCustomerLike;
use App\Repositories\Eloquent\Criteria\EloquentDeviceLike;
use App\Repositories\Eloquent\Criteria\EloquentEagerLoad;
use App\Repositories\Eloquent\Criteria\EloquentFilteredDeviceManufacturers;
use App\Repositories\Eloquent\Criteria\EloquentFilteredDeviceModels;
use App\Repositories\Eloquent\Criteria\EloquentHasValidCustomer;
use App\Repositories\Eloquent\Criteria\EloquentLatest;
use App\Repositories\Eloquent\Criteria\EloquentLimit;
use App\Repositories\Eloquent\Criteria\EloquentOrderBy;
use App\Repositories\Eloquent\Criteria\EloquentWhere;
use App\Repositories\Eloquent\Criteria\EloquentWhereIn;
use App\Repositories\Eloquent\Criteria\WorkItem\EloquentAssignedTo;
use App\Repositories\Eloquent\Criteria\WorkItem\EloquentDateRange;
use App\Repositories\Eloquent\Criteria\WorkItem\EloquentIsBilled;
use App\Repositories\Eloquent\Criteria\WorkItem\EloquentSignedInBy;
use App\Repositories\Eloquent\Criteria\WorkItem\EloquentWithBranch;
use App\Repositories\Eloquent\Criteria\WorkItem\EloquentWithPriority;
use App\Repositories\Eloquent\Criteria\WorkItem\EloquentWithStatus;
use App\Repositories\Eloquent\Criteria\WorkItem\EloquentWithSupportContract;
use App\Repositories\Eloquent\Criteria\WorkItem\EloquentWithWarranty;
use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;
use App\Repositories\RepositoryBase\Criteria\CriterionFactory;
use App\Repositories\RepositoryCriteria\Common\ByWorkItemId;
use App\Repositories\RepositoryCriteria\CompanyLike;
use App\Repositories\RepositoryCriteria\Contains;
use App\Repositories\RepositoryCriteria\CustomerLike;
use App\Repositories\RepositoryCriteria\Device\BelongsToCustomer;
use App\Repositories\RepositoryCriteria\Device\HasPassword;
use App\Repositories\RepositoryCriteria\DeviceLike;
use App\Repositories\RepositoryCriteria\EagerLoad;
use App\Repositories\RepositoryCriteria\FilteredDeviceManufacturers;
use App\Repositories\RepositoryCriteria\FilteredDeviceModels;
use App\Repositories\RepositoryCriteria\HasValidCustomer;
use App\Repositories\RepositoryCriteria\Latest;
use App\Repositories\RepositoryCriteria\Limit;
use App\Repositories\RepositoryCriteria\OrderBy;
use App\Repositories\RepositoryCriteria\Where;
use App\Repositories\RepositoryCriteria\ColumnTextSearch;
use App\Repositories\RepositoryCriteria\WhereIn as WhereInBase;
use App\Repositories\RepositoryCriteria\WorkItem\AssignedTo;
use App\Repositories\RepositoryCriteria\WorkItem\DateRange;
use App\Repositories\RepositoryCriteria\WorkItem\IsBilled;
use App\Repositories\RepositoryCriteria\WorkItem\SignedInBy;
use App\Repositories\RepositoryCriteria\WorkItem\WithBranch;
use App\Repositories\RepositoryCriteria\WorkItem\WithPriority;
use App\Repositories\RepositoryCriteria\WorkItem\WithSupportContract;
use App\Repositories\RepositoryCriteria\WorkItem\WithStatus;
use App\Repositories\RepositoryCriteria\WorkItem\WithWarranty;

class EloquentCriterionFactory implements CriterionFactory
{
    /**
     * @return array
     */
    protected function getMap(): array
    {
        return [
            WhereInBase::class => EloquentWhereIn::class,
            EagerLoad::class => EloquentEagerLoad::class,
            Contains::class => EloquentContains::class,
            Where::class => EloquentWhere::class,
            OrderBy::class => EloquentOrderBy::class,
            CustomerLike::class => EloquentCustomerLike::class,
            DeviceLike::class => EloquentDeviceLike::class,
            Limit::class => EloquentLimit::class,
            CompanyLike::class => EloquentCompanyLike::class,
            Latest::class => EloquentLatest::class,
            FilteredDeviceManufacturers::class => EloquentFilteredDeviceManufacturers::class,
            FilteredDeviceModels::class => EloquentFilteredDeviceModels::class,
            HasValidCustomer::class => EloquentHasValidCustomer::class,
            HasPassword::class => EloquentHasPassword::class,
            ColumnTextSearch::class => EloquentColumnTextSearch::class,
            WithStatus::class => EloquentWithStatus::class,
            WithBranch::class => EloquentWithBranch::class,
            WithPriority::class => EloquentWithPriority::class,
            DateRange::class => EloquentDateRange::class,
            SignedInBy::class => EloquentSignedInBy::class,
            AssignedTo::class => EloquentAssignedTo::class,
            WithWarranty::class => EloquentWithWarranty::class,
            WithSupportContract::class => EloquentWithSupportContract::class,
            IsBilled::class => EloquentIsBilled::class,
            ByWorkItemId::class => EloquentByWorkItemId::class,
            BelongsToCustomer::class => EloquentBelongsToCustomer::class,
        ];
    }


    public function make(CriterionBase $criterionBase): Criterion
    {
        $criterionBaseClass = get_class($criterionBase);
        if (array_key_exists($criterionBaseClass, $this->getMap())) {
            $concreteClass = $this->getMap()[$criterionBaseClass];
            /** @var Criterion $concreteObject */
            $concreteObject = new $concreteClass();
            return $concreteObject->setBase($criterionBase);
        }

        switch(true) {
            // Maybe it needs to be made with constructor arguments... do that here...
            default:
                throw new \Exception('Criteria Not Implemented' . get_class($criterionBase));
        }
    }
}
