<?php
namespace App\Repositories\Eloquent\Criteria;

abstract class EloquentCriterion
{
    protected $base;
    abstract protected function getBaseClass():string;

    public function setBase($base) {
        $class = $this->getBaseClass();
        if (!$base instanceof $class) {
            $actualClass = get_class($base);
            throw new \InvalidArgumentException("Criterion base must be of type {$class}. {$actualClass} provided.");
        }
        $this->base = $base;
        return $this;
    }

    public function getBase()
    {
        return $this->base;
    }
}
