<?php
namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\WhereIn;

class EloquentWhereIn extends EloquentCriterion implements Criterion
{
    protected $whereInBase;

    protected function getBaseClass(): string
    {
        return WhereIn::class;
    }

    public function apply($entity)
    {
        return $entity->whereIn($this->getBase()->getColumn(), $this->getBase()->getHaystack());
    }

    public function getBase(): WhereIn
    {
        return $this->base;
    }

}
