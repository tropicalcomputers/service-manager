<?php
namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\DeviceLike;

class EloquentDeviceLike extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return DeviceLike::class;
    }

    public function getBase(): DeviceLike
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->like($this->getBase()->getTerm());
    }
}
