<?php
namespace App\Repositories\Eloquent\Criteria\Device;

use App\Repositories\Eloquent\Criteria\EloquentCriterion;
use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryContracts\CustomerRepository;
use App\Repositories\RepositoryCriteria\Device\BelongsToCustomer;

class EloquentBelongsToCustomer extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return BelongsToCustomer::class;
    }

    public function getBase(): BelongsToCustomer
    {
        return $this->base;
    }

    public function apply($entity)
    {
        //TODO: refactor obvz
        /** @var CustomerRepository $customerRepo */
        $customerRepo = app()->make(CustomerRepository::class);
        $customer = $customerRepo->find($this->getBase()->getCustomerId());

        if ($company = $customer->company) {
            $deviceIds = $company->devices->pluck('id')->toArray();
        } else {
            $deviceIds = $customer->devices->pluck('id')->toArray();
        }

        return $entity->whereIn('id', $deviceIds);
    }
}
