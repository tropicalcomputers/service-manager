<?php
namespace App\Repositories\Eloquent\Criteria\Device;

use App\Repositories\Eloquent\Criteria\EloquentCriterion;
use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\Device\HasPassword;

class EloquentHasPassword extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return HasPassword::class;
    }

    public function getBase(): HasPassword
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->whereNotNull('password')
            ->where('password','<>','');
    }
}
