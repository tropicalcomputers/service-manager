<?php
namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\CompanyLike;

class EloquentCompanyLike extends EloquentCriterion implements Criterion
{

    protected function getBaseClass(): string
    {
        return CompanyLike::class;
    }

    public function getBase(): CompanyLike
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->like($this->getBase()->getTerm());
    }
}
