<?php
namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\Limit;

class EloquentLimit extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return Limit::class;
    }

    public function getBase(): Limit
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->limit($this->getBase()->getMax());
    }
}
