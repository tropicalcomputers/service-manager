<?php
namespace App\Repositories\Eloquent\Criteria\Common;


use App\Repositories\Eloquent\Criteria\EloquentCriterion;
use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\Common\ByWorkItemId;

class EloquentByWorkItemId extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return ByWorkItemId::class;
    }

    public function getBase(): ByWorkItemId
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->where('work_item_id', $this->getBase()->getWorkItemId());
    }
}
