<?php
namespace App\Repositories\Eloquent\Criteria\Common;

use App\Repositories\Eloquent\Criteria\EloquentCriterion;
use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\ColumnTextSearch;

class EloquentColumnTextSearch extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return ColumnTextSearch::class;
    }

    public function getBase(): ColumnTextSearch
    {
        return $this->base;
    }

    public function apply($entity)
    {
        if (($term = $this->getBase()->getTerm()) && ($columns = $this->getBase()->getColumns())) {
            if (!empty($columns)) {
                $entity->where(function($query) use ($term, $columns) {
                    $query->where(array_shift($columns), 'like', '%' . $term . '%');
                    foreach ($columns as $column) {
                        if (stripos($column,'.')) {
                            $query = $this->relationSearch($query, $column, $term);
                        } else {
                            $query = $query->orWhere($column, 'like', '%' . $term . '%');
                        }
                    }
                });
            }
        }
        return $entity;
    }

    protected function relationSearch($builder, $column, $term)
    {
        $relationship = explode('.',$column);
        switch(count($relationship)) {
            case 2:
                $builder = $builder->orWhereHas($relationship[0], function($query) use ($relationship, $term) {
                    $query->where($relationship[1], 'like', '%' . $term . '%');
                });
                break;
            case 3:
                $builder = $builder->orWhereHas($relationship[0], function($query) use ($relationship, $term) {
                    $query->whereHas($relationship[1], function($query) use ($relationship, $term) {
                        $query->where($relationship[2], 'like', '%' . $term . '%');
                    });
                });
                break;
        }

        return $builder;

    }
}
