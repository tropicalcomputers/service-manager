<?php
namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\Latest;
use App\Repositories\RepositoryCriteria\Limit;

class EloquentLatest extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return Latest::class;
    }

    public function apply($entity)
    {
        return $entity->orderBy('id','desc');
    }
}
