<?php
namespace App\Repositories\Eloquent\Criteria\WorkItem;

use App\Repositories\Eloquent\Criteria\EloquentCriterion;
use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\WorkItem\WithBranch;

class EloquentWithBranch extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return WithBranch::class;
    }

    public function getBase(): WithBranch
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->whereIn('branch_id',$this->getBase()->getBranches());
    }
}
