<?php
namespace App\Repositories\Eloquent\Criteria\WorkItem;

use App\Repositories\Eloquent\Criteria\EloquentCriterion;
use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\WorkItem\WithWarranty;

class EloquentWithWarranty extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return WithWarranty::class;
    }

    public function getBase(): WithWarranty
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->where('warranty',$this->getBase()->getWarrantyFlag());
    }
}
