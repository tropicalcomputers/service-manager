<?php
namespace App\Repositories\Eloquent\Criteria\WorkItem;

use App\Repositories\Eloquent\Criteria\EloquentCriterion;
use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\WorkItem\AssignedTo;

class EloquentAssignedTo extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return AssignedTo::class;
    }

    public function getBase(): AssignedTo
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->whereIn('assigned_to',$this->getBase()->getUserIds());
    }
}
