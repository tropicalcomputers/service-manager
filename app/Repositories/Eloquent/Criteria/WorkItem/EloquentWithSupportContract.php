<?php
namespace App\Repositories\Eloquent\Criteria\WorkItem;

use App\Repositories\Eloquent\Criteria\EloquentCriterion;
use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\WorkItem\WithSupportContract;

class EloquentWithSupportContract extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return WithSupportContract::class;
    }

    public function getBase(): WithSupportContract
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->where('support_contract',$this->getBase()->getSupportContractFlag());
    }
}
