<?php
namespace App\Repositories\Eloquent\Criteria\WorkItem;

use App\Repositories\Eloquent\Criteria\EloquentCriterion;
use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\WorkItem\IsBilled;
use App\ServiceManager\Billing\Billing;
use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem;

class EloquentIsBilled extends EloquentCriterion implements Criterion
{
    protected $billingTable;
    protected $table;
    protected $entity;

    protected function getBaseClass(): string
    {
        return IsBilled::class;
    }

    public function getBase(): IsBilled
    {
        return $this->base;
    }

    public function apply($entity)
    {
        $this->entity = $entity;
        $this->billingTable = (new Billing())->getTable();
        $this->table = (new EloquentWorkItem())->getTable();
        return $this->getBase()->isBilled() ? $this->billed(): $this->notBilled();
    }

    protected function billed()
    {
        return $this->entity->whereIn('id',function($query) {
            return $query
                ->select('work_item_id')
                ->distinct()
                ->from($this->billingTable)
                ->get();
        });
    }

    protected function notBilled()
    {
        return $this->entity->whereNotIn('id',function($query) {
            return $query
                ->select('work_item_id')
                ->distinct()
                ->from($this->billingTable)
                ->get();
        });
    }
}
