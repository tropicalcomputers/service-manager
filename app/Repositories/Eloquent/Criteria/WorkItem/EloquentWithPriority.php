<?php
namespace App\Repositories\Eloquent\Criteria\WorkItem;

use App\Repositories\Eloquent\Criteria\EloquentCriterion;
use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\WorkItem\WithPriority;

class EloquentWithPriority extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return WithPriority::class;
    }

    public function getBase(): WithPriority
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->whereIn('priority_id',$this->getBase()->getPriorities());
    }
}
