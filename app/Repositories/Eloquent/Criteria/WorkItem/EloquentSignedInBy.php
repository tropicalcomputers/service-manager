<?php
namespace App\Repositories\Eloquent\Criteria\WorkItem;

use App\Repositories\Eloquent\Criteria\EloquentCriterion;
use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\WorkItem\SignedInBy;

class EloquentSignedInBy extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return SignedInBy::class;
    }

    public function getBase(): SignedInBy
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->whereIn('signed_in_by',$this->getBase()->getUserIds());
    }
}
