<?php
namespace App\Repositories\Eloquent\Criteria\WorkItem;

use App\Repositories\Eloquent\Criteria\EloquentCriterion;
use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\WorkItem\DateRange;

class EloquentDateRange extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return DateRange::class;
    }

    public function getBase(): DateRange
    {
        return $this->base;
    }
    public function apply($entity)
    {
        return $entity
            ->whereDate('updated_at','>=',$this->getBase()->getStart()->startOfDay())
            ->whereDate('updated_at','<=',$this->getBase()->getEnd()->endOfDay());
    }
}
