<?php
namespace App\Repositories\Eloquent\Criteria\WorkItem;

use App\Repositories\Eloquent\Criteria\EloquentCriterion;
use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\WorkItem\WithStatus;

class EloquentWithStatus extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return WithStatus::class;
    }

    public function getBase(): WithStatus
    {
        return $this->base;
    }
    public function apply($entity)
    {
        return $entity->whereIn('status_id',$this->getBase()->getStatuses());
    }
}
