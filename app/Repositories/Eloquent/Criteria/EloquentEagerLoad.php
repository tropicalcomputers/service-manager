<?php
namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\EagerLoad;

class EloquentEagerLoad extends EloquentCriterion implements Criterion
{
    protected $eagerLoadBase;

    protected function getBaseClass(): string
    {
        return EagerLoad::class;
    }

    public function getBase(): EagerLoad
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->with($this->getBase()->getRelations());
    }

}
