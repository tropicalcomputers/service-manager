<?php
namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\FilteredDeviceManufacturers;

class EloquentFilteredDeviceManufacturers extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return FilteredDeviceManufacturers::class;
    }

    public function getBase(): FilteredDeviceManufacturers
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->manufacturerLike($this->getBase()->getTerm());
    }
}
