<?php
namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\Where;

class EloquentWhere implements Criterion
{
    protected function getBaseClass(): string
    {
        return Where::class;
    }

    public function getBase(): Where
    {
        return $this->base;
    }
    public function apply($entity)
    {
        return $entity->where($this->getBase()->getColumn(), $this->getBase()->getOperator(), $this->getBase()->getTerm());
    }
}
