<?php
namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\OrderBy;

class EloquentOrderBy extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return OrderBy::class;
    }

    public function getBase(): OrderBy
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->orderBy($this->getBase()->getColumn(), $this->getBase()->getDirection());
    }

}
