<?php
namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\CustomerLike;

class EloquentCustomerLike extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return CustomerLike::class;
    }

    public function getBase(): CustomerLike
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->like($this->getBase()->getTerm());
    }
}
