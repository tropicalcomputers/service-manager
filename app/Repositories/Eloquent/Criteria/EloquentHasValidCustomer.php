<?php
namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\HasValidCustomer;

class EloquentHasValidCustomer extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return HasValidCustomer::class;
    }

    public function getBase(): HasValidCustomer
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->has('customer');
    }
}
