<?php
namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\FilteredDeviceModels;

class EloquentFilteredDeviceModels extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return FilteredDeviceModels::class;
    }

    public function getBase(): FilteredDeviceModels
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->modelLike($this->getBase()->getTerm(), $this->getBase()->getManufacturer());
    }
}
