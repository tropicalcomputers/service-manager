<?php
namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\RepositoryBase\Criteria\Criterion;
use App\Repositories\RepositoryCriteria\Contains;

class EloquentContains extends EloquentCriterion implements Criterion
{
    protected function getBaseClass(): string
    {
        return Contains::class;
    }

    public function getBase(): Contains
    {
        return $this->base;
    }

    public function apply($entity)
    {
        return $entity->where($this->getBase()->getColumn(), 'LIKE', "%" . $this->getBase()->getTerm() . "%");
    }
}
