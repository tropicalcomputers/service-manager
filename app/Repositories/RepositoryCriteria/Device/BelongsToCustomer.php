<?php
namespace App\Repositories\RepositoryCriteria\Device;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class BelongsToCustomer implements CriterionBase
{
    protected $customerId;

    public function setCustomerId($customerId): BelongsToCustomer
    {
        $this->customerId = $customerId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }
}
