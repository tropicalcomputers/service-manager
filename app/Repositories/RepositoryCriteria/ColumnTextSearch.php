<?php
namespace App\Repositories\RepositoryCriteria;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class ColumnTextSearch implements CriterionBase
{
    protected $columns = [];
    protected $term;

    public function setColumns(array $columns): ColumnTextSearch
    {
        $this->columns = $columns;
        return $this;
    }

    public function setTerm(string $term): ColumnTextSearch
    {
        $this->term = $term;
        return $this;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @return string
     */
    public function getTerm(): string
    {
        return $this->term;
    }
}
