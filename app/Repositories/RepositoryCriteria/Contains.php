<?php
namespace App\Repositories\RepositoryCriteria;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class Contains implements CriterionBase
{
    protected $column;
    protected $term;

    public function __construct(string $column, $term)
    {
        $this->column = $column;
        $this->term = $term;
    }

    /**
     * @return string
     */
    public function getColumn(): string
    {
        return $this->column;
    }

    /**
     * @return array
     */
    public function getTerm()
    {
        return $this->term;
    }
}
