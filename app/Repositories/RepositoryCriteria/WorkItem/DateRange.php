<?php
namespace App\Repositories\RepositoryCriteria\WorkItem;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;
use Carbon\Carbon;

class DateRange implements CriterionBase
{
    protected $start;
    protected $end;

    public function setStart(Carbon $start): DateRange
    {
        $this->start = $start;
        return $this;
    }


    public function setEnd(Carbon $end): DateRange
    {
        $this->end = $end;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getStart(): Carbon
    {
        return $this->start;
    }

    /**
     * @return Carbon
     */
    public function getEnd(): Carbon
    {
        return $this->end;
    }
}
