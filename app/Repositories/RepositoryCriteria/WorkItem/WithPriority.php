<?php
namespace App\Repositories\RepositoryCriteria\WorkItem;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class WithPriority implements CriterionBase
{
    protected $priorities = [];

    public function setPriorities(array $priorities): WithPriority
    {
        $this->priorities = $priorities;
        return $this;
    }

    /**
     * @return array
     */
    public function getPriorities(): array
    {
        return $this->priorities;
    }
}
