<?php
namespace App\Repositories\RepositoryCriteria\WorkItem;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class SignedInBy implements CriterionBase
{
    protected $userIds;

    public function setUserIds(array $userIds): SignedInBy
    {
        $this->userIds = $userIds;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserIds()
    {
        return $this->userIds;
    }
}
