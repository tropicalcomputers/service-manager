<?php
namespace App\Repositories\RepositoryCriteria\WorkItem;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class AssignedTo implements CriterionBase
{
    protected $userIds;

    public function setUserIds($userIds): AssignedTo
    {
        $this->userIds = $userIds;
        return $this;
    }

    /**
     * @return array
     */
    public function getUserIds(): array
    {
        return $this->userIds;
    }
}
