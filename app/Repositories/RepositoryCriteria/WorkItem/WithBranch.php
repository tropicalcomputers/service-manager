<?php
namespace App\Repositories\RepositoryCriteria\WorkItem;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class WithBranch implements CriterionBase
{
    protected $branches = [];

    public function setBranches(array $branches): WithBranch
    {
        $this->branches = $branches;
        return $this;
    }

    /**
     * @return array
     */
    public function getBranches(): array
    {
        return $this->branches;
    }
}
