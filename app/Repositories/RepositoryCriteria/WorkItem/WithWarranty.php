<?php
namespace App\Repositories\RepositoryCriteria\WorkItem;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class WithWarranty implements CriterionBase
{
    protected $warrantyFlag;

    public function setWarrantyFlag(bool $warrantyFlag): WithWarranty
    {
        $this->warrantyFlag = $warrantyFlag;
        return $this;
    }

    /**
     * @return bool
     */
    public function getWarrantyFlag(): bool
    {
        return $this->warrantyFlag;
    }
}
