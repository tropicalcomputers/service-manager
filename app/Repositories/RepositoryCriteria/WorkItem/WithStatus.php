<?php
namespace App\Repositories\RepositoryCriteria\WorkItem;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class WithStatus implements CriterionBase
{
    protected $statuses = [];
    protected $term;

    public function setStatuses(array $statuses): WithStatus
    {
        $this->statuses = $statuses;
        return $this;
    }

    /**
     * @return array
     */
    public function getStatuses(): array
    {
        return $this->statuses;
    }
}
