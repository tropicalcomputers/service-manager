<?php
namespace App\Repositories\RepositoryCriteria\WorkItem;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class WithSupportContract implements CriterionBase
{
    protected $supportContractFlag;

    public function setSupportContractFlag(bool $supportContractFlag): WithSupportContract
    {
        $this->supportContractFlag = $supportContractFlag;
        return $this;
    }

    /**
     * @return bool
     */
    public function getSupportContractFlag(): bool
    {
        return $this->supportContractFlag;
    }
}
