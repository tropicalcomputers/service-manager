<?php
namespace App\Repositories\RepositoryCriteria\WorkItem;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class IsBilled implements CriterionBase
{
    protected $billed;

    public function setBilled(bool $billed): IsBilled
    {
        $this->billed = $billed;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBilled(): bool
    {
        return $this->billed;
    }
}
