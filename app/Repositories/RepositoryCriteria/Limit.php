<?php
namespace App\Repositories\RepositoryCriteria;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class Limit implements CriterionBase
{
    protected $max;

    public function __construct($max)
    {
        $this->max = $max;
    }

    /**
     * @return array
     */
    public function getMax()
    {
        return $this->max;
    }
}
