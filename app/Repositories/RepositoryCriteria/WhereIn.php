<?php
namespace App\Repositories\RepositoryCriteria;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class WhereIn implements CriterionBase
{
    protected $column;
    protected $haystack;

    public function __construct(string $column, array $haystack)
    {
        $this->column = $column;
        $this->haystack = $haystack;
    }

    /**
     * @return string
     */
    public function getColumn(): string
    {
        return $this->column;
    }

    /**
     * @return array
     */
    public function getHaystack()
    {
        return $this->haystack;
    }
}
