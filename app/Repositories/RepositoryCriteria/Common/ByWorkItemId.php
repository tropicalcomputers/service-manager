<?php
namespace App\Repositories\RepositoryCriteria\Common;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class ByWorkItemId implements CriterionBase
{
    protected $workItemId;

    public function setWorkItemId(string $workItemId): ByWorkItemId
    {
        $this->workItemId = $workItemId;
        return $this;
    }

    /**
     * @return int
     */
    public function getWorkItemId()
    {
        return $this->workItemId;
    }
}
