<?php
namespace App\Repositories\RepositoryCriteria;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class CustomerLike implements CriterionBase
{
    protected $term;

    public function setTerm(string $term): CustomerLike
    {
        $this->term = $term;
        return $this;
    }

    /**
     * @return string
     */
    public function getTerm(): string
    {
        return $this->term;
    }
}
