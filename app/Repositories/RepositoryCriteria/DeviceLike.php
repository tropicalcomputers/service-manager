<?php
namespace App\Repositories\RepositoryCriteria;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class DeviceLike implements CriterionBase
{
    protected $term;

    public function setTerm(string $term): DeviceLike
    {
        $this->term = $term;
        return $this;
    }

    /**
     * @return array
     */
    public function getTerm()
    {
        return $this->term;
    }
}
