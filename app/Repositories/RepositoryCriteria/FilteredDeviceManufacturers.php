<?php
namespace App\Repositories\RepositoryCriteria;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class FilteredDeviceManufacturers implements CriterionBase
{
    protected $term;

    public function __construct($term)
    {
        $this->term = $term;
    }

    /**
     * @return array
     */
    public function getTerm()
    {
        return $this->term;
    }
}
