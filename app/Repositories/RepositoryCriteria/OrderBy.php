<?php
namespace App\Repositories\RepositoryCriteria;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class OrderBy implements CriterionBase
{
    protected $column;
    protected $direction;

    public function __construct()
    {
    }

    public function setColumn(string $column): OrderBy
    {
        $this->column = $column;
        return $this;
    }

    public function setDirection(string $direction = 'asc'): OrderBy
    {
        $this->direction = $direction;
        return $this;
    }

    /**
     * @return string
     */
    public function getColumn(): string
    {
        return $this->column;
    }

    /**
     * @return string
     */
    public function getDirection(): string
    {
        return $this->direction;
    }
}
