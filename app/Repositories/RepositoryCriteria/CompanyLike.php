<?php
namespace App\Repositories\RepositoryCriteria;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class CompanyLike implements CriterionBase
{
    protected $term;

    public function __construct($term = null)
    {
        $this->term = $term;
    }

    public function setTerm(string $term): CompanyLike
    {
        $this->term = $term;
        return $this;
    }

    /**
     * @return array
     */
    public function getTerm()
    {
        return $this->term;
    }
}
