<?php
namespace App\Repositories\RepositoryCriteria;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class FilteredDeviceModels implements CriterionBase
{
    protected $term;
    protected $manufacturer;

    public function __construct(string $term = null, string $manufacturer = null)
    {
        $this->term = $term;
        $this->manufacturer = $manufacturer;
    }

    /**
     * @return string|null
     */
    public function getTerm(): ?string
    {
        return $this->term;
    }

    /**
     * @return string|null
     */
    public function getManufacturer(): ?string
    {
        return $this->manufacturer;
    }

}
