<?php
namespace App\Repositories\RepositoryCriteria;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

class Where implements CriterionBase
{
    protected $column;
    protected $operator;
    protected $term;

    public function __construct(string $column, string $operator, $term)
    {
        $this->column = $column;
        $this->operator = $operator;
        $this->term = $term;
    }

    /**
     * @return string
     */
    public function getColumn(): string
    {
        return $this->column;
    }

    /**
     * @return string
     */
    public function getOperator(): string
    {
        return $this->operator;
    }


    /**
     * @return array
     */
    public function getTerm()
    {
        return $this->term;
    }
}
