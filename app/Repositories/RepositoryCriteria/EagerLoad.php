<?php
namespace App\Repositories\RepositoryCriteria;
use App\Repositories\RepositoryBase\Criteria\CriterionBase;

/**
 * Class EagerLoad
 * @package App\Repositories\RepositoryCriteria
 */
class EagerLoad implements CriterionBase
{
    /**
     * @var string[]
     */
    protected $relations;

    /**
     * EagerLoad constructor.
     * @param string[] $relations
     */
    public function __construct(array $relations)
    {
        $this->relations = $relations;
    }

    /**
     * @return string[]
     */
    public function getRelations(): array
    {
        return $this->relations;
    }

}
