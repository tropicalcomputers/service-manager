<?php


namespace App\Repositories\RepositoryBase\Criteria;

interface Criterion
{
    public function apply($entity);
    public function setBase($base);
    public function getBase();
}
