<?php


namespace App\Repositories\RepositoryBase\Criteria;


use App\Repositories\RepositoryBase\Repository;

interface Criteria
{
    public function withCriteria(...$criteriaNames): Repository;
}
