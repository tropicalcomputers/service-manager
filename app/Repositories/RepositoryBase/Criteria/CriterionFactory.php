<?php


namespace App\Repositories\RepositoryBase\Criteria;


interface CriterionFactory
{
    public function make(CriterionBase $criterionBase): Criterion;
}
