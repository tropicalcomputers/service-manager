<?php

namespace App\Repositories\RepositoryBase;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface Repository {

    /**
     * @param mixed ...$criteria
     * @return $this
     */
    public function withCriteria(...$criteria): Repository ;

    /**
     * @param mixed ...$relations
     * @return $this
     */
    public function withRelations(...$relations): Repository;

    /**
     * @param int $limit
     * @return $this
     */
    public function withLimit(int $limit): Repository;

    /**
     * @param int $offset
     * @return $this
     */
    public function withOffset(int $offset);

    /**
     * @param  string  $column
     * @param  string  $direction
     * @return $this
     */
    public function withOrderBy($column, $direction = 'asc');

    /**
     * @param int $count
     * @param callable $callback
     */
    public function chunk(int $count, callable $callback);

    /**
     * @param array $ids
     * @return $this
     */
    public function whereIds(array $ids);

    /**
     * @param mixed $id
     * @return object|null
     */
    public function find($id);

    /**
     * @param mixed $id
     * @return object
     * @throws \Exception
     */
    public function findOrFail($id);

    /**
     *  //TODO: discuss: eloquent specific?
     * @return mixed
     */
    public function newInstance();

    /**
     * @return Collection
     */
    public function get(): Collection;

    /**
     * @return object|null
     */
    public function first();

    /**
     * @return object
     * @throws \Exception
     */
    public function firstOrFail();

    /**
     * @return int
     */
    public function count(): int;

    /**
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function paginate($perPage = 15): LengthAwarePaginator;

    /**
     * @param mixed $id
     * @param array $attributes
     * @return bool
     * @throws \RuntimeException if model not found
     */
    public function update($id, array $attributes): bool;

    /**
     * @param array $attributes
     * @param array|null $ids
     * @return int count of affected rows
     */
    public function massUpdate(array $attributes, array $ids = null): int;

    /**
     * @param mixed $id
     * @return bool|null
     */
    public function delete($id): ?bool;

    /**
     * @param array|null $ids
     * @return int
     * @throws \Exception
     */
    public function massDelete(array $ids = null): int;

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes);

    /**
     *  //TODO: discuss: eloquent specific?
     * @param \stdClass $model
     * @param array $attributes
     * @return \stdClass
     */
    public function saveModel($model, array $attributes);

    /**
     * @return bool
     */
    public function exists() : bool;



}
