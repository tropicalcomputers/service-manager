<?php

namespace App\Providers;

use App\Repositories\Eloquent\EloquentCriterionFactory;
use App\Repositories\RepositoryBase\Criteria\CriterionFactory;
use App\Repositories\RepositoryContracts\CompanyRepository;
use App\Repositories\RepositoryContracts\CustomerRepository;
use App\Repositories\RepositoryContracts\DeviceRepository;
use App\Repositories\RepositoryContracts\UserRepository;
use App\Repositories\RepositoryContracts\WorkItemHistoryRepository;
use App\Repositories\RepositoryContracts\WorkItemRepository;
use App\ServiceManager\Company\EloquentCompanyRepository;
use App\ServiceManager\Customer\EloquentCustomerRepository;
use App\ServiceManager\Device\EloquentDeviceRepository;
use App\ServiceManager\Part\Eloquent\EloquentPartRepository;
use App\ServiceManager\Part\PartRepository;
use App\ServiceManager\User\Eloquent\EloquentUserRepository;
use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItemRepository;
use App\ServiceManager\WorkItemHistory\EloquentWorkItemHistoryRepository;
use App\Utilities\Encryption\StringCrypt;
use App\Utilities\Encryption\StringCryptInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(
            [
                EloquentUserRepository::class,
                EloquentCompanyRepository::class,
                EloquentCustomerRepository::class,
                EloquentDeviceRepository::class,
                EloquentWorkItemRepository::class,
                EloquentWorkItemHistoryRepository::class
            ]
        )
            ->needs(CriterionFactory::class)
            ->give(EloquentCriterionFactory::class);

        $this->app->bind(WorkItemHistoryRepository::class, EloquentWorkItemHistoryRepository::class);
        $this->app->bind(WorkItemRepository::class, EloquentWorkItemRepository::class);
        $this->app->bind(CompanyRepository::class, EloquentCompanyRepository::class);
        $this->app->bind(CustomerRepository::class, EloquentCustomerRepository::class);
        $this->app->bind(DeviceRepository::class, EloquentDeviceRepository::class);
        $this->app->bind(UserRepository::class, EloquentUserRepository::class);
        $this->app->bind(PartRepository::class, EloquentPartRepository::class);
        $this->app->singleton(StringCryptInterface::class, StringCrypt::class);





    }
}
