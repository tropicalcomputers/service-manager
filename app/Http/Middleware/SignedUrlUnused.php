<?php


namespace App\Http\Middleware;


use Closure;
use Illuminate\Routing\Exceptions\InvalidSignatureException;

class SignedUrlUnused
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($user = $request->route('user')) {
            if ($request->fullUrl() !== $user->signed_url) {
                throw new InvalidSignatureException();
            }
            $user->signed_url = null;
            $user->save();
        }
        return $next($request);
    }
}
