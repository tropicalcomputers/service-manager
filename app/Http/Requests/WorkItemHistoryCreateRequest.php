<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkItemHistoryCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "work_item_id"=>"required|exists:work_items,id",
            "branch_id"=>"required|exists:branches,id",
            "status_id"=>"required|exists:statuses,id",
            "changed_by"=>"required|exists:users,id",
            "assigned_to"=>"required|exists:users,id",
            "description"=>"required|string",
            "customer_contacted"=>"sometimes|boolean",
            "time"=>"required|integer",
            "part" => "sometimes|array",
            "part.serial"=>"required_with:part.description|string",
            "part.description"=>"required_with:part.serial|string",
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            "work_item_id"=>"work item",
            "branch_id"=>"branch",
            "status_id"=>"status",
            "changed_by"=>"changed by",
            "assigned_to"=>"assigned to",
            "customer_contacted"=>"customer contacted",
            "time"=>"time spent",
        ];
    }
}
