<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => 'nullable|integer|exists:companies,id',
            'first_name' => 'required|max:35',
            'last_name' => 'required|max:35',
            'address' => 'sometimes|required_if:customer_type,Individual|max:250|nullable|string',
            'email' => 'nullable|email',
            'home_telephone' => 'nullable|present|string|max:20',
            'work_telephone' => 'nullable|present|string|max:20',
            'mobile_telephone' => 'nullable|present|string|max:20',
            ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'company_id.exists' => 'No valid company specified for company contact.',
            'company_id.integer' => 'A valid Company is required when adding a company contact.',
            'address.required' => 'Person/Contact address is required.',
            'email.email' => 'Email address must be a valid format.',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'address' => 'Customer Address',
        ];
    }
}
