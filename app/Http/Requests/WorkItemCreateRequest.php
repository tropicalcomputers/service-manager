<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkItemCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "workItem.customer_id"=>"required|exists:customers,id",
            "workItem.device_id"=>"required|exists:devices,id",
            "workItem.priority_id"=>"required|exists:priorities,id",
            "workItem.notes"=>"nullable|present|string",
            "workItem.warranty"=>"boolean|required",
            "workItem.support_contract"=>"boolean|required",
            "workItem.inventory_notes"=>"nullable|present|string",

            "device.password"=>"nullable|present|string",

            "workItem.description"=>"required|string",
            "workItem.branch_id"=>"required|integer",

            "inventory"=>"sometimes|array",
            "inventory.*"=>"exists:inventory_options,id",
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'workItem.description' => 'Work Item Description',
            'customer.person.id' => 'Customer',
            'device.id' => 'Device or System',
            "device.password"=>"Password",
            "info.branch.id"=>"Branch",
            "info.priority"=>"Priority",
            "info.description"=>"Description",
            "info.notes"=>"Note",
            "inventory.deviceType"=>"Device Type",
            "inventory.components"=>"Components",
            "inventory.additionalInformation"=>"Additional Information",
        ];
    }
}
