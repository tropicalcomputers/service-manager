<?php

namespace App\Http\Controllers;

use App\ServiceManager\Device\DeviceType;
use App\Transformers\DeviceTransformer;
use App\Transformers\DeviceTypeTransformer;
use Illuminate\Http\Request;

class DeviceTypeController extends ApiController
{
    protected $deviceTypeTransformer;

    public function __construct(DeviceTypeTransformer $deviceTypeTransformer)
    {
        $this->deviceTypeTransformer = $deviceTypeTransformer;
    }

    public function index()
    {
        if (!request()->expectsJson()) {
            return view('devices.device-types-index');
        }

        return fractal()
            ->collection(DeviceType::all())
            ->transformWith($this->deviceTypeTransformer)
            ->respond();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $attributes = $this->validate($request, [
            'name' => 'required|string|unique:device_types,name'
        ]);
        $deviceType = DeviceType::create($attributes);
        return fractal()
            ->item($deviceType)
            ->transformWith($this->deviceTypeTransformer)
            ->respond()->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  DeviceTransformer $deviceTransformer
     * @pram $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function update(Request $request, DeviceType $deviceType)
    {
        $attributes = $this->validate($request, [
            'name' => 'string|unique:device_types,name'
        ]);

        $deviceType->update($attributes);
        $deviceType->refresh();

        return fractal()
            ->item($deviceType)
            ->transformWith(new DeviceTypeTransformer())
            ->respond();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DeviceType $deviceType
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeviceType $deviceType)
    {
        $deviceType->delete();
        return response()->noContent();
    }
}
