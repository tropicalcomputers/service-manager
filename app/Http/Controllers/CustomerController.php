<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerCreateRequest;
use App\Repositories\RepositoryContracts\CustomerRepository;
use App\Repositories\RepositoryCriteria\CustomerLike;
use App\Repositories\RepositoryCriteria\DeviceLike;
use App\Repositories\RepositoryCriteria\EagerLoad;
use App\Repositories\RepositoryCriteria\Latest;
use App\Repositories\RepositoryCriteria\Limit;
use App\ServiceManager\Customer\Customer;
use App\Transformers\ContactTransformer;
use App\Transformers\CustomerTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CustomerController extends Controller
{
    protected $customerRepository;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $criteria = [new Latest(), new EagerLoad(['company'])];

        if ($term = $request->get('term')) {
            $criteria[] = (new CustomerLike())->setTerm($term);
        }

        if ($pageSize = $request->get('page_size', 5)) {
            $criteria[] = new Limit($pageSize);
        }

        $devices = $this->customerRepository->withCriteria(
            $criteria
        )->get();

        $transformedCustomers = fractal()
            ->collection($devices)
            ->transformWith(new CustomerTransformer())
            ->parseIncludes(['company'])
            ->toArray();

        return response()->json($transformedCustomers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function store(CustomerCreateRequest $request)
    {
        $validatedData = $request->validated();
        $customer = $this->customerRepository->create($validatedData);
        $customer = $this->customerRepository->withRelations(['company'])->find($customer->id);
        $transformedCustomer = $this->getTransformedCustomer($customer);
        return response()->json($transformedCustomer)->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = $this->getCustomer($id,['company']);
        $personTransformed = $customer->company ? $this->getTransformedContact($customer) : $this->getTransformedCustomer($customer);
        return $customer->company ?
            view('companies.contact-show',['contactJson'=>json_encode($personTransformed['data']), 'contact'=>$customer]) :
            view('customers.show',['customerJson'=>json_encode($personTransformed['data']), 'customer'=>$customer]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->customerRepository->findOrFail($id);
        $validated = $this->validate($request, [
            'first_name' => 'required|string|min:2|max:30',
            'last_name' => 'required|string|min:2|max:30',
            'address' => 'nullable|string|min:2|max:150',
            'email' => 'email|min:7|max:100',
            'home_telephone' => 'nullable|string|min:7|max:32',
            'work_telephone' => 'nullable|string|min:7|max:32',
            'mobile_telephone' => 'nullable|string|min:7|max:32',
        ]);
        $this->customerRepository->update($id,$validated);
        $customer = $this->getCustomer($id, ['company']);
        $transformedCustomer = $this->getTransformedCustomer($customer);
        return response()->json($transformedCustomer)->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function getCustomer($id, array $eagerLoad = []): Customer
    {
        return $this->customerRepository->withRelations($eagerLoad)->find($id);
    }

    protected function getTransformedCustomer(Customer $customer, $includes = ['devices'], $isContact = false)
    {
        if ($customer->company) {
            $transformer = new ContactTransformer();
            $includes[] = 'company';
        } else {
            $transformer = new CustomerTransformer();

        }

        return fractal()
            ->item($customer)
            ->transformWith($transformer)
            ->parseIncludes($includes)
            ->toArray();
    }

    protected function getTransformedContact(Customer $contact, $includes = ['company'])
    {
        return $this->getTransformedCustomer($contact, $includes, true);
    }
}
