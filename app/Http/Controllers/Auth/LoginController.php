<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\ServiceManager\User\Eloquent\UserModel;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/work-items/create';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
           'email'=>'required|email|exists:users'
        ]);

        $expireMinutes = config('emailLoginLinkExpirationMinutes',15);
        if (!$user = UserModel::whereEmail($request->get('email'))
            ->where('enabled',true)->first()) {
            return back()->withInput()->withErrors(['email'=>'The selected email is invalid.']);

        }
        $signedUrl = URL::temporarySignedRoute(
           'login.email',
           now()->addMinutes($expireMinutes),
           ['user'=>$user->id]
        );

        $user->signed_url = $signedUrl;
        $user->save();

        Mail::send(
           'auth.emails.login',
           [
               'signedUrl' => $signedUrl,
               'expireMinutes' => $expireMinutes,
           ],
           function (Message $message) use ($user) {
               $message->subject('TCL Service Manager Login Link');
               $message->to($user->email);
               $message->from(config('mail.from.address'), config('mail.from.name'));
           }
        );
        return view('auth.email-sent');
    }

    public function loginByEmail(UserModel $user)
    {
        if (!$user->isEnabled()) {
            abort(401, 'Whoops! User not enabled.');
        }
        Auth::login($user);
        return redirect()->route('dashboard.index');
    }
}
