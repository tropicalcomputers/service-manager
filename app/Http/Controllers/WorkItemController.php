<?php

namespace App\Http\Controllers;

use App\Http\Requests\WorkItemCreateRequest;
use App\Repositories\RepositoryContracts\CompanyRepository;
use App\Repositories\RepositoryContracts\CustomerRepository;
use App\Repositories\RepositoryContracts\DeviceRepository;
use App\Repositories\RepositoryContracts\WorkItemHistoryRepository;
use App\Repositories\RepositoryContracts\WorkItemRepository;
use App\Repositories\RepositoryCriteria\ColumnTextSearch;
use App\Repositories\RepositoryCriteria\EagerLoad;
use App\Repositories\RepositoryCriteria\HasValidCustomer;
use App\Repositories\RepositoryCriteria\OrderBy;
use App\Repositories\RepositoryCriteria\WorkItem\AssignedTo;
use App\Repositories\RepositoryCriteria\WorkItem\DateRange;
use App\Repositories\RepositoryCriteria\WorkItem\IsBilled;
use App\Repositories\RepositoryCriteria\WorkItem\SignedInBy;
use App\Repositories\RepositoryCriteria\WorkItem\WithBranch;
use App\Repositories\RepositoryCriteria\WorkItem\WithPriority;
use App\Repositories\RepositoryCriteria\WorkItem\WithSupportContract;
use App\Repositories\RepositoryCriteria\WorkItem\WithStatus;
use App\Repositories\RepositoryCriteria\WorkItem\WithWarranty;
use App\ServiceManager\Branch\Branch;
use App\ServiceManager\Device\Eloquent\EloquentDevice;
use App\ServiceManager\Device\DeviceType;
use App\ServiceManager\Priority\Priority;
use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem;
use App\ServiceManager\WorkItem\WorkItem;
use App\ServiceManager\WorkItem\WorkItemUnitOfWork;
use App\Transformers\WorkItemTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

/**
 * Class WorkItemController
 * @package App\Http\Controllers
 */
class WorkItemController extends Controller
{
    protected $workItemRepository;
    protected $workItemHistoryRepository;
    protected $companyRepository;
    protected $customerRepository;
    protected $deviceRepository;
    protected $workItemTransformer;
    protected $workItemUnitOfWork;


    public function __construct(
        WorkItemUnitOfWork $workItemUnitOfWork,
        WorkItemRepository $workItemRepository,
        CompanyRepository $companyRepository,
        CustomerRepository $customerRepository,
        WorkItemHistoryRepository $workItemHistoryRepository,
        DeviceRepository $deviceRepository,
        WorkItemTransformer $workItemTransformer
    )
    {
        $this->workItemUnitOfWork = $workItemUnitOfWork;
        $this->workItemRepository = $workItemRepository;
        $this->workItemHistoryRepository = $workItemHistoryRepository;
        $this->customerRepository = $customerRepository;
        $this->companyRepository = $companyRepository;
        $this->deviceRepository = $deviceRepository;
        $this->workItemTransformer = $workItemTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //TODO - validate request vars

        $length = request()->input('length');
        $column = request()->input('column');
        $status = request()->input('status');
        $branch = request()->input('branch');
        $billed = request()->input('billed');
        $priority = request()->input('priority');
        $warranty = request()->input('warranty');
        $serviceContract = request()->input('serviceContract');
        $signedInBy = request()->input('signedInBy');
        $assignedTo = request()->input('assignedTo');
        $dateRange = request()->input('dateRange');
        $dir = request()->input('dir');
        $searchValue = request()->input('search');

        if (request()->expectsJson()) {
            $criteria = [new HasValidCustomer()];
            if ($searchValue) {
                $criteria[] = (new ColumnTextSearch())->setTerm($searchValue)->setColumns([
                    'id',
                    'customer.first_name',
                    'customer.last_name',
                    'customer.company.name',
                    'device.serial',
                    ]);
            }

            if ($column) {
                $criteria[] = (new OrderBy())->setColumn($column)->setDirection($dir ?? 'asc');
            }

            if ($status) {
                switch ($status) {
                    case 5:
                        $statuses = [1,2,3,7]; // filter to incomplete statuses ('incomplete' statuses include active, awaiting customer, awaiting parts, rma)
                        break;
                    case 999:
                        $statuses = [11,10,9,8]; // filter to billable statuses (completed, awaiting collection etc)
                        break;
                    default:
                        $statuses = [$status];
                }

                $criteria[] = (new WithStatus())->setStatuses($statuses);
            }

            if ($priority) {
                $criteria[] = (new WithPriority())->setPriorities([$priority]);
            }

            if ($branch) {
                $criteria[] = (new WithBranch())->setBranches([$branch]);
            }

            if ($signedInBy) {
                $criteria[] = (new SignedInBy())->setUserIds([$signedInBy]);
            }

            if ($assignedTo) {
                $criteria[] = (new AssignedTo())->setUserIds([$assignedTo]);
            }

            if (!is_null($billed)) {
                $criteria[] = (new IsBilled())->setBilled($billed);
            }

            if (!is_null($warranty)) {
                $criteria[] = (new WithWarranty())->setWarrantyFlag($warranty);
            }

            if (!is_null($serviceContract)) {
                $criteria[] = (new WithSupportContract())->setSupportContractFlag($serviceContract);
            }

            if ($dateRange) {
                $dateRange = json_decode($dateRange, true);
                $criteria[] = (new DateRange())->setStart(Carbon::parse($dateRange['startDate']))->setEnd(Carbon::parse($dateRange['endDate']));
            }

            $workItems = $this->workItemRepository->withCriteria(
                $criteria
            )->withRelations(['customer.company','device','history','parts','assignedTo'])->paginate($length ?? 10);

            $transformedWorkItems = fractal()
                ->collection($workItems)
                ->transformWith($this->workItemTransformer)
                ->parseIncludes(['customer.company','device','workItemHistory','assigned_to'])
                ->paginateWith(new IlluminatePaginatorAdapter($workItems))
                ->addMeta(['draw'=> request()->input('draw')])
                ->toArray();
            return response()->json($transformedWorkItems);
        }

        return view('work-items.index',['filters'=>json_encode(request()->only('status'))]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $deviceTypes = DeviceType::with('inventoryOptions')->get();
        $branches = Branch::all();
        $priorities = Priority::all();
        $manufacturers = EloquentDevice::select('manufacturer')->distinct()->get()->pluck('manufacturer');
        $models = EloquentDevice::select('model')->distinct()->get()->pluck('model');

        return view('work-items.create',[
            'models' => $models,
            'manufacturers' => $manufacturers,
            'inventory'=>$deviceTypes,
            'branches'=> $branches,
            'priorities'=>$priorities
        ]);
    }

    public function store(WorkItemCreateRequest $request)
    {
        $attributes = $request->validated();
        $attributes['workItem'] += [
                'signed_in_by'=>Auth::id(),
                'assigned_to' => null,
                'status_id' => config('settings.defaultStatus',1),
                'time' => 0,
            ];

        $workItem = $this->workItemUnitOfWork->create($attributes);
        $transformedWorkItem = $this->getTransformedWorkItem($workItem);

        return response()->json($transformedWorkItem)->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $workItem = $this->getWorkItem($id);
        $transformedWorkItem = $this->getTransformedWorkItem($workItem);

        if (request()->expectsJson()) {
            return $transformedWorkItem;
        }
        return view('work-items.show', ['workItem' => json_encode($transformedWorkItem)]);
    }

    protected function getWorkItem($id, array $eagerLoad = null): EloquentWorkItem
    {
        $defaultEagerLoad = [
            'history',
            'customer',
            'device',
            'priority',
            'billings',
            'parts',
            'signedInBy',
            'assignedTo',
        ];
        $relations = is_array($eagerLoad) ? $eagerLoad : $defaultEagerLoad;
        return $this->workItemRepository->withRelations($relations)->find($id);
    }

    protected function getTransformedWorkItem(WorkItem $workItem)
    {
        return fractal()
            ->item($workItem)
            ->transformWith($this->workItemTransformer)
            ->parseIncludes(['customer.company','device','workItemHistory','billings','parts','signed_in_by','assigned_to'])
            ->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * @param Request $request
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $validated = $this->validate($request, [
            'priority' => 'integer|exists:priorities,id',
            'description' => 'string|min:2'
        ]);

        if ($workItem = $this->getWorkItem($id,[])) {
            foreach ($validated as $reference => $value) {
                switch($reference) {
                    case 'priority':
                        $workItem->priority_id = $value;
                        break;
                    case 'description':
                        $workItem->description = $value;
                        break;
                }
            }

            if ($workItem->isDirty()) {
                $workItem->save();
            }

            return $this->getTransformedWorkItem($this->getWorkItem($id));
        }

        abort(404, 'Work Item Not Found');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
