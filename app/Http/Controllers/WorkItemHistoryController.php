<?php

namespace App\Http\Controllers;

use App\Http\Requests\WorkItemHistoryCreateRequest;
use App\Repositories\RepositoryContracts\WorkItemHistoryRepository;
use App\Repositories\RepositoryContracts\WorkItemRepository;
use App\ServiceManager\WorkItemHistory\WorkItemHistory;
use App\Transformers\WorkItemHistoryTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WorkItemHistoryController extends ApiController
{
    protected $workItemRepository;
    protected $workItemHistoryRepository;
    protected $resourceTransformer;

    public function __construct(
        WorkItemRepository $workItemRepository,
        WorkItemHistoryRepository $workItemHistoryRepository,
        WorkItemHistoryTransformer $resourceTransformer
    )
    {
        $this->workItemRepository = $workItemRepository;
        $this->workItemHistoryRepository = $workItemHistoryRepository;
        $this->resourceTransformer = $resourceTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param WorkItemHistoryCreateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorkItemHistoryCreateRequest $request)
    {
        $workItemHistory = $this->workItemHistoryRepository->create($request->validated());
        $transformedWorkItemHistory = $this->getTransformed($workItemHistory);
        return response()->json($transformedWorkItemHistory)->setStatusCode(201);

    }

    public function storePart(Request $request)
    {
        $validated = $request->validate([
            'work_item_id' => 'exists:work_items,id',
            'description' => 'required|string',
            'serial' => 'nullable|string|present',
        ]);

        $workItemHistory = $this->workItemHistoryRepository->createPart($validated);

        if (request()->expectsJson()) {
            return redirect()->route('work-items.show', $workItemHistory->work_item_id)
                ->withHeaders(['Accept'=>'application/json']);
        }
        return redirect()->route('work-items.show', $workItemHistory->work_item_id);
    }

    /**
     * @param WorkItemHistory $workItemHistory
     */
    public function show(WorkItemHistory $workItemHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param WorkItemHistory $workItemHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkItemHistory $workItemHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param WorkItemHistory $workItemHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkItemHistory $workItemHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param WorkItemHistory $workItemHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkItemHistory $workItemHistory)
    {
        //
    }
}
