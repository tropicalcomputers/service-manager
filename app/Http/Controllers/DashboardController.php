<?php

namespace App\Http\Controllers;


use App\Transformers\StatusCountTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $statusCounts = fractal()
            ->item([])
            ->transformWith(new StatusCountTransformer())
            ->parseIncludes([])
            ->toArray();
        return view('dashboard.index', ['statusCountsJson' => json_encode($statusCounts)]);
    }
}
