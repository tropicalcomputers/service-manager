<?php

namespace App\Http\Controllers;

use App\Repositories\RepositoryContracts\WorkItemRepository;
use App\ServiceManager\Part\Eloquent\EloquentPart;
use App\ServiceManager\Part\PartUnitOfWork;
use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem;
use App\Transformers\PartTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class PartController
 * @package App\Http\Controllers
 */
class PartController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param PartUnitOfWork $partUnitOfWork
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PartUnitOfWork $partUnitOfWork)
    {
        $partProperties = $request->validate([
            'work_item_id' => 'required',
            'serial' => 'nullable|string',
            'description' => 'required|string',
        ]);

        $partProperties['user_id'] = Auth::id();
        $part = $partUnitOfWork->create($partProperties);

        $transformedPart = fractal()
            ->item($part)
            ->transformWith(new PartTransformer())
            ->parseIncludes([])
            ->toArray();
        return response()->json($transformedPart)->setStatusCode(201);
    }


    /**
     * @param PartUnitOfWork $partUnitOfWork
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(PartUnitOfWork $partUnitOfWork, $id)
    {
        $result = $partUnitOfWork->delete($id);

        if ($result === null) {
            abort(404);
        }

        if ($result === false) {
            abort(500);
        }

        return response()->json($result)->setStatusCode(204);
    }
}
