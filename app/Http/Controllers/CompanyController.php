<?php

namespace App\Http\Controllers;

use App\Repositories\RepositoryContracts\CompanyRepository;
use App\Repositories\RepositoryCriteria\CompanyLike;
use App\Repositories\RepositoryCriteria\EagerLoad;
use App\Repositories\RepositoryCriteria\Latest;
use App\Repositories\RepositoryCriteria\Limit;
use App\ServiceManager\Company\Company;
use App\Transformers\CompanyTransformer;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    protected $companyRepository;
    protected $companyTransformer;

    public function __construct(CompanyRepository $companyRepository, CompanyTransformer $companyTransformer)
    {
        $this->companyRepository = $companyRepository;
        $this->companyTransformer = $companyTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $criteria = [new Latest()];

        if ($term = $request->get('term')) {
            $criteria[] = (new CompanyLike())->setTerm($term);
        }

        if ($pageSize = $request->get('page_size', 5)) {
            $criteria[] = new Limit($pageSize);
        }

        $devices = $this->companyRepository->withCriteria(
            $criteria
        )->get();

        $transformedCompanies = fractal()
            ->collection($devices)
            ->transformWith(new CompanyTransformer())
            ->parseIncludes([])
            ->toArray();

        return response()->json($transformedCompanies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:companies|max:255',
            'address' => 'required',
            'telephone' => 'required',
        ]);

        $company = $this->companyRepository->create($validatedData);
        $transformedCompany = fractal()
            ->item($company)
            ->transformWith(new CompanyTransformer())
            ->parseIncludes([])
            ->toArray();
        return response()->json($transformedCompany)->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = $this->getCompany($id, ['contacts']);
        $transformedCompany = $this->getTransformedCompany($company);
        if (request()->expectsJson()) {
            return response()->json($transformedCompany)->setStatusCode(200);
        }
        return view('companies.show',['company'=>$company]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->companyRepository->findOrFail($id);
        $validated = $this->validate($request, [
            'name' => 'required|string|min:2|max:100',
            'address' => 'required|string|min:2|max:150',
            'telephone' => 'required|string|min:7|max:30',
        ]);
        $this->companyRepository->update($id,$validated);
        $company = $this->getCompany($id, ['contacts']);
        $transformedCompany = $this->getTransformedCompany($company);
        return response()->json($transformedCompany)->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function getCompany($id, array $eagerLoad = null): Company
    {
        $defaultEagerLoad = [];
        $eagerLoad = is_array($eagerLoad) ? $eagerLoad : $defaultEagerLoad;
        return $this->companyRepository->withCriteria(new EagerLoad($eagerLoad))->find($id);
    }

    protected function getTransformedCompany(Company $company, $includes = ['contacts','devices'])
    {
        return fractal()
            ->item($company)
            ->transformWith($this->companyTransformer)
            ->parseIncludes($includes)
            ->toArray();
    }
}
