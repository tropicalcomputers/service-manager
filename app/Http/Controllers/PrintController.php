<?php

namespace App\Http\Controllers;


use App\Repositories\RepositoryContracts\WorkItemRepository;

class PrintController extends Controller
{
    public function workItemReceipt(WorkItemRepository $workItemRepository, $id)
    {
        $workItem = $workItemRepository->find($id);
        return view('prints.work-items.receipt',['workItem'=>$workItem]);
    }
}
