<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeviceCreateRequest;
use App\Repositories\RepositoryContracts\DeviceRepository;
use App\Repositories\RepositoryCriteria\Device\BelongsToCustomer;
use App\Repositories\RepositoryCriteria\DeviceLike;
use App\Repositories\RepositoryCriteria\EagerLoad;
use App\Repositories\RepositoryCriteria\Latest;
use App\Repositories\RepositoryCriteria\Limit;
use App\ServiceManager\Device\Device;
use App\ServiceManager\Device\Eloquent\EloquentDevice;
use App\Transformers\DeviceTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Ramsey\Uuid\Uuid;

class DeviceController extends ApiController
{
    protected $deviceRepository;
    protected $deviceTransformer;

    public function __construct(DeviceRepository $deviceRepository, DeviceTransformer $deviceTransformer)
    {
        $this->deviceRepository = $deviceRepository;
        $this->deviceTransformer = $deviceTransformer;
        $this->resourceTransformer = $deviceTransformer;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(DeviceCreateRequest $request)
    {
        $validatedData = $request->validated();
        $validatedData['serial'] = $validatedData['serial']?? Carbon::now()->format('Ymd') . '-' . explode('-',  Uuid::uuid4()->toString())[1];
        $device = $this->deviceRepository->create($validatedData);
        $device = $this->deviceRepository->withCriteria(new EagerLoad(['deviceType']))->find($device->getId());
        return response()->json($this->getTransformedDevice($device))->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  DeviceTransformer $deviceTransformer
     * @pram $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $device = $this->getDevice($id, ['workItems']);
        $deviceTransformed = $this->getTransformed($device,['workItems']);
        return view('devices.show',['deviceJson'=>json_encode($deviceTransformed)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function edit(EloquentDevice $device)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $this->deviceRepository->findOrFail($id);

        $attributes = $this->validate($request, [
            'password' => 'nullable',
        ]);

        if ($this->deviceRepository->update($id, $attributes)) {
            return response()->json()->setStatusCode(204);
        }

        // TODO: change error format
        return response()->json(['message'=>'A server error occurred'])->setStatusCode(500);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function destroy(EloquentDevice $device)
    {
        //
    }


    protected function getDevice($id, array $eagerLoad = null): EloquentDevice
    {
        $defaultEagerLoad = [];
        $eagerLoad = is_array($eagerLoad) ? $eagerLoad : $defaultEagerLoad;
        return $this->deviceRepository->withCriteria(new EagerLoad($eagerLoad))->find($id);
    }

    protected function getTransformedDevice(Device $device, $includes = ['workItems'])
    {
        return fractal()
            ->item($device)
            ->transformWith($this->deviceTransformer)
            ->parseIncludes($includes)
            ->toArray();
    }

    public function index(Request $request)
    {
        $criteria = [new Latest(), new EagerLoad(['deviceType'])];

        if ($term = $request->get('term')) {
            $criteria[] = (new DeviceLike())->setTerm($term);
        }

        if ($pageSize = $request->get('page_size', 5)) {
            $criteria[] = new Limit($pageSize);
        }

        if ($customerId = $request->get('customer_id')) {
            $criteria[] = (new BelongsToCustomer())->setCustomerId($customerId);
        }

        $devices = $this->deviceRepository->withCriteria(
            $criteria
        )->get();

        $transformedDevices = fractal()
            ->collection($devices)
            ->transformWith($this->deviceTransformer)
            ->parseIncludes([])
            ->toArray();

        return response()->json($transformedDevices);
    }
}
