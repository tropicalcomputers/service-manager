<?php

namespace App\Http\Controllers;

use App\Repositories\RepositoryContracts\CompanyRepository;
use App\Repositories\RepositoryContracts\CustomerRepository;
use App\Repositories\RepositoryContracts\DeviceRepository;
use App\Repositories\RepositoryContracts\WorkItemRepository;
use App\Repositories\RepositoryCriteria\CompanyLike;
use App\Repositories\RepositoryCriteria\CustomerLike;
use App\Repositories\RepositoryCriteria\DeviceLike;
use App\Repositories\RepositoryCriteria\EagerLoad;
use App\Repositories\RepositoryCriteria\FilteredDeviceManufacturers;
use App\Repositories\RepositoryCriteria\FilteredDeviceModels;
use App\Repositories\RepositoryCriteria\Latest;
use App\Repositories\RepositoryCriteria\Limit;
use App\Repositories\RepositoryCriteria\OrderBy;
use App\Repositories\RepositoryCriteria\Where;
use App\ServiceManager\Branch\Branch;
use App\ServiceManager\Customer\Customer;
use App\ServiceManager\Device\Eloquent\EloquentDevice;
use App\ServiceManager\Device\DeviceType;
use App\ServiceManager\Inventory\Inventory;
use App\ServiceManager\Inventory\InventoryOption;
use App\ServiceManager\Priority\Priority;
use App\ServiceManager\Status\Status;
use App\ServiceManager\User\Eloquent\UserModel;
use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class AutoCompleteController extends Controller
{
    protected $companyRepository;
    protected $customerRepository;
    protected $deviceRepository;
    protected $workItemRepository;

    public function __construct(
        CompanyRepository $companyRepository,
        DeviceRepository $deviceRepository,
        WorkItemRepository $workItemRepository,
        CustomerRepository $customerRepository)
    {
        $this->workItemRepository = $workItemRepository;
        $this->deviceRepository = $deviceRepository;
        $this->customerRepository = $customerRepository;
        $this->companyRepository = $companyRepository;
    }

    public function enums()
    {
        $enums = [];
        $enums['deviceTypes'] = DeviceType::with('inventoryOptions')->get()->toArray();
        $enums['priorities'] = Priority::all()->toArray();
        $enums['statuses'] = Status::all()->toArray();
        $enums['branches'] = Branch::where('name','<>','Collymore Rock')->get()->toArray();
        $enums['manufacturers'] = EloquentDevice::select('manufacturer')->where('manufacturer','<>',"")->distinct()->orderBy('manufacturer')->get()->toArray();
        $enums['models'] = EloquentDevice::select('model')->where('model','<>',"")->distinct()->orderBy('model')->get()->toArray();
        $enums['users'] = UserModel::orderBy('name','asc')->get()->toArray();
        return response()->json($enums);
    }

    public function manufacturers(Request $request)
    {
        $term = $request->get('term');
        $criteria = [
            new FilteredDeviceManufacturers($term),
            new Limit(5),
        ];
        $manufacturers = $this->deviceRepository->withCriteria(
            $criteria
        )->get();
        return response()->json($manufacturers);
    }

    public function models(Request $request)
    {
        $term = $request->get('term');
        $manufacturer = $request->get('manufacturer');
        $criteria = [
            new FilteredDeviceModels($term, $manufacturer),
            new Limit(5),
        ];
        $models = $this->deviceRepository->withCriteria(
            $criteria
        )->get();
        return response()->json($models);
    }

    public function existingCustomers(Request $request)
    {
        $term = $request->get('term');
        $criteria = [
            (new CustomerLike())->setTerm($term),
            new EagerLoad(['company']),
            new Limit(5),
        ];
        if (empty($term)) {
            $criteria[] = new Latest();
        }
        $customers = $this->customerRepository->withCriteria(
            $criteria
        )->get();
        return response()->json($customers);
    }

    public function employees(Request $request)
    {
        $companyId = $request->get('company_id');
        $persons = $this->customerRepository
            ->withCriteria(
                new Where('company_id','=',$companyId),
                new OrderBy('first_name','asc')
            )->get();

        $personsAutoComplete = $this->getCustomerDto($persons,collect([]));
        return response()->json($personsAutoComplete);
    }

    public function companies(Request $request)
    {
        $term = $request->get('term');
        $companies = $this->companyRepository->withCriteria(
            new Limit(10),
            new CompanyLike($term)
        )->get();
        return response()->json($companies);
    }

    public function company($companyId)
    {
        $company = $this->companyRepository->find($companyId);
        $companyAutoComplete = $this->getCustomerDto(collect([]),collect([$company]));
        return response()->json($companyAutoComplete->first());
    }

    public function person($personId)
    {
        $person = $this->customerRepository->find($personId);
        $personAutoComplete = $this->getCustomerDto(collect([$person]),collect([]));
        return response()->json($personAutoComplete->first());
    }

    protected function getCustomerDto(Collection $persons, Collection $company)
    {
        $personsAutoComplete = $persons->map(function($item,$key){
            return [
                'person' => $item->toArray(),
                'company' => $item->company ? $item->company->toArray() : [],
                'type' => 'Person',
            ];
        });

        $companiesAutoComplete = $company->map(function($item,$key){
            return [
                'person' => [],
                'company' => $item->toArray(),
                'type' => 'Company',
            ];
        });
        return $personsAutoComplete->concat($companiesAutoComplete);
    }

    public function previousDevices(Customer $customer)
    {
        $customerIds = $customer->company_id ? Customer::where('company_id',$customer->company_id)->get()->pluck('id') : [$customer->id];
        $deviceIds = EloquentWorkItem::whereIn('customer_id',$customerIds)->select('system_id')->distinct()->get()->pluck('system_id');
        $devices = EloquentDevice::whereIn('id',$deviceIds)->get();

        $deviceAutoComplete = $devices->map(function($item,$key){
            return [
                'id'=> $item->id,
                'serial'=> $item->serial,
                'manufacturer'=> $item->manufacturer,
                'model'=> $item->model,
                'password'=> $item->password,
            ];
        });
        return response()->json($deviceAutoComplete);
    }

    public function device(EloquentDevice $device)
    {
        $deviceAutoComplete = [
            'id'=> $device->id,
            'serial'=> $device->serial,
            'manufacturer'=> $device->manufacturer,
            'model'=> $device->model,
            'password'=> $device->password,

        ];
        return response()->json($deviceAutoComplete);
    }
}
