<?php

namespace App\Http\Controllers;

use App\Repositories\RepositoryContracts\UserRepository;
use App\ServiceManager\User\Eloquent\UserModel;
use App\ServiceManager\User\User;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;

class UserController extends Controller
{

    protected $repo;
    public function __construct(UserRepository $userRepository)
    {
        $this->repo = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', User::class);
        $users = $this->repo->get();
        $transformedUsers = fractal()
            ->collection($users)
            ->transformWith(new UserTransformer())
            ->parseIncludes([])
            ->toArray();

        return view('users.index',['usersJson'=> json_encode($transformedUsers)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create',User::class);
        $userData = $request->validate([
            'email' => "required|email",
            'name' => "required|max:50|min:5",
            'enabled' => "required|boolean"
        ]);

        return response()->json($this->getTransformed($this->repo->create($userData)))->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->repo->findOrFail($id);
        $this->authorize('update', $user);
        $attributes = $this->validate($request, [
            'enabled' => 'boolean',
            'email' => 'email',
            'name'=>'string|min:5',
        ]);

        if ($this->repo->update($id,$attributes)) {
            $user = $this->repo->findOrFail($id);
        }
        return response()->json($this->getTransformed($user));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        //TODO: check if user has work items / histories

        /*
         *         return response()->json([
            'errors' =>
                ['too-many'=>['user had too many issues']]
        ])->setStatusCode(422);
         */

        abort(405);
        if (UserModel::destroy($id)) {
            return response()->json()->setStatusCode(204);
        }
    }

    protected function getTransformed(User $user)
    {
        return fractal()
            ->item($user)
            ->transformWith(new UserTransformer())
            ->parseIncludes([])
            ->toArray();
    }
}
