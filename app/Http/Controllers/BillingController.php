<?php

namespace App\Http\Controllers;

use App\ServiceManager\Billing\Billing;
use App\Repositories\RepositoryContracts\WorkItemRepository;
use App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem;
use App\Transformers\BillingTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class BillingController extends ApiController
{
    public function __construct(BillingTransformer $resourceTransformer)
    {
        $this->resourceTransformer = $resourceTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @param WorkItemRepository $workItemRepository
     * @param EloquentWorkItem $workItem
     * @return Redirect
     */
    public function store(Request $request, WorkItemRepository $workItemRepository, EloquentWorkItem $workItem)
    {
        $billingData = $request->validate([
            'type' => 'string|required',
            'reference' => 'required_if:type,Invoice|nullable|string',
            'notes' => 'nullable|string'
        ]);

        $fullBillingData = array_merge($billingData,['user_id'=>Auth::id()]);
        $billing = $workItemRepository->addBilling($workItem->id, $fullBillingData);
        return $this->createdResponse($billing);

    }

    /**
     * Display the specified resource.
     *
     */
    public function show()
    {
//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Billing  $billing
     * @return \Illuminate\Http\Response
     */
    public function edit(Billing $billing)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Billing  $billing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Billing $billing)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Billing  $billing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Billing $billing)
    {
        //
    }
}
