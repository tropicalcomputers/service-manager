<?php


namespace App\Http\Controllers;


abstract class ApiController extends Controller
{
    protected $resourceTransformer = null;

    protected function getResourceTransformer()
    {
        return $this->resourceTransformer;
    }

    protected function getTransformed($resource, $includes = [])
    {
        return fractal()
            ->item($resource)
            ->transformWith($this->getResourceTransformer())
            ->parseIncludes($includes)
            ->toArray();
    }

    protected function createdResponse($resource, $includes = [])
    {
        $transformed = $this->getTransformed($resource);
        return response()->json($transformed)->setStatusCode(201);
    }
}
