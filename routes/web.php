<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('auth/login/{user}',[\App\Http\Controllers\Auth\LoginController::class,'loginByEmail'])
->name('login.email')
->middleware(['signed','signed-url-unused']);

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {

    Route::get('/auth/me', 'Auth\AuthController@me')->name('auth.me');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');

    Route::get('/auto-complete/enums', 'AutoCompleteController@enums')->name('auto-complete.enums');
    Route::get('/auto-complete/customers', 'AutoCompleteController@existingCustomers')->name('auto-complete.customers');
    Route::get('/auto-complete/employees', 'AutoCompleteController@employees')->name('auto-complete.employees');
    Route::get('/auto-complete/companies', 'AutoCompleteController@companies')->name('auto-complete.companies');
    Route::get('/auto-complete/companies/{companyId}', 'AutoCompleteController@company')->name('auto-complete.company');
    Route::get('/auto-complete/persons/{personId}', 'AutoCompleteController@person')->name('auto-complete.person');

    Route::get('/auto-complete/devices', 'AutoCompleteController@devices')->name('auto-complete.devices');
    Route::get('/auto-complete/device-manufacturers', 'AutoCompleteController@manufacturers')->name('auto-complete.device-manufacturers');
    Route::get('/auto-complete/device-models', 'AutoCompleteController@models')->name('auto-complete.device-models');
    Route::get('/auto-complete/devices/{device}', 'AutoCompleteController@device')->name('auto-complete.device');
    Route::get('/auto-complete/customers/{customer}/devices', 'AutoCompleteController@previousDevices')->name('auto-complete.customer-devices');

    Route::resource('companies', 'CompanyController');
    Route::resource('customers', 'CustomerController');

    // Devices
    Route::resource('devices', 'DeviceController');




    Route::get('prints/work-items/{id}/receipt','PrintController@workItemReceipt')->name('prints.work-items.receipt');


    Route::post('work-items/{workItem}/history', [\App\Http\Controllers\WorkItemHistoryController::class,'store'])->name('work-item-history.store');
    Route::post('work-items/{workItem}/parts', [\App\Http\Controllers\PartController::class,'store'])->name('part.store');
    Route::post('work-items/{workItem}/billings', [\App\Http\Controllers\BillingController::class,'store'])->name('billing.store');
    Route::delete('parts/{id}', [\App\Http\Controllers\PartController::class,'destroy'])->name('part.destroy');


    Route::resource('work-items', 'WorkItemController');
    Route::resource('work-item-histories', 'WorkItemHistoryController');
    Route::resource('components', 'ComponentController');
    Route::resource('users', 'UserController');
    Route::resource('device-types', 'DeviceTypeController');

});



