<?php

use Illuminate\Database\Seeder;
use App\ServiceManager\ {
    Device\DeviceType
};

class DeviceTypeInventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mapping = [
            'Desktop' => [
                'Power Cord', 'Power Adapter', 'Surge Protector','Monitor', 'Mouse', 'Keyboard', 'Software',
            ],
            'Notebook' => [
                'Power Cord','Power Adapter',  'Surge Protector', 'Battery', 'Keyboard', 'Mouse', 'Software', 'Notebook Bag',
            ],
            'Server' => [
                'Power Cord', 'Power Adapter', 'Surge Protector','Keyboard', 'Mouse', 'Software',
            ],
            'Printer' => [
                'Power Cord', 'Power Adapter', 'Surge Protector','Printer Cartridge', 'Printer Toner', 'Printer Cable',
            ],
            'Other' => [
                'Power Cord','Power Adapter','Surge Protector','Software', 'Carry Case','Battery',
            ]
        ];

        $deviceTypes = DeviceType::all();
        \Illuminate\Support\Facades\DB::table('device_type_inventory_option')->truncate();
        $inventoryOptions = \Illuminate\Support\Facades\DB::table('inventory_options')->get();

        foreach ($deviceTypes as $deviceType) {
            if (isset($mapping[$deviceType->name])) {
                $options = [];
                foreach ($mapping[$deviceType->name] as $inventoryItem) {
                    if ($option = $inventoryOptions->firstWhere('name','=',$inventoryItem)) {
                        $options[] =  $option->id;
                    }
                }
                $deviceType->inventoryOptions()->attach($options);
            }
        }
    }
}
