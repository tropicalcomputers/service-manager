<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CopyFromOldTableSeeder extends Seeder
{
    protected $oldConnection = 'old_mysql';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configs = $this->getConfigs();
        $this->alterTablesBefore();
        foreach ($configs as $config) {
            $this->convertAndCopy($config);
        }
        $this->fannyAboutAfter();
        $this->alterTablesAfter();
    }

    protected function fannyAboutAfter()
    {
        // make system account
        $user = new \App\ServiceManager\User\Eloquent\UserModel;
        $user->name = 'System';
        $user->email = 'service-manager-app@tropicalcomputers.com';
        $user->enabled = false;
        $user->save();


        $parts = \App\ServiceManager\Part\Eloquent\EloquentPart::all();
        foreach ($parts as $part) {
            $workItemHistory = \App\ServiceManager\WorkItemHistory\WorkItemHistory::find($part->change_id);
            $workItemHistory->timestamps = false;
            $part->timestamps = false;

            $part->created_at = $workItemHistory->created_at;
            $part->created_at = $workItemHistory->updated_at;
            $part->work_item_id = $workItemHistory->work_item_id;
            $part->user_id = $workItemHistory->changed_by;

            $workItemHistory->description = "Part Added: " . $part->description . ' (Serial: ' . ($part->serial ?? "none") . ')';
            $workItemHistory->system_log = true;
            $workItemHistory->time = 0;
            $workItemHistory->customer_contacted = false;

            $workItemHistory->save();
            $part->save();
        }

        // Create billing records for invoices and other

        $workItems = \App\ServiceManager\WorkItem\Eloquent\EloquentWorkItem::whereNotNull('invoice_number')->get();
        $repo = app()->make(\App\Repositories\RepositoryContracts\WorkItemRepository::class);

        foreach ($workItems as $workItem) {
            if (is_numeric($workItem->invoice_number)) {
                $repo->addBilling($workItem->id, [
                    'type' => 'Invoice',
                    'reference' => $workItem->invoice_number,
                    'notes' => 'Migrated from previous application',
                    'user_id' => $user->id,
                ]);
            } elseif (stripos($workItem->invoice_number, 'warranty')!== false) {
                $repo->addBilling($workItem->id, [
                    'type' => 'Warranty',
                    'reference' => $workItem->invoice_number,
                    'notes' => 'Migrated from previous application',
                    'user_id' => $user->id,
                ]);
            } elseif (stripos($workItem->invoice_number, 'support contract')!== false) {
                $repo->addBilling($workItem->id, [
                    'type' => 'Support Contract',
                    'reference' => $workItem->invoice_number,
                    'notes' => 'Migrated from previous application',
                    'user_id' => $user->id,
                ]);
            } else {
                $repo->addBilling($workItem->id, [
                    'type' => 'Other',
                    'reference' => $workItem->invoice_number,
                    'notes' => 'Migrated from previous application',
                    'user_id' => $user->id,
                ]);
            }
        }
    }

    protected function alterTablesBefore()
    {
        Schema::table('parts', function (Blueprint $table) {
            $table->integer('change_id');
            $table->integer('work_item_id')->nullable()->change();
            $table->integer('user_id')->nullable()->change();
        });
        \App\ServiceManager\Billing\Billing::truncate();
    }

    protected function alterTablesAfter()
    {
        Schema::table('parts', function (Blueprint $table) {
            $table->dropColumn('change_id');
            $table->integer('work_item_id')->nullable(false)->change();
            $table->integer('user_id')->nullable(false)->change();
        });
    }

    protected function getConfigs()
    {
        $configs['customers'] =  new TableCopyConfig([
            'old'=>'Customers',
            'new'=>'customers',
            'id' => 'CustomerId',
            'overrides' =>[],
            'map'=>[
                'CustomerId' => 'id',
                'CompanyId' => 'company_id',
                'FirstName' => 'first_name',
                'LastName' => 'last_name',
                'Address' => 'address',
                'Email' => 'email',
                'HomeNumber' => 'home_telephone',
                'WorkNumber' => 'work_telephone',
                'CellNumber' => 'mobile_telephone',
            ],
        ]);
        $configs['companies'] =  new TableCopyConfig([
            'old'=>'Companies',
            'new'=>'companies',
            'id' => 'CompanyId',
            'overrides' =>[],
            'map'=>[
                'CompanyId' => 'id',
                'CompanyName' => 'name',
                'Address' => 'address',
                'ContactNumber' => 'telephone',
            ],
        ]);
        $configs['devices'] =  new TableCopyConfig([
            'old'=>'SystemInfos',
            'new'=>'devices',
            'id' => 'SystemId',
            'map'=>[
                'SystemId' => 'id',
                'Manufacturer' => 'manufacturer',
                'Model' => 'model',
                'Serial' => 'serial',
                'Password' => 'password',
            ],
            'overrides' => [
                'device_type_id' => function () {return 0;},
                // 'password'=> function() {return '***********';},
            ],
            'defaults' => [
                'serial' => "",
            ],
        ]);
        $configs['workItemHistory'] =  new TableCopyConfig([
            'generateTimestamps'=>false,
            'old'=>'WorkItemHistory',
            'new'=>'work_item_histories',
            'id' => 'ChangeId',
            'map'=>[
                'ChangeId' => 'id',
                'WorkItemId' => 'work_item_id',
                'Description' => 'description',
                'Time' => 'time',
                'ChangedBy' => 'changed_by',
                'AssignedTo' => 'assigned_to',
                'Status' => 'status_id',
                'Branch' => 'branch_id',
                'CustomerContacted' => 'customer_contacted',
            ],
            'defaults' => [],
            'overrides' => [
                'updated_at' => function ($row) {
                    return \Carbon\Carbon::parse($row->Updated);
                },
                'created_at' => function ($row) {
                    return \Carbon\Carbon::parse($row->Updated);
                },
            ],
        ]);
        $configs['parts'] =  new TableCopyConfig([
            'old'=>'Parts',
            'new'=>'parts',
            'id' => 'PartId',
            'map'=>[
                'PartId' => 'id',
                'Serial' => 'serial',
                'Description' => 'description',
                'ChangeId' => 'change_id',
            ],
            'overrides' =>[
            ],
            'defaults' => [
                'serial' => "",
            ],
        ]);
        $configs['workItems'] =  new TableCopyConfig([
            'old'=>'WorkItems',
            'new'=>'work_items',
            'id' => 'WorkItemId',
            'map'=>[
                'WorkItemId' => 'id',
                'CustomerId' => 'customer_id',
                'SystemId' => 'device_id',
                'Invoice' => 'invoice_number',
                'Notes' => 'notes',
                'Priority' => 'priority_id',
            ],
        ]);
        $configs['users'] =  new TableCopyConfig([
            'old'=>'Users',
            'new'=>'users',
            'id' => 'UserId',
            'map'=>[
                'UserId' => 'id',
                'Email' => 'email',
            ],
            'defaults' => [
               // 'password' => \Illuminate\Support\Facades\Hash::make('jds&64%%.appaKerriJerry15!!'),
            ],
            'overrides' => [
                'name' => function($row) {
                    return $row->FirstName . " " . $row->LastName;
                },
                'email' =>  function($row) {
                    return filter_var($row->Email, FILTER_VALIDATE_EMAIL) ? $row->Email : "z-no-email-" . \Illuminate\Support\Str::random(5) . "@example.com";
                },

            ],
        ]);

        return $configs;

    }

    protected function convertAndCopy(TableCopyConfig $config)
    {
        DB::table($config->getNew())->truncate();
        DB::connection($this->oldConnection)
            ->table($config->getOld())
            ->select(['*'])
            ->orderBy($config->getId())
            ->chunk(1000,function($oldData) use ($config) {

                $newData = $oldData->map(function($item,$key) use($config) {
                    $newRow =[];

                    $newRow = $config->isGenerateTimestamps() ? array_merge($newRow, $config->getTimestamps()) : $newRow;

                    foreach ($config->getMap() as $oldColumn => $newColumn) {
                        $newRow[$newColumn] = $item->$oldColumn;
                    }

                    foreach ($config->getDefaults() as $defaultColumn => $defaultValue) {
                        $newRow[$defaultColumn] = $newRow[$defaultColumn] ?? $defaultValue;
                    }

                    foreach ($config->getOverrides() as $overrideColumn => $callback) {
                        $newRow[$overrideColumn] = $callback($item);
                    }

                    return $newRow;
                });

                DB::table($config->getNew())->insert($newData->toArray());
            });
    }

}
