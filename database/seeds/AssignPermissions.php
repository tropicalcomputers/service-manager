<?php
use Illuminate\Database\Seeder;

class AssignPermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Role::where('id','>',0)->delete();
        \Spatie\Permission\Models\Permission::where('id','>',0)->delete();

        $adminPermissions = [];
        $role = \Spatie\Permission\Models\Role::create(['name' => 'admin']);
        $adminPermissions[] = \Spatie\Permission\Models\Permission::create(['name' => 'create users']);
        $adminPermissions[] = \Spatie\Permission\Models\Permission::create(['name' => 'edit other users']);
        $adminPermissions[] = \Spatie\Permission\Models\Permission::create(['name' => 'delete other users']);
        $role->syncPermissions($adminPermissions);

        \App\ServiceManager\User\Eloquent\UserModel::where('email','ofitzpatrick@tropicalcomputers.com')->update([
            'email' => 'jerome@jeromefitzpatrick.com'
        ]);

        $adminUsers = \App\ServiceManager\User\Eloquent\UserModel::whereIn('email',['jerome@jeromefitzpatrick.com','sbrereton@tropicalcomputers.com','mcodrington@tropicalcomputers.com'])->get();

        /** @var \App\ServiceManager\User\Eloquent\UserModel $adminUser */
        foreach ($adminUsers as $adminUser) {
            $adminUser->assignRole('admin');
        }
    }
}
