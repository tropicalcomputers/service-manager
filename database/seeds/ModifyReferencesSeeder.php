<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use App\ServiceManager\ {
    WorkItemHistory\WorkItemHistory,
    WorkItem\Eloquent\EloquentWorkItem
};

class ModifyReferencesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workItemHistoryTable = (new WorkItemHistory())->getTable();

        // Done this way to prevent timestamp updates
        DB::table($workItemHistoryTable)->where('branch_id',1)->update(['branch_id' => 2]);
        DB::table($workItemHistoryTable)->where('branch_id',0)->update(['branch_id' => 1]);
        DB::table($workItemHistoryTable)->where('status_id',0)->update(['status_id' => 11]);

        EloquentWorkItem::where('priority_id',1)->update(['priority_id'=>2]);
        EloquentWorkItem::where('priority_id',0)->update(['priority_id'=>1]);


    }
}
