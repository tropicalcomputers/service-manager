<?php

use Illuminate\Database\Seeder;
use App\ServiceManager\ {
    WorkItem\Eloquent\EloquentWorkItem
};

class CopyWorkItemHistoryFieldsToWorkItems extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();

        EloquentWorkItem::with('history')->chunk(100, function($workItems) use ($now) {
            foreach ($workItems as $workItem) {
                $attributes = [
                    'description' => $workItem->history->last()->description ?? "Description Missing",
                    'assigned_to' => $workItem->history->first()->assigned_to ?? 0,
                    'signed_in_by' => $workItem->history->last()->changed_by ?? 0,
                    'branch_id' => $workItem->history->first()->branch_id ?? 0,
                    'status_id' => $workItem->history->first()->status_id ?? 0,
                    'time' => $workItem->history->sum('time'),
                    'created_at' =>  $workItem->history->last()->created_at ?? $now,
                    'updated_at' => $workItem->history->first()->created_at ?? $now,
                ];

                EloquentWorkItem::where('id',$workItem->id)->update($attributes);
            }
        });
    }
}
