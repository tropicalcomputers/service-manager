<?php


class TableCopyConfig
{

    protected $old = "";
    protected $new = "";
    protected $id = "";
    protected $map = [];
    protected $overrides = [];
    protected $defaults = [];
    protected $generateTimestamps = true;


    public function __construct($config = [])
    {
        foreach ($config as $key => $value) {
            if (property_exists(static::class, $key)) {
                $this->$key = $value;
            }
        }
    }

    public function getTimestamps()
    {
        return ['created_at' => \Carbon\Carbon::now(), 'updated_at'=> \Carbon\Carbon::now()];
    }

    /**
     * @return bool
     */
    public function isGenerateTimestamps(): bool
    {
        return $this->generateTimestamps;
    }

    /**
     * @param bool $generateTimestamps
     */
    public function setGenerateTimestamps(bool $generateTimestamps): void
    {
        $this->generateTimestamps = $generateTimestamps;
    }



    /**
     * @return string
     */
    public function getOld(): string
    {
        return $this->old;
    }

    /**
     * @param string $old
     */
    public function setOld(string $old): void
    {
        $this->old = $old;
    }

    /**
     * @return string
     */
    public function getNew(): string
    {
        return $this->new;
    }

    /**
     * @param string $new
     */
    public function setNew(string $new): void
    {
        $this->new = $new;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getMap(): array
    {
        return $this->map;
    }

    /**
     * @param array $map
     */
    public function setMap(array $map): void
    {
        $this->map = $map;
    }

    /**
     * @return array
     */
    public function getOverrides(): array
    {
        return $this->overrides;
    }

    /**
     * @param array $overrides
     */
    public function setOverrides(array $overrides): void
    {
        $this->overrides = $overrides;
    }

    /**
     * @return array
     */
    public function getDefaults(): array
    {
        return $this->defaults;
    }

    /**
     * @param array $defaults
     */
    public function setDefaults(array $defaults): void
    {
        $this->defaults = $defaults;
    }
}
