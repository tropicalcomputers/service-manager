<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class SetDeviceTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->setDeviceTypes();

    }
    protected function setDeviceTypes()
    {
        $deviceNames = ['Server','Cpu', 'Notebook','Printer'];

        $allDeviceTypes = DB::table('device_types')->get();
        $inventoryOptions = DB::table('inventory_options')->whereIn('name',$deviceNames)->get();
        $newMapping = [];
        foreach ($allDeviceTypes as $deviceType) {
            if ($inv = $inventoryOptions->firstWhere('name','=',$deviceType->name)) {
                $newMapping[$inv->id] = $deviceType->id;
            }
        }

        $inventoryOptionIds = $inventoryOptions->pluck('id');
        $inventories = DB::table('inventory')
            ->join('work_items', 'inventory.work_item_id', '=', 'work_items.id')
            ->join('devices', 'work_items.device_id', '=', 'devices.id')
            ->whereIn('inventory_option_id',$inventoryOptionIds)
            ->select('work_items.device_id', 'inventory.inventory_option_id', 'devices.manufacturer','devices.model')
            ->get();

        $updates = [];
        foreach ($inventories as $inventory) {
            $updates[$inventory->device_id] = [
                'device_type_id' => $newMapping[$inventory->inventory_option_id]
            ];
        }

        foreach ($updates as $deviceId => $update) {
            DB::table('devices')->where('id','=',$deviceId)->update($update);
        }

        DB::table('devices')->where('device_type_id','=',0)->update(['device_type_id'=>5]);
        DB::table('device_types')->where('name','=','Cpu')->update(['name'=>'Desktop']);

    }

}
