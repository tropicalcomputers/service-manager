<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    protected $oldConnection = 'old_mysql';

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(
            [
                // BuildOldSchemaSeeder::class,
                EnumTableSeeder::class,
                DeviceTypesTableSeeder::class,
                PrioritiesTableSeeder::class,
                CopyFromOldTableSeeder::class,
                CopyInventorySeeder::class,
                SetDeviceTypesSeeder::class,
                ModifyReferencesSeeder::class,
                DeviceTypeInventorySeeder::class,
                CopyWorkItemHistoryFieldsToWorkItems::class,
                AssignPermissions::class,
            ]
        );
        echo 'Thanks for playing :)' . PHP_EOL;
    }
}
