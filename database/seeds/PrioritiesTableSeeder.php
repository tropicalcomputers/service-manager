<?php

use Illuminate\Database\Seeder;
use App\ServiceManager\Priority\Priority;

class PrioritiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Priority::truncate();
        foreach ($this->getPriorities() as $priority) {
            Priority::create($priority);
        }
    }

    protected function getPriorities()
    {
        return [
            ['name'=>'High'],
            ['name'=>'Normal'],
            ['name'=>'Low'],
        ];
    }
}
