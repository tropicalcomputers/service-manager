<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class BuildOldSchemaSeeder extends Seeder
{
    protected $connection = 'old_mysql';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->companies();
        $this->customers();
        $this->parts();
        $this->systemInfos();
        $this->users();
        $this->workItemHistory();
        $this->workItems();
        $this->attachments();
        $this->inventory();
    }

    protected function companies()
    {
        $table = 'Companies';
        Schema::dropIfExists($table);

        Schema::connection($this->connection)->create($table,
            function (\Illuminate\Database\Schema\Blueprint $table) {
                $table->increments('CompanyId');
                $table->string('CompanyName');
                $table->string('Address');
                $table->string('ContactNumber')->nullable();
            });
    }

    protected function customers()
    {
        $table = 'Customers';
        Schema::dropIfExists($table);

        \Illuminate\Support\Facades\Schema::connection($this->connection)->create($table,
            function (\Illuminate\Database\Schema\Blueprint $table) {
                $table->increments('CustomerId');
                $table->unsignedInteger('CompanyId')->nullable();
                $table->string('FirstName');
                $table->string('LastName');
                $table->string('Address');
                $table->string('Email')->nullable();
                $table->string('HomeNumber')->nullable();
                $table->string('WorkNumber')->nullable();
                $table->string('CellNumber')->nullable();
            });
    }

    protected function parts()
    {
        $table = 'Parts';
        Schema::dropIfExists($table);

        \Illuminate\Support\Facades\Schema::connection($this->connection)->create($table, function (\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('PartId');
            $table->string('Serial')->nullable();
            $table->text('Description');
            $table->unsignedInteger('ChangeId');
        });
    }

    protected function systemInfos()
    {
        $table = 'SystemInfos';
        Schema::dropIfExists($table);

        \Illuminate\Support\Facades\Schema::connection($this->connection)->create($table,
            function (\Illuminate\Database\Schema\Blueprint $table) {
                $table->increments('SystemId');
                $table->string('Manufacturer');
                $table->string('Model');
                $table->string('Serial')->nullable();
                $table->string('Password')->nullable();
            });
    }

    protected function users()
    {
        $table = 'Users';
        Schema::dropIfExists($table);

        \Illuminate\Support\Facades\Schema::connection($this->connection)->create($table, function (\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('UserId');
            $table->string('UserName')->nullable();
            $table->string('FirstName')->nullable();
            $table->string('LastName')->nullable();
            $table->string('Email')->nullable();
            $table->unsignedInteger('Preferences');
        });
    }

    protected function workItemHistory()
    {
        $table = 'WorkItemHistory';
        Schema::dropIfExists($table);

        \Illuminate\Support\Facades\Schema::connection($this->connection)->create($table,
            function (\Illuminate\Database\Schema\Blueprint $table) {
                $table->increments('ChangeId');
                $table->unsignedInteger('WorkItemId')->nullable();
                $table->string('Updated');
                $table->text('Description');
                $table->string('Time');
                $table->unsignedInteger('ChangedBy');
                $table->unsignedInteger('AssignedTo');
                $table->unsignedInteger('Status');
                $table->unsignedInteger('Branch');
                $table->boolean('CustomerContacted');
            });
    }

    protected function workItems()
    {
        $table = 'WorkItems';
        Schema::dropIfExists($table);

        \Illuminate\Support\Facades\Schema::connection($this->connection)->create($table,
            function (\Illuminate\Database\Schema\Blueprint $table) {
                $table->increments('WorkItemId');
                $table->unsignedInteger('CustomerId');
                $table->unsignedInteger('SystemId');
                $table->string('Invoice')->nullable();
                $table->unsignedInteger('InventoryId');
                $table->text('Notes')->nullable();
                $table->integer('Priority');
            });
    }

    protected function attachments()
    {
        $table = 'Attachments';
        Schema::dropIfExists($table);

        \Illuminate\Support\Facades\Schema::connection($this->connection)->create($table,
            function (\Illuminate\Database\Schema\Blueprint $table) {
                $table->increments('AttachmentId');
                $table->string('FileName');
                $table->text('Description');
                $table->integer('ChangeId');
                $table->string('ContentType');
                $table->integer('ContentLength');
            });
    }

    protected function inventory()
    {
        $table = 'Inventory';
        Schema::dropIfExists($table);

        \Illuminate\Support\Facades\Schema::connection($this->connection)->create($table, function (\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('InventoryId');
            $table->boolean('Server')->default(false);
            $table->boolean('Cpu')->default(false);
            $table->boolean('PowerCord')->default(false);
            $table->boolean('Monitor')->default(false);
            $table->boolean('Mouse')->default(false);
            $table->boolean('Keyboard')->default(false);
            $table->boolean('Software')->default(false);
            $table->boolean('Notebook')->default(false);
            $table->boolean('Adapter')->default(false);
            $table->boolean('Battery')->default(false);
            $table->boolean('ExternalFloppyDrive')->default(false);
            $table->boolean('NotebookBag')->default(false);
            $table->boolean('Printer')->default(false);
            $table->boolean('PrinterPowercord')->default(false);
            $table->boolean('PrinterCartridge')->default(false);
            $table->boolean('PrinterToner')->default(false);
            $table->boolean('PrinterCable')->default(false);
            $table->string('Description')->nullable();
        });
    }
}
