<?php

use Illuminate\Database\Seeder;
use \App\ServiceManager\Inventory\InventoryOption;
use \App\ServiceManager\Device\DeviceType;
use \App\ServiceManager\Inventory\DeviceTypeInventoryOption;

/**
 * Class InventoryOptionsTableSeeder
 */
class InventoryOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InventoryOption::truncate();
        DeviceType::truncate();
        DeviceTypeInventoryOption::truncate();

        foreach ($this->getDeviceTypes() as $deviceType) {
            DeviceType::create($deviceType);
        }

        foreach ($this->getInventoryOptions() as $inventoryOption) {
            InventoryOption::create($inventoryOption);
        }

        foreach ($this->getPivot() as $deviceTypeName => $inventoryOptionIds) {
            $deviceType = \App\ServiceManager\Device\DeviceType::where('name',$deviceTypeName)->first();
            foreach($inventoryOptionIds as $inventoryOptionId) {
                $deviceType->inventoryOptions()->attach($inventoryOptionId);
            }
        }
    }

    /**
     * @return array
     */
    protected function getDeviceTypes()
    {
        return [
            ['name'=>'Desktop'],
            ['name'=>'Notebook'],
            ['name'=>'Printer'],
        ];
    }

    /**
     * @return array
     */
    protected function getInventoryOptions()
    {
        return [
            ['name'=>'Power Cable'],
            ['name'=>'Power Adapter'],
            ['name'=>'Keyboard'],
            ['name'=>'Mouse'],
            ['name'=>'Software'],
            ['name'=>'Carry Case'],
        ];
    }

    /**
     * @return array
     */
    protected function getPivot()
    {
        return [
            'Desktop'=>[1,3,4,5],
            'Notebook'=>[2,5,6],
        ];
    }
}
