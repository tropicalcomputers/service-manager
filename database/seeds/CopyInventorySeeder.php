<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class CopyInventorySeeder extends Seeder
{
    protected $oldConnection = 'old_mysql';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->copyInventory();

    }

    protected function copyInventory()
    {
        $now = \Carbon\Carbon::now();

        $inventoryColumns = DB::connection($this->oldConnection)->getSchemaBuilder()->getColumnListing('Inventory');
        $inventoryColumns = array_flip($inventoryColumns);
        unset($inventoryColumns['Description']);
        unset($inventoryColumns['InventoryId']);

        $inventoryOptions = [];

        foreach ($inventoryColumns as $column => $inventoryColumn) {
            $inventoryColumns[$column] = \Illuminate\Support\Str::title(str_replace('_',' ',\Illuminate\Support\Str::snake($column, "_")));
            $inventoryOptions[] = ['name'=>$inventoryColumns[$column], 'created_at'=>$now, 'updated_at'=>$now];
        }
        
        DB::table('inventory_options')->truncate();
        DB::table('inventory_options')->insert($inventoryOptions);
        $inventoryOptionsDb = DB::table('inventory_options')->get();
        foreach ($inventoryOptionsDb as $inventoryOptionDb) {
            $inventoryColumns[str_replace(" ", "",$inventoryOptionDb->name)] = $inventoryOptionDb->id;
        }

        DB::table('inventory')->truncate();
        DB::connection($this->oldConnection)
            ->table('WorkItems')
            ->join('Inventory', 'WorkItems.InventoryId', '=', 'Inventory.InventoryId')
            ->select('WorkItems.WorkItemId', 'Inventory.*')
            ->orderBy('WorkItems.WorkItemId')
            ->chunk(1000, function($inventory) use ($inventoryColumns, $now) {
                foreach ($inventory as $row) {
                    $insertInventory = [];

                    foreach ($inventoryColumns as $columnName => $columnId) {
                        if ($row->$columnName) {
                            $insertInventory[] = [
                                'work_item_id' => $row->WorkItemId,
                                'inventory_option_id' => $columnId,
                                'created_at' => $now,
                                'updated_at' => $now,
                            ];
                        }
                    }

                    DB::table('work_items')->where('id','=',$row->WorkItemId)->update(
                        ['inventory_notes' => $row->Description]
                    );

                    DB::table('inventory')->insert($insertInventory);
                }

            });

        // insert new options
        DB::table('inventory_options')->insert([
            ['name'=>'Surge Protector','created_at'=>$now, 'updated_at'=>$now],
        ]);

    }

}
