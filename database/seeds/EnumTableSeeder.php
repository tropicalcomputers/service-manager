<?php

use Illuminate\Database\Seeder;
use App\ServiceManager\ {
    Branch\Branch,
    Status\Status
};

class EnumTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Branch::truncate();
        Status::truncate();

        foreach ($this->getBranches() as $branch) {
            Branch::create($branch);
        }
        foreach ($this->getStatuses() as $status) {
            Status::create($status);
        }
    }

    protected function getStatuses()
    {
        return [
            ['name' => 'Active'],
            ['name' => 'Awaiting Customer Response'],
            ['name' => 'Awaiting Parts'],
            ['name' => 'Duplicate'],
            ['name' => 'Incomplete'],
            ['name' => 'Closed'],
            ['name' => 'RMA'],
            ['name' => 'Awaiting Billing'],
            ['name' => 'Awaiting Collection'],
            ['name' => 'Awaiting Delivery'],
            ['name' => 'Completed'],
        ];
    }

    protected function getBranches()
    {
        return [
            ['name'=>'Fontabelle'],
            ['name'=>'Collymore Rock'],
        ];
    }
}
