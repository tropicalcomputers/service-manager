<?php

use Illuminate\Database\Seeder;
use App\ServiceManager\Device\DeviceType;

class DeviceTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DeviceType::truncate();
        foreach ($this->getDeviceTypes() as $deviceType) {
            DeviceType::create($deviceType);

        }
    }

    protected function getDeviceTypes()
    {
        return [
            ['name'=>'Cpu'],
            ['name'=>'Notebook'],
            ['name'=>'Printer'],
            ['name'=>'Server'],
            ['name'=>'Other'],
        ];
    }
}
