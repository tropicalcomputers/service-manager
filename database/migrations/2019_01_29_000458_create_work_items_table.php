<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('device_id');
            $table->integer('priority_id');
            $table->integer('status_id')->nullable();
            $table->integer('branch_id')->nullable();
            $table->integer('signed_in_by')->nullable();
            $table->integer('assigned_to')->nullable();
            $table->boolean('warranty')->default(false);
            $table->boolean('support_contract')->default(false);
            $table->text('description')->nullable();
            $table->integer('time')->default(0);
            $table->text('notes')->nullable();
            $table->text('inventory_notes')->nullable();
            $table->string('invoice_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_items');
    }
}
