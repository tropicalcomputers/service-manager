<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkItemHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_item_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('work_item_id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('changed_by');
            $table->unsignedInteger('assigned_to')->nullable();
            $table->text('description');
            $table->boolean('customer_contacted');
            $table->smallInteger('time');
            $table->boolean('system_log')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_item_histories');
    }
}
