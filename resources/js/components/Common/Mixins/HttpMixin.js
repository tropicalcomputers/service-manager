export const HttpMixin = {
    data: function() {
        return {
            http: {
                action:null,
                resetState: false,
            },
        };
        },
    computed: {
        submitting: function(){
            return !!this.http.action;
        }
    },

    methods: {

        httpCreate() {
            this.http.action = 'create';
        },

        httpPatch() {
            this.http.action = 'patch';
        },

        resetHttpAction() {
            this.http.action = null;
        },

        resetHttpState() {
            this.http.resetState = true;
        },

        handleHttpStateReset() {
            this.http.resetState = false;
        },
    }
};

