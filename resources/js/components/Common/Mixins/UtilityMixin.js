export const UtilityMixin = {

    methods: {
        makeWarrantyLink(device) {
            switch (device.manufacturer.toLowerCase()) {
                case "dell":
                    return "https://www.dell.com/support/home/ie/en/iebsdt1/product-support/servicetag/" + device.serial;
                case "apc":
                    return "https://www.apc.com/us/en/tools/warranty-services/";
                case "hp":
                case "hewlett packard":
                    return "https://support.hp.com/us-en/checkwarranty";
                case "lenovo":
                    return "https://www.lenovo.com/us/en/warrantyApos?serialNumber=" + device.serial;
            }
            return false;
        }
    }
};

