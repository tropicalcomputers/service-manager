
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.prototype.$http = axios;

import vuetify from './vuetify'

import 'simple-line-icons/css/simple-line-icons.css';
import 'font-awesome/css/font-awesome.min.css';
import '@coreui/icons/css/coreui-icons.min.css';
import 'pace-progress/themes/green/pace-theme-minimal.css';

import Autocomplete from 'vuejs-auto-complete'
import DateRangePicker from 'vue2-daterange-picker'


import moment from 'moment'
Vue.prototype.moment = moment;



Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('http-helper', require('./components/Common/HttpHelper.vue').default);
Vue.component('modal', require('./components/Common/Modal.vue').default);
Vue.component('modal-launch-button', require('./components/Common/ModalLaunchButton.vue').default);
Vue.component('tcl-simple-auto-complete', require('./components/SimpleAutoCompleteComponent.vue').default);
Vue.component('tcl-auto-complete', require('./components/AutoCompleteComponent.vue').default);
Vue.component('material-text-box', require('./components/MaterialTextBox.vue').default);
Vue.component('device-add', require('./components/Devices/DeviceAdd.vue').default);
Vue.component('work-item-history-create', require('./components/WorkItemHistory/WorkItemHistoryCreate.vue').default);
Vue.component('dashboard', require('./components/Dashboard/DashboardComponent.vue').default);
Vue.component('work-item-create', require('./components/WorkItems/WorkItemCreate.vue').default);
Vue.component('work-items-index', require('./components/WorkItems/WorkItemsIndex.vue').default);
Vue.component('user-index', require('./components/Users/UserIndex.vue').default);
Vue.component('work-item-show', require('./components/WorkItems/WorkItemShow.vue').default);
Vue.component('create-customer-form', require('./components/Customers/CreateCustomerForm.vue').default);
Vue.component('errors', require('./components/Common/Errors.vue').default);
Vue.component('datatable', require('./components/Common/Datatable.vue').default);
Vue.component('pagination', require('./components/Common/Pagination.vue').default);
Vue.component('autocomplete', Autocomplete);
Vue.component('date-range-picker', DateRangePicker);
Vue.component('company-show', require('./components/Companies/CompanyShow.vue').default);
Vue.component('contact-show', require('./components/Companies/ContactShow.vue').default);
Vue.component('customer-show', require('./components/Customers/CustomerShow.vue').default);
Vue.component('device-show', require('./components/Devices/DeviceShow.vue').default);
Vue.component('device-types', require('./components/Devices/DeviceTypes.vue').default);
Vue.component('device-autocomplete', require('./components/Devices/DeviceAutoComplete.vue').default);
Vue.component('customer-autocomplete', require('./components/Customers/CustomerAutoComplete.vue').default);


Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('ddd, MMM Do YYYY h:mmA')
    }
});

Vue.filter('shortDate', function(value) {
    if (value) {
        return moment(String(value)).format('ddd, MMM Do YYYY')
    }
});

const app = new Vue({
    vuetify: vuetify,
    el: '#app',
    data: {
        currentUser: null,
        url: process.env.MIX_APP_URL,
    },
    created() {
        this.getMe();
    },
    methods: {
        getMe() {
            axios.get('/auth/me')
                .then(response => {
                    this.currentUser = Object.assign({}, this.enums, response.data);
                })
                .catch(error => {
                    console.log(error);
                })
        }

    }
});


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */





var $ = require('jquery');
window.$ = window.jQuery = $;
import 'jquery-ui/ui/widgets/autocomplete.js';

