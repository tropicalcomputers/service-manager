<html>
<head>
    <style>
        #page {
            width: 210mm;
        }
        .terms-and-conditions {
            font-size:0.8em;
            padding-top:1mm;
        }

        .text-center {
            text-align:center;
        }

        .text-right {
            text-align:right;
        }

        .strong {
            font-weight:bold;
        }

        .red {
            color:red;
        }

        .big-bold {
            font-weight:bold;
            font-size:1.2em;
        }

        .float-right {
            float:right;
        }

        .float-left {
            float:left;
        }

        .clearfix::after {
            content: "";
            clear: both;
            display: table;
        }
        .half {
            width:95mm;
        }

        .half.float-right {
            padding-left:5px;
        }

        .half.float-left {
            padding-right:5px;
        }

        .mb-1 {
            margin-bottom:5mm;
        }
        .mb-2 {
            margin-bottom:10mm;
        }
        .signature-line {
            width:80%;
            margin: 15mm auto 0mm auto;
            border-bottom:solid 1px #333;
        }

        .phone {
            margin-right:5mm;
        }

        .header-margin-top {
            margin-top:7mm;
        }

        .header-line {
            padding-bottom: 5mm;
            margin-bottom:5mm;
            border-bottom:solid 1px #999;
        }

    </style>
</head>
<body>
    <div id="page">
        <section class="clearfix mb-2 header-line">
            <article class="float-left" style="width:30mm">
                <img src="{{asset('images/assets/header-logo-small.png')}}"
                style="width:25mm;"
                />
            </article>
            <article class="float-left header-margin-top">
                "Gladstonia", Fontabelle, St. Michael <br />
                <strong>Tel:</strong> (246) 430-0306 <strong>Fax:</strong> (246) 228-3959 <br />
                <strong>Email:</strong> sales@tropicalcomputers.com <br />
            </article>
            <article class="clearfix float-right half text-right header-margin-top">
                <div class="big-bold">
                    {{$workItem->created_at->format('d M Y')}}
                </div>
                <div class="big-bold">
                    Service Request Receipt: {{$workItem->id}}
                </div>

            </article>
        </section>

        <section class="clearfix mb-1">
            <article class="float-left half">
                @if ($workItem->customer->company)
                    <div>
                        <strong>Company:</strong> {{$workItem->customer->company->name}}
                    </div>
                    <div>
                        <strong>Contact:</strong> {{$workItem->customer->full_name}}
                    </div>
                    <div>
                        <strong>Address:</strong> {{$workItem->customer->company->address}}
                    </div>
                    <div>
                        <strong>Company:</strong> <span class="phone"> {{$workItem->customer->company->telephone ?? "None"}}</span><br/>
                        <strong>Office:</strong> <span class="phone"> {{$workItem->customer->work_telephone ?? "None"}}</span><br />
                        <strong>Mobile:</strong> <span class="phone"> {{$workItem->customer->mobile_telephone ?? "None"}}</span>
                    </div>
                @else
                    <div>
                        <strong>Customer:</strong> {{$workItem->customer->fullname}}
                    </div>
                    <div>
                        <strong>Address:</strong> {{$workItem->customer->address ?? "None"}}
                    </div>
                    <div>
                        <strong>Home:</strong> <span class="phone"> {{$workItem->customer->home_telephone ?? "None"}}</span><br/>
                        <strong>Work:</strong> <span class="phone"> {{$workItem->customer->work_telephone ?? "None"}}</span><br />
                        <strong>Mobile:</strong> <span class="phone"> {{$workItem->customer->mobile_telephone ?? "None"}}</span>
                    </div>
                @endif

            </article>
            <article class="float-right half">
                <div>
                    <strong>Device:</strong> {{$workItem->device->name}}
                </div>
                <div>
                    <strong>Serial:</strong> {{$workItem->device->serial ?? "None"}}
                </div>
                <div>
                    <strong>Warranty:</strong> {{$workItem->warranty ? "Yes" : "No"}}
                </div>
                <div>
                    <strong>Support Contract:</strong> {{$workItem->support_contract ? "Yes" : "No"}}
                </div>
                <div>
                    <strong>Inventory:</strong> {{$workItem->inventoryOptions}}
                </div>
            </article>
        </section>
        <section class="description">
            <div class="big-bold">Description</div>
            <p>
                {{$workItem->description}}
            </p>
        </section>
        <section class="terms-and-conditions">
            <h3 class="text-center">TERMS AND CONDITIONS FOR EQUIPMENT DIAGNOSTICS AND REPAIRS</h3>
            <ol>
                <li>
                    <p class="strong">
                        A non-refundable deposit of $80.85 plus VAT is applicable to all equipment left at Tropical Computers for repairs. Your deposit is required for diagnostic checking of the equipment.
                    </p>
                    <p>
                        Where equipment is collected and/or delivered, a non-refundable deposit of $110 plus VAT is required for diagnostic checking and repairs are billed at a rate of $175 plus VAT per hour
                    </p>
                    <p>
                        We estimate that preliminary diagnostics will be completed within three (3) business days. The completion of repairs is dependent on the nature of your reported problem and availability of parts.
                    </p>
                </li>

                <li>
                    <p>
                        A Labour Rate of $150 plus VAT per hour is applicable to repairs. Your deposit is deducted from the final repair charge.
                    </p>
                </li>

                <li>
                    <p>
                        Emergency Diagnostics and Repairs are conducted by the next business day (dependent on the nature of problem and parts availability).
                    </p>
                    <p class="strong">
                        A non-fundable deposit of $130 plus VAT is required for Emergency Diagnostics. A Labour Rate of $225 plus VAT per hour is applicable to emergency repairs.
                    </p>
                    <p class="strong">
                        Where equipment is collected and/or delivered for emergency diagnostics and repairs, a non-refundable deposit of $150 plus VAT is required and repairs are billed at a rate of $275 plus VAT per hour.
                    </p>
                </li>

                <li>
                    <p class="red">
                        Tropical Computers Limited accepts no liability for the functionality of any parts, supplies or accessories provided by the Customer to effect repairs to his equipment. TCL reserves the right to void the labour warranty covering the repairs conducted using any customer-supplied item.
                    </p>
                </li>
                <li>
                    <p>
                        The customer is responsible for maintaining current backups of data and programs. Tropical Computers is released of all liability for data and programs as well as any loss or damages due to the inability to use the equipment for any reason. The cost of performing backups including supplies CDs, tape cartridges, USB drives etc) will be borne by the Customer. The Customer is required to supply the operating system software and other applications in order that TCL may conduct effective repairs to his equipment.
                    </p>
                </li>
                <li>
                    <p>
                        Completed repairs are warranted for a period of 30 days. Parts used to effect repairs are warranted for a period of 90 days unless otherwise stated on an approved TCL invoice.
                    </p>
                </li>
                <li class="red">
                    <p>
                        It is the responsibility of the customer to pick up the equipment within 20 business days of notification of completion of services. Any equipment not collected within 20 business days will incur a daily storage fee of $10, up to a maximum of $250. Equipment not collected within 45 business days of notification of completion of services will be deemed abandoned and disposed of by Tropical Computers
                    </p>
                </li>
            </ol>
        </section>

        <section>
            <p class="text-center">
                <strong>
                    I hereby acknowledge that I have read and understand that Tropical Computers has explained these terms and conditions to me.
                </strong>
            </p>
        </section>
        <!-- Agree T&Cs -->
        <section class="clearfix">
            <article class="float-left half">
                <div class="text-center">
                    <strong>Signature</strong>
                    <div class="signature-line">

                    </div>
                </div>
            </article>
            <article class="float-right half">
                <div class="text-center">
                    <strong>Date</strong>
                    <div class="signature-line" style="position:relative;">
                        <div style="position: absolute; bottom:5mm; left: 1%; right: 1%;">
                            {{\Carbon\Carbon::now()->format('d M Y')}}
                        </div>
                    </div>
                </div>
            </article>
        </section>

        <section class="clearfix" style="page-break-inside:avoid;">
            <article>
                <p>
                    I/We confirm that the equipment repaired by Tropical Computers has been checked prior to my collection and confirmed to be working.
                    <br />
                    Yes <input type="checkbox"> No <input type="checkbox">
                </p>
            </article>
            <article>
                <p>
                    I/We confirm that the equipment has been checked for diagnostics only by Tropical Computers and may not be working. I/We are no longer pursuing further repairs.
                    <br />
                    Yes <input type="checkbox"> No <input type="checkbox">
                </p>
            </article>

        <!-- Collection Delcaration -->
            <article class="float-left half">
                <div class="text-center">
                    <strong>Collected By</strong>
                    <div class="signature-line">

                    </div>
                </div>
            </article>
            <article class="float-right half">
                <div class="text-center">
                    <strong>Date</strong>
                    <div class="signature-line" style="position:relative;">
                        <div style="position: absolute; bottom:5mm; left: 1%; right: 1%;">

                        </div>
                    </div>
                </div>
            </article>
        </section>
    </div>
<script>
    window.print();
</script>
    </body>
</html>
