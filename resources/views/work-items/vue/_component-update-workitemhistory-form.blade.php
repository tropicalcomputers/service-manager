<script>
    function updateWorkItemDefaultData() {
        return {
            form: {
                submitting: false,
                errors: {},
                data: {
                    workItemHistory: {
                        work_item_id: null,
                        status_id: "@php echo $workItem->status->id ?? null; @endphp",
                        branch_id: "@php echo $workItem->branch->id ?? null; @endphp",
                        priority_id: "@php echo $workItem->statusId; @endphp",
                        description: null,
                        customer_contacted:false,
                        time:0
                    }
                }
            },
            statuses: @php echo $statuses @endphp,
            branches: @php echo $branches @endphp,
        priorities: @php echo $priorities @endphp,
        availableComponents: []
        };
    }

    Vue.component('updateworkitemhistoryform', {
        data: function () {
            return updateWorkItemDefaultData();
        },
        watch: {
        },
        methods:{
            reset() {
                Object.assign(this.$data, updateWorkItemDefaultData())
            },
            updateWorkItem() {
                axios.post('{{route('work-items.update',[$workItem->id])}}',
                    this.form.data
                )
                    .then(response => {
                        console.log(response);

                        if (response.status === 200) {
                            app.setDevice(response.data);
                            $('#newDeviceModal').modal('hide');
                            this.reset();
                        }
                    })
                    .catch(e => {
                        this.form.errors = e.response.data.errors;
                        console.log(e);
                        if (e.response) {
                            console.log(e.response);
                        }
                        // this.errors.push(e)
                    })
            },
        },
        template: `@include('work-items._snippets._change-form')`
    });

</script>
