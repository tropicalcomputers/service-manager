@extends('layouts.dashboard.master')

@section('title', 'Create A New Company')
@push('styles')
    <style>
        .entity-card {
            min-height: 165px;
        }
        .white-opacity-50 {
            background: rgba(255,255,255,0.5);
        }
    </style>
@endpush
@section('breadcrumbs')
    <li class="breadcrumb-item">
        <a href="{{route('work-items.index')}}">Work Items</a>
    </li>
    <li class="breadcrumb-item active">Add New</li>
@endsection

@section('content')
    <work-item-create></work-item-create>
@endsection
