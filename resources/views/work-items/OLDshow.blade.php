<?php
$scripts = ['datatables'];
?>
@extends('layouts.dashboard.master')

@section('title', 'Work Item')
@push('styles')
    <style>
    </style>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item">
        <a href="{{route('work-items.index')}}">Work Items</a>
    </li>
    <li class="breadcrumb-item active">Work Item</li>
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header alt">
                    <i class="fa fa-align-justify"></i> Work Item
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <p><strong>Device:</strong> @{{workItem.device.manufacturer + ' ' + workItem.device.model}}</p>
                            <p><strong>Serial:</strong> @{{workItem.device.serial}}</p>
                            <p><strong>Password:</strong> @{{workItem.device.password}}</p>
                            <p><strong>Created At:</strong> @{{workItem.created_at | formatDate}}</p>
                            <p><strong>Signed In By:</strong> @{{workItem.signed_in_by}}</p>
                            <p><strong>Assigned To:</strong> @{{workItem.assigned_to}}</p>

                        </div>
                        <div class="col">
                            <p><strong>Branch:</strong> @{{workItem.branch ? workItem.branch.name : "Unknown"}}</p>
                            <p><strong>Company:</strong> @{{workItem.customer.company ? workItem.customer.company.name : "N/A"}}</p>
                            <p><strong>Contact:</strong> @{{workItem.customer.full_name}}</p>
                            <p><strong>Priority:</strong> @{{workItem.priority ? workItem.priority.name : 'Unknown'}}</p>
                            <p><strong>Status:</strong> @{{workItem.status ? workItem.status.name : 'Unknown'}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p><strong>Description:</strong><br />
                                @{{workItem.issue_description}}
                            </p>

                        </div>
                    </div>
                </div>
            </div>
            <div class="card mt-5">
                <div class="card-header alt">
                    <i class="fa fa-align-justify"></i> Change History
                    <div class="card-header-actions">
                        <button class='btn btn-primary' href='#' data-toggle='modal' data-target='#newChangeModal'>Add A Change</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card" v-for="history in workHistories">
                        <div class="card-header">
                            @{{history.changed_by.name}} said...
                            <div class="card-header-actions">
                                <small>@{{history.created_at | formatDate}}</small>
                                <span v-if="history.customer_contacted" class="badge badge-pill badge-primary ml-2">Customer Contacted</span>
                                <span class="badge badge-pill badge-success ml-2">@{{ history.time }}</span>
                            </div>
                        </div>
                        <div class="card-body">
                            @{{history.description}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <work-item-history-form
    url="{{route('work-item-histories.store')}}"
    :user = "{{\Illuminate\Support\Facades\Auth::user()}}"
    :statuses = "{{$statuses}}"
    :branches = "{{$branches}}"
    :priorities = "{{$priorities}}"
    :workItemHistory = "{{$workItem->history->first()}}"
    @created = "historyCreatedHandler"
    ></work-item-history-form>
@endsection
@push('scripts')

    <script>

        const app = new Vue({
            el: '#app',
            data: {
                workItem: @php echo $workItem @endphp,
                workHistories: @php echo $histories @endphp,
            },
            watch: {
            },
            computed: {
            },
            methods:{
                deviceSelected(device) {
                    console.log(device);
                },
                historyCreatedHandler(history) {
                    this.workHistories.unshift(history);
                    $('#newChangeModal').modal('hide');
                }
            }
        });

        $( document ).ready(function() {
            $('.datatable').DataTable({
                "ajax": "data/arrays.txt",
                "deferRender": true,
                "columns": [
                    null,
                    null,
                    null,
                    { "width": "110px" }
                ]
            });
            $('.datatable').attr('style','border-collapse: collapse !important');
        });




    </script>
@endpush
