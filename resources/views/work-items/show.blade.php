<?php
$scripts = ['datatables'];
?>
@extends('layouts.dashboard.master')

@section('title', 'Work Item')
@push('styles')
    <style>
    </style>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item">
        <a href="{{route('work-items.index')}}">Work Items</a>
    </li>
    <li class="breadcrumb-item active">Work Item</li>
@endsection

@section('content')
    <work-item-show
    :work-item-object ="{{$workItem}}"
    :current-user="this.$root.currentUser"
    ></work-item-show>
@endsection

@push('scripts')
@endpush
