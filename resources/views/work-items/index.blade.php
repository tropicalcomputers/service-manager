@extends('layouts.dashboard.master')

@section('title', 'Work Items')
@push('styles')
    <style>
    </style>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item active">Work Items</li>
@endsection

@section('content')
<work-items-index
:filters = "{{$filters}}"
></work-items-index>
@endsection
@push('scripts')
@endpush
