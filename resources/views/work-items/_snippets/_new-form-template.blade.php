<div id="newWorkItem">
    <div class="row">
        <div class="col-md-4">
            <div class="card mb-4">
                <div class="card-body">
                    <h4>Company</h4>
                    <p>@{{ form.customer.company ? form.customer.company.name : 'N/A' }}</p>

                    <h4>Person</h4>
                    <p>@{{ form.customer.person ? personFullName : 'N/A' }}</p>
                </div>
            </div>

        </div>
        <div class="col-md-8">

            <div class="card border-0">
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">System</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Info</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="messages-tab" data-toggle="tab" href="#messages" role="tab" aria-controls="messages" aria-selected="false">Inventory</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content p-3 mt-3">
                        <div class="tab-pane active " id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-8">
                                    <div class="form-group form-row d-flex align-items-center clearfix">

                                        <label for="serial" class="col-4 mb-0">Search Existing (Serial):</label>
                                        <input id="existingDevice" type="text" class="form-control col-8">
                                    </div>

                                    <div class="form-group form-row d-flex align-items-center">
                                        <label for="manufacturer" class="col-4 mb-0">Manufacturer:</label>
                                        <input type="text" class="form-control col-8" id="manufacturer" :value="form.device.manufacturer" disabled>
                                    </div>

                                    <div class="form-group form-row d-flex align-items-center">
                                        <label for="model" class="col-4 mb-0">Model:</label>
                                        <input type="text" class="form-control col-8" id="model" :value="form.device.model" disabled>
                                    </div>

                                    <div class="form-group form-row d-flex align-items-center">
                                        <label for="model" class="col-4 mb-0">Serial:</label>
                                        <input type="text" class="form-control col-8" id="serial" :value="form.device.serial" disabled>
                                    </div>

                                    <div class="form-group form-row d-flex align-items-center">
                                        <label for="password" class="col-4 mb-0" id='personNameHelp' data-toggle="tooltip" data-placement="top" title="Optional Password">Password:</label>
                                        <input type="text" class="form-control col-8" id="password" :value="form.device.password">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div v-if="form.customer.person.id == 30" class="border">
                                        <div class="clearfix alert-info p-3">
                                            Devices
                                        </div>
                                        <ul v-if="customerDevices.length" class="list-group list-group-flush" >
                                            <li v-for="device in customerDevices" class="list-group-item clearfix">
                                                <button class="btn btn-sm btn-primary float-right select-device" @click="selectDevice(device.id)">Select</button>
                                                @{{ device.manufacturer }}  @{{ device.model }}
                                                <div><small>@{{device.serial}}</small></div>
                                            </li>
                                        </ul>
                                        <div class="clearfix p-3">
                                            <button data-toggle="modal" data-target="#deviceModal" class="btn btn-primary btn-sm float-right">Add New Device</button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="form-group form-row d-flex align-items-center">
                                <label for="branch" class="col-2 mb-0">Location</label>
                                <select class="form-control col-10" id="branch">
                                    <option value=0>Fontabelle</option>
                                </select>
                            </div>

                            <div class="form-group form-row d-flex align-items-center">
                                <label for="priority" class="col-2 mb-0">Priority</label>
                                <select class="form-control col-10" id="priority" >
                                    <option value="0">High</option>
                                    <option value="1" selected>Normal</option>
                                </select>
                            </div>
                            <div class="form-group mt-3">
                                <label for="description" class="">Description</label>
                                <textarea class="form-control" id="description" rows="5" aria-describedby="" placeholder="Type description of issue here"></textarea>
                            </div>
                            <div class="form-group mt-3">
                                <label for="notes" class="">Notes</label>
                                <textarea class="form-control" id="notes" rows="5" aria-describedby="" placeholder="Type notes here" ></textarea>
                            </div>
                        </div>
                        <div class="tab-pane" id="messages" role="tabpanel" aria-labelledby="messages-tab">
                            <div id="inventoryFields">
                                <div class="form-group form-row d-flex align-items-center">
                                    <label for="device" class="col-2 mb-0">Device</label>
                                    <select class="form-control col-10" id="device">
                                        <option value="">Choose A Device</option>
                                        <option v-for="deviceType in form.deviceTypes" :value="deviceType.name" class="alert-danger">
                                            @{{ deviceType.name }}
                                        </option>
                                    </select>
                                </div>
                                <div>Components</div>

                                <div id="components" class="mt-2 p-2 border form-group">
                                    <div class="form-check form-check-inline" v-for="option in form.selectedDeviceOptions">
                                        <input class="form-check-input" type="checkbox" :id="'component' + option " :value="option" >
                                        <label class="form-check-label" :for="'component' + option ">@{{option}}</label>
                                    </div>

                                </div>

                                <div class="form-group mt-3">
                                    <label for="additionalInformation" class="">Additional Information</label>
                                    <textarea class="form-control" id="additionalInformation" rows="5" aria-describedby="" placeholder="Type additional information here"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
