<div class="modal fade" id="newChangeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
    <div class="modal-dialog modal-xl modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Change</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="newDeviceFormWrapper">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    Changes by: {{\Illuminate\Support\Facades\Auth::user()->name}}
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col-sm-4">
                                            <label for="branch">Status</label>
                                            <select class="form-control" id="branch" v-model="form.data.workItemHistory.status_id">
                                                <option v-for="status in statuses" :value="status.id">@{{ status.name }}</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="branch">Branch</label>
                                            <select class="form-control" id="branch" v-model="form.data.workItemHistory.branch_id">
                                                <option v-for="branch in branches" :value="branch.id">@{{ branch.name }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-4">
                                            <label class="" for="time">Time</label>
                                            <input class="form-control disable-autocomplete" id="time" v-model="form.data.workItemHistory.time" placeholder="" />
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label class="form-check-label" for="customerContacted">Customer Contacted</label>
                                            <input type="checkbox" class="form-control" id="customerContacted" v-model="form.data.workItemHistory.customer_contacted" placeholder="" />
                                        </div>
                                    </div>



                                    <div class="form-field">
                                        <div class="form-group form-field__control">
                                            <label class="form-field__label" for="description">Description</label>
                                            <textarea style='max-height:100px;' class="form-field__textarea" id="description" v-model="form.data.workItemHistory.description" placeholder=""></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- /.col-->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger float-right" type="button" @click="reset()">
                    <i class="fa fa-ban"></i> Reset
                </button>
                <button class="btn btn-primary" type="button" @click="saveDevice()">Save changes</button>
            </div>
        </div>

    </div>
</div>
@push('scripts')

@endpush
