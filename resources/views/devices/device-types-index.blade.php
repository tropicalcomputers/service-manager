@extends('layouts.dashboard.master')

@section('title', 'Device Types')
@push('styles')
    <style>
    </style>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item">
        <a href="{{route('dashboard.index')}}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Device Types</li>
@endsection

@section('content')
    <device-types></device-types>
@endsection

@push('scripts')
@endpush
<script>

</script>
