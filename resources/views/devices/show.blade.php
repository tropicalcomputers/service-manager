@extends('layouts.dashboard.master')

@section('title', 'Device')
@push('styles')
    <style>
    </style>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item">
        <a href="{{route('work-items.index')}}">Work Items</a>
    </li>
    <li class="breadcrumb-item active">Device</li>
@endsection

@section('content')
    <device-show
        :device = "{{$deviceJson}}"
        :current-user="this.$root.currentUser"
    ></device-show>
@endsection

@push('scripts')
@endpush
