<div class="row" >
    <div class="col">
        <div v-if="form.submitting == true" class="card alert-info">
            <div class="card-body">
                Submitting...
            </div>
        </div>
        <div v-if="Object.keys(form.errors).length !== 0" class="card alert-danger">
            <div class="card-body">
                <ul>
                    <li class="" v-for="error in form.errors">
                        @{{ error[0] }}
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
