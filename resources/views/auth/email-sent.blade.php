@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Sweet sweet...') }}</div>

                <div class="card-body">
                    An email has been sent to your email address with a one-time use login link.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
