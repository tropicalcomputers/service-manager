
<p>
    Click the link below to sign into {{config('app.name')}}.
</p>

<p>
    This one-time use link will expire in {{$expireMinutes}} minutes: <a href="{!! $signedUrl !!}" target="_blank">Sign In</a>
</p>

<p>
    You can also copy and paste the URL directly into your browser: <br />{!! $signedUrl !!}
</p>
