@extends('layouts.dashboard.master')

@section('title', 'Contact')
@push('styles')
    <style>
    </style>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item">
        <a href="{{route('companies.show',[$contact->company->id])}}">{{$contact->company->name}}</a>
    </li>
    <li class="breadcrumb-item active">Contact</li>
@endsection

@section('content')
    <contact-show
        :contact = "{{$contactJson}}"
    ></contact-show>
@endsection

@push('scripts')
@endpush
