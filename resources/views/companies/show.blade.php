@extends('layouts.dashboard.master')

@section('title', 'Company')
@push('styles')
    <style>
    </style>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item">
        <a href="{{route('work-items.index')}}">Work Items</a>
    </li>
    <li class="breadcrumb-item active">Company</li>
@endsection

@section('content')
    <company-show
        company-id={{$company->id}}
        :current-user="this.$root.currentUser"
    ></company-show>
@endsection

@push('scripts')
@endpush
