<?php
$scripts = ['datatables'];
?>
@extends('layouts.dashboard.master')

@section('title', 'Page Title')
@push('styles')
    <style>
    </style>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item active">Companies</li>
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> All Companies
                    <div class="card-header-actions">
                        <a class="btn btn-primary" href="{{route('companies-old')}}" target="">
                            Create New
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-responsive-sm datatable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th style="width:auto; max-width:100px !important;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($companies as $company)
                        <tr>
                            <td>{{$company->name}}</td>
                            <td>{{$company->address}}</td>
                            <td>{{$company->phone}}</td>
                            <td style="max-width:100px;">
                                <a class="btn btn-success" href="{{route('companies-old',['id'=>$company->id])}}">
                                    <i class="fa fa-search-plus"></i>
                                </a>
                                <a class="btn btn-info" href="#">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a class="btn btn-danger" href="#">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script>

        const app = new Vue({
            el: '#app',
            data: {
            },
            watch: {
            },
            computed: {
            },
            methods:{
            }
        });

        $( document ).ready(function() {
            $('.datatable').DataTable({
                "columns": [
                    null,
                    null,
                    null,
                    { "width": "110px" }
                ]
            });
            $('.datatable').attr('style','border-collapse: collapse !important');
        });
    </script>
@endpush
