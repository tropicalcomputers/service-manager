<?php
$scripts = ['datatables'];
?>
@extends('layouts.dashboard.master')

@section('title', 'Create A New Company')
@push('styles')
    <style>
    </style>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item">
        <a href="#">Companies</a>
    </li>
    <li class="breadcrumb-item active">{{$company->name}}</li>
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="mb-4">
                <div class="card">
                    <div class="card-header">
                        <strong>Company Info</strong>
                    </div>
                    <div class="card-body">
                        <h2>{{$company->name}}</h2>
                        <p>
                            {{nl2br($company->address)}}
                        </p>
                        <strong>Phone: </strong> {{$company->telephone}}
                    </div>
                </div>
            </div>
            <div class="mb-4">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active show" data-toggle="tab" href="#home4" role="tab" aria-controls="home" aria-selected="true">
                            <i class="icon-calculator"></i> Work Items
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#profile4" role="tab" aria-controls="profile" aria-selected="false">
                            <i class="icon-basket-loaded"></i> Devices
                            <span class="badge badge-pill badge-danger">29</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#messages4" role="tab" aria-controls="messages" aria-selected="false">
                            <i class="icon-pie-chart"></i> Contacts</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active show" id="home4" role="tabpanel">
                        <table class="table table-striped table-hover table table-borderless table-responsive-sm datatable">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Last Updated</th>
                                <th style="width:auto; max-width:100px !important;">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>13</td>
                                <td>This is the description</td>
                                <td>Status</td>
                                <td>4pm</td>
                                <td style="max-width:100px;">
                                    <a class="btn btn-success" href="#">
                                        <i class="fa fa-search-plus"></i>
                                    </a>
                                    <a class="btn btn-info" href="#">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a class="btn btn-danger" href="#">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                    <div class="tab-pane" id="profile4" role="tabpanel">
                        2. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                    </div>
                    <div class="tab-pane" id="messages4" role="tabpanel">
                        <div v-if="contacts.length" class="">
                            <table class="table table-hover table table-borderless table-responsive-sm datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th>ID</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th style="width:auto; max-width:100px !important;">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="border-bottom" v-for="contact in contacts">
                                        <td>@{{ contact.id }}</td>
                                        <td>@{{ contact.first_name }}</td>
                                        <td>@{{ contact.last_name }}</td>
                                        <td>@{{ contact.email }}</td>
                                        <td style="max-width:100px;">
                                            <button
                                                class="btn btn-sm btn-outline-primary"
                                                @click="selectCustomer(contact.id)"
                                                type="button"
                                                data-toggle="modal"
                                                data-target="#primaryModal">
                                                <i class="fa fa-search-plus"></i>
                                            </button>
                                            <a class="btn btn-sm btn-outline-primary" href="#">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="btn btn-sm btn-outline-primary" href="#">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!-- /.col-->
    </div>

    <div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
        <div class="modal-dialog modal-xl modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Work Item</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                   <work-item-form
                       :person="newWorkItem.currentCustomer.person"
                       :company="newWorkItem.currentCustomer.company"

                   ></work-item-form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="button">Save changes</button>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('scripts')
    <script>

        Vue.component('work-item-form', {
            props: ['person','company'],
            watch: {
                person: function () {
                    this.form.customer.person = this.person;
                },
                company: function () {
                    this.form.customer.company = this.company;
                }
            },
            computed: {
                // a computed getter
                personFullName: function () {
                    return this.form.customer.person.first_name ? this.form.customer.person.first_name + " " + this.form.customer.person.last_name : "";
                }
            },
            data: function () {
                return {
                    form: {
                        customer: {
                            person:{first_name:"jacob"},
                            company:{},
                        },
                        device: {},
                        info: {},
                        inventory: {}
                    },
                }
            },
            template: `@include('work-items._snippets._new-form-template')`
        });

        const app = new Vue({
            el: '#app',
            data: {
                company: {!! $company !!},
                contacts: {!! $company->contacts !!},

                newWorkItem: {
                    currentCustomer: {
                        person: {},
                        company: {}
                    }
                }
            },
            watch: {
            },
            computed: {
            },
            methods:{
                selectCustomer(contactId) {
                    app.newWorkItem.currentCustomer.person = this.company.contacts.find(contact => contact.id === contactId);
                    app.newWorkItem.currentCustomer.company = this.company;
                },
            }
        });

        $( document ).ready(function() {
            $('.datatable').DataTable({
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    { "width": "110px" }
                ]
            });
            $('.datatable').attr('style','border-collapse: collapse !important');
        });
    </script>
@endpush
