@extends('layouts.dashboard.master')

@section('title', 'Create A New Company')
@push('styles')
    <style>
    </style>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item">
        <a href="#">Companies</a>
    </li>
    <li class="breadcrumb-item active">Add New</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-6 offset-sm-3">
            <div class="card">
                <div class="card-header">
                    <strong>Add A Company</strong>
                </div>
                <form method="POST" action="{{route('companies-old')}}">
                    <div class="card-body">
                        <div class="form-field">
                            <div class="form-field__control">
                                <label class="form-field__label" for="company">Company Name</label>
                                <input class="form-field__input" id="company" type="text" v-model="form.name" placeholder="">
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="form-group form-field__control">
                                <label class="form-field__label" for="address">Address</label>
                                <textarea class="form-field__textarea" id="address" v-model="form.address"  placeholder=""></textarea>
                            </div>
                        </div>

                        <div class="form-field">
                            <div class="form-field__control">
                                <label class="form-field__label" for="country">Country</label>
                                <input class="form-field__input" id="country" type="text" v-model="form.country" value="Barbados" placeholder="">
                            </div>
                        </div>

                        <div class="form-field">
                            <div class="form-field__control">
                                <label class="form-field__label" for="telephone">Telephone</label>
                                <input class="form-field__input" id="telephone" type="text" v-model="form.telephone" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer clearfix">
                        <button class="btn btn-success float-right ml-1" type="submit">
                            <i class="fa fa-dot-circle-o"></i> Save
                        </button>
                        <button class="btn btn-danger float-right" type="reset">
                            <i class="fa fa-ban"></i> Reset
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.col-->
    </div>
@endsection
@push('scripts')
    <script>

        const app = new Vue({
            el: '#app',
            data: {
                form: {
                    name: "",
                    address: "",
                    telephone: "",
                    country: "Barbados",
                }
            },
            watch: {
            },
            computed: {
            },
            methods:{
            }
        });

        $( document ).ready(function() {

        });
    </script>
@endpush
