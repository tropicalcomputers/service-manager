
<footer class="app-footer">
    <div>
        <a href="https://coreui.io">Jerome Fitzpatrick</a>
        <span>&copy; 2019 jeromefitzpatrick.</span>
    </div>
    <div class="ml-auto">
        <span>Powered by</span>
        <a href="https://www.jeromefitzpatrick.com">Jerome Fitzpatrick</a>
    </div>
</footer>
