<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">Service Manager</li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('dashboard.index')}}">
                    <i class="nav-icon icon-speedometer"></i> Dashboard
                </a>
            </li>
            <li class="nav-title">Work Items</li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('work-items.create')}}">
                    <i class="nav-icon icon-doc"></i> New Work Item</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('work-items.index')}}">
                    <i class="nav-icon icon-list"></i> All Work Items</a>
            </li>


            <li class="nav-title">Tools</li>


            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-cursor"></i> Warranty Links</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.dell.com/support/home/bb/en/bbbsdt1/?c=bb&l=en&s=bsd" target="_blank">
                            <i class="nav-icon icon-doc"></i>Dell</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.apc.com/us/en/tools/warranty-services/" target="_blank">
                            <i class="nav-icon icon-doc"></i>APC</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://support.hp.com/us-en/checkwarranty" target="_blank">
                            <i class="nav-icon icon-doc"></i>HP</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://pcsupport.lenovo.com/us/en/warrantylookup" target="_blank">
                            <i class="nav-icon icon-doc"></i>Lenovo</a>
                    </li>
                </ul>
            </li>


            <li class="nav-title">Settings</li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('device-types.index')}}">
                    <i class="nav-icon icon-list"></i> Device Types</a>
            </li>
        </ul>

    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
