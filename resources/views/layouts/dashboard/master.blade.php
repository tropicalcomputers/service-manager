<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Service Manager') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href='https://fonts.googleapis.com/css?family=Nunito|Roboto:300,400,500,700|Material+Icons' rel="stylesheet">


    <!-- Icons
    <link href="/vendor/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
    <link href="/vendor/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <link href="/vendor/@coreui/css/style.css" rel="stylesheet">

    <link href="/vendor/pace-progress/css/pace.min.css" rel="stylesheet">
-->
    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <style>
        [v-cloak] {
            display: none;
        }

        body {
            font-family: Roboto, Nunito, sans-serif !important;
        }
        .ui-autocomplete-loading {
            background:url(/images/loading_16x16.gif) no-repeat right center;
        }
        .ui-menu-item .ui-menu-item-wrapper {
         padding:10px;
            border-bottom:solid 1px #DDD;
        }
        .ui-menu-item .ui-menu-item-wrapper:hover {
            background: #20a8d8;
            border-color:transparent;
        }
        .ui-widget-content.ui-autocomplete {
            z-index:1050;
        }

    </style>

    @stack('styles')


</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<div id="app">
    <v-app v-cloak>
    @include('layouts.dashboard.header')


    <div class="app-body">
        @include('layouts.dashboard.sidebar')
        <main class="main">
            <!-- Breadcrumb-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('dashboard.index')}}">Home</a>
                </li>
                @yield('breadcrumbs')

                <!-- Breadcrumb Menu-->

            </ol>

            <div class="container-fluid">
                <div id="ui-view">
                    <div>
                        <div class="animated fadeIn" style="display:none;">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </main>
        @include('layouts.dashboard.aside')
    </div>

    @include('layouts.dashboard.footer')
    </v-app>
</div>
<!-- Scripts -->

<script src="{{ mix('js/app.js') }}"></script>
<script src="/vendor/popper.js/dist/umd/popper.min.js"></script>
<script src="/vendor/pace-progress/pace.min.js"></script>
<script src="/vendor/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
<script src="/vendor/@coreui/coreui/dist/js/coreui.min.js"></script>
<script src="/vendor/chart.js/dist/Chart.min.js"></script>
<script src="/vendor/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js"></script>

<?php $scripts = $scripts ?? [] ;?>
@if (in_array('charts', $scripts))
    <script src="/js/coreui/main.js"></script>
@endif

@if (in_array('datatables', $scripts))
    <script type="text/javascript" src="/vendor/datatables.net/js/jquery.dataTables.js" class="view-script"></script>
    <script type="text/javascript" src="/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js" class="view-script"></script>
@endif

<script>
    // $('#ui-view').ajaxLoad();
    $(document).ready(function() {
        $('#ui-view > div > div').show();
    });
</script>
@stack('scripts')
</body>

</html>






