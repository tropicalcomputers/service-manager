<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{route('dashboard.index')}}">
        <img class="navbar-brand-full" src="{{asset('images/assets/header-logo.png')}}" width="97" height="33" alt="TCL Logo">
        <img class="navbar-brand-minimized" src="{{asset('images/assets/header-logo-small.png')}}" width="30" height="35" alt="CoreUI Logo">
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
            <a class="nav-link" href="{{route('dashboard.index')}}">Dashboard</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="{{route('work-items.create')}}">Create Work Item</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="{{route('work-items.index')}}">Work Items</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="{{route('users.index')}}">Users</a>
        </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img class="img-avatar" src="{{asset('images/assets/header-logo-small.png')}}" alt="">
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
                    <i class="fa fa-lock"></i> {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
    <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
        <span class="navbar-toggler-icon"></span>
    </button>
</header>
