@extends('layouts.dashboard.master')

@section('title', 'Users')

@section('breadcrumbs')
    <li class="breadcrumb-item active">Users</li>
@endsection

@section('content')
    <user-index
        :users="{{$usersJson}}"
    >
    </user-index>
@endsection
