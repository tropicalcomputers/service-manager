<div class="modal fade" id="newCustomerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
    <div class="modal-dialog modal-xl modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Customer</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="newCustomerFormWrapper">

                    <div class="row">
                        <div class="col-12">
                            <div class="form-check form-group form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="Individual" v-model="form.contactType">
                                <label class="form-check-label" for="inlineRadio1">Individual</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Company" v-model="form.contactType">
                                <label class="form-check-label" for="inlineRadio2">Company & Contact</label>
                            </div>
                        </div>
                    </div>

                    <div class="row" v-show="form.contactType == 'Company'">
                        <div class="col-4">
                            <div class="form-group ui-widget">
                                <tcl-auto-complete
                                    ref="existingcompany"
                                    input="existingCompanies"
                                    placeholder="Search Existing Companies"
                                    url="{{route('companies')}}"
                                    template= "%%name%%"
                                    modal=""
                                    @selected="setExistingCompany"
                                ></tcl-auto-complete>
                            </div>
                        </div>
                        <div class="col-8">
                            <button class='btn btn-outline-primary' @click="showCompanyForm()">Add New Company</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="card border-0" v-if="form.existingCompany.id">
                                <div class="card-body">
                                    <h4>{{ form.existingCompany.name }}</h4>
                                    {{ form.existingCompany.address }} <br />
                                    {{ form.existingCompany.telephone }} <br />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col">
                            <div v-if="form.submitting == true" class="card alert-info">
                                <div class="card-body">
                                    Submitting...
                                </div>
                            </div>
                            <div v-if="Object.keys(form.errors).length !== 0" class="card alert-danger">
                                <div class="card-body">
                                    <ul>
                                        <li class="" v-for="error in form.errors">
                                            @{{ error[0] }}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="card border-0" v-show="form.data.addNewCompany && !form.existingCompany.id">
                                <div class="card-header alt">
                                    Add A New Company
                                </div>
                                <div class="card-body company-fields">
                                    <div class="row">
                                        <div class="col">
                                            <v-text-field
                                                label="Company Name"
                                                class="disabe-autocomplete"
                                                v-model="form.data.company.name"
                                            ></v-text-field>
                                            <v-text-field
                                                label="Address"
                                                class="disabe-autocomplete"
                                                v-model="form.data.company.address"
                                            ></v-text-field>
                                            <v-text-field
                                                label="Telephone"
                                                class="disabe-autocomplete"
                                                v-model="form.data.company.telephone"
                                            ></v-text-field>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="card border-0">
                                <div class="card-header alt">
                                    Add
                                        <span v-if="form.contactType == 'Company'">Company Contact</span>
                                        <span v-else>Individual</span>

                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <v-text-field
                                                label="First Name"
                                                class="disabe-autocomplete"
                                                v-model="form.data.customer.first_name"
                                            ></v-text-field>
                                        </div>
                                        <div class="col">
                                            <v-text-field
                                                label="Last Name"
                                                class="disabe-autocomplete"
                                                v-model="form.data.customer.last_name"
                                            ></v-text-field>
                                        </div>
                                    </div>

                                    <button type="button" v-if="form.existingCompany.address" class="mt-1 btn btn-sm btn-outline-primary inline" onClick="document.getElementById('customer_address').focus()" @click="copyCompanyAddress">Copy Company Address</button>

                                    <md-field>
                                        <label>Address</label>
                                        <md-textarea class="disable-autocomplete" :md-autogrow=true v-model="form.data.customer.address"></md-textarea>
                                    </md-field>

                                    <md-field>
                                        <label>Email</label>
                                        <md-input class="disable-autocomplete" v-model="form.data.customer.email"></md-input>
                                    </md-field>



                                    <div class="row">
                                        <div class="col">
                                            <md-field>
                                                <label>Home Phone</label>
                                                <md-input class="disable-autocomplete" v-model="form.data.customer.home_telephone"></md-input>
                                            </md-field>
                                        </div>
                                        <div class="col">
                                            <md-field>
                                                <label>Work Phone</label>
                                                <md-input class="disable-autocomplete" v-model="form.data.customer.work_telephone"></md-input>
                                            </md-field>
                                        </div>
                                        <div class="col">
                                            <md-field>
                                                <label>Mobile Phone</label>
                                                <md-input class="disable-autocomplete" v-model="form.data.customer.mobile_telephone"></md-input>
                                            </md-field>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <!-- /.col-->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" @click="reset()">Reset</button>
                <a href="#newCustomerFormWrapper" class="btn btn-primary" @click="saveCustomer()">Save changes</a>
            </div>
        </div>

    </div>
</div>

@push('scripts')
    <script>
        $( document ).ready(function() {
        });
    </script>
@endpush
