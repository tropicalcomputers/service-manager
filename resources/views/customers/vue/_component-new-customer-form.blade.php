<script>
    function getNewCompanyDefault() {
        return {
            name:"",
            address: "",
            telephone: "",

        }
    }
    function getNewCustomerFormDefaultData() {
        return {
            form: {
                contactType: 'Individual',
                submitting: false,
                errors: {},
                existingCompany: {},
                data: {
                    addNewCompany: false,
                    customer: {
                        company_id: null,
                        first_name: "",
                        last_name: "",
                        address: "",
                        email: null,
                        home_telephone: null,
                        work_telephone: null,
                        mobile_telephone: null,
                    },
                    company: {},
                },
            },
        };
    }
    Vue.component('newcustomerform', {
        data: function () {
            return getNewCustomerFormDefaultData();
        },
        watch: {
            'form.contactType': function(val,oldval){
                if(val === 'Individual') {
                    this.resetCompany();
                    this.form.data.addNewCompany = false;
                } else {
                    this.form.data.company = {};
                }
            },
        },
        methods:{
            reset() {
                Object.assign(this.$data, getNewCustomerFormDefaultData())
            },
            resetErrors() {
                this.form.errors = {};
            },
            resetCompany() {
                this.form.data.company = {};
                this.form.data.customer.company_id = null;
                this.form.existingCompany = getNewCompanyDefault();
            },
            setExistingCompany(company) {
                this.form.existingCompany = company;
                this.form.data.customer.company_id = company.id;
                this.form.data.addNewCompany = false;
            },
            showCompanyForm() {
                this.resetCompany();
                this.form.data.addNewCompany = true;
            },
            resetCustomer() {
                this.form.customer = {};
                this.resetCompany();
            },
            saveCustomer() {
                this.resetErrors();
                this.form.submitting = true;
                axios.post('{{route('customers.store')}}',
                    this.form.data
            )
            .then(response => {
                this.form.submitting = false;
                if (response.status === 200) {
                        app.setCustomer(response.data);
                        $('#newCustomerModal').modal('hide');
                        this.reset();
                    }
                })
                    .catch(e => {
                        this.form.submitting = false;
                        this.form.errors = e.response.data.errors;
                        if (e.response) {
                        }
                    })
            },
            copyCompanyAddress() {
                this.form.data.customer.address = this.form.existingCompany.address ? this.form.existingCompany.address : this.form.data.company.address ;
            },
        },
        template: `@include('customers._snippets._new-customer-form-modal')`
    });

</script>
