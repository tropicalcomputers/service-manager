@extends('layouts.dashboard.master')

@section('title', 'Customer')
@push('styles')
    <style>
    </style>
@endpush

@section('breadcrumbs')
    <li class="breadcrumb-item">
        <a href="{{route('work-items.index')}}">Work Items</a>
    </li>
    <li class="breadcrumb-item active">Customer</li>
@endsection

@section('content')
    <customer-show
        :customer = "{{$customerJson}}"
    ></customer-show>
@endsection

@push('scripts')
@endpush
