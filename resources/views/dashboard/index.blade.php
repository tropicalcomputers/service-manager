<?php
$scripts = ['charts','main'];
?>
@extends('layouts.dashboard.master')

@section('content')
    <dashboard
    :status-counts-prop="{{$statusCountsJson}}"
    ></dashboard>
@endsection
